from PyQt5 import QtWidgets, uic, QtCore, Qt
import os
import math

from .business_logic.podatak.model.podatak_tip import PodatakTip
from .business_logic.model.tabela_gui_kontroler import TabelaGUIKontroler

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from PyQt5 import QtGui
    from .business_logic.podatak.model.kompozitni_podatak import KompozitniPodatak
    from .business_logic.podatak.model.podatak import Podatak
    from .business_logic.podatak.interface.cvor_interface import CvorInterface
    from .business_logic.podatak.interface.podatak_interface import PodatakInterface
    from .business_logic.podatak.interface.list import ListInterface

class TabelaZaOdabirGUI:
    def __init__(self, kompozitni_podatak, lisni_podatak, filteri={}, cb_fns=[]):
        super(TabelaZaOdabirGUI, self).__init__()
        self.cb_fns = cb_fns

        self.lisni_podatak = lisni_podatak

        kompozitni_podatak : KompozitniPodatak
        if kompozitni_podatak.dobavi_tip() != PodatakTip.BATCH:
            raise ValueError('pokusano je instanciranje klase TabelaZaOdabirGUI sa kompozitnim podatkom koji nije tipa BATCH')
        
        self.kompozitni_podatak = kompozitni_podatak

        self.kontroler = TabelaGUIKontroler(self, kompozitni_podatak)
        for tabela in self.kontroler.dobavi_metapodatak_baze()['tabele']:
            if tabela['naziv'] == kompozitni_podatak.dobavi_atribut():
                self.metapodatak_batcha = tabela

        self.filteri = {}
        for atribut in self.metapodatak_batcha['atributi']:
            if atribut['naziv'] not in self.filteri:
                self.filteri[atribut['naziv']] = None

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_bazom_cinjenica', 'ui', 'tabela_za_odabir_dark.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QDialog
        # self.ui.setWindowTitle('Tabelarni prikaz za odabir')

        # ui elements
        self.table = self.ui.table
        self.table : QtWidgets.QTableWidget
        self.table.setSortingEnabled(True)
        self.table.itemSelectionChanged.connect(self._na_promenu_selekcije_u_tabeli)
        self.table.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        
        self.label_naziv_batcha = self.ui.label_naziv_batcha
        self.label_naziv_batcha : QtWidgets.QLabel
        self.label_naziv_batcha.setText(self.kompozitni_podatak.dobavi_reprezentacioni_atribut())

        self.sp_box_broj_strane = self.ui.sp_box_broj_strane
        self.sp_box_broj_strane : QtWidgets.QSpinBox
        self.sp_box_broj_strane.setMinimum(1)
        uspesno, broj_mogucih_strana = self.kontroler.dobavi_broj_mogucih_strana()
        if uspesno:
            self.sp_box_broj_strane.setMaximum(1 if broj_mogucih_strana == 0 else broj_mogucih_strana)

        self.sp_box_limit = self.ui.sp_box_limit
        self.sp_box_limit : QtWidgets.QSpinBox
        self.sp_box_limit.setValue(self.kontroler.limit)
        self.sp_box_limit.setMinimum(1)
        self.sp_box_limit.setMaximum(20)

        self.scroll_area = self.ui.scroll_area
        self.scroll_area : QtWidgets.QScrollArea
        self.scroll_area.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scroll_area.setVisible(False)

        self.grid_layout = QtWidgets.QGridLayout(self.scroll_area.widget())
        self.grid_layout : QtWidgets.QGridLayout

        self.btn_prva_strana = self.ui.btn_prva_strana
        self.btn_prva_strana : QtWidgets.QPushButton

        self.btn_leva_strana = self.ui.btn_leva_strana
        self.btn_leva_strana : QtWidgets.QPushButton

        self.btn_desna_strana = self.ui.btn_desna_strana
        self.btn_desna_strana : QtWidgets.QPushButton

        self.btn_poslednja_strana = self.ui.btn_poslednja_strana
        self.btn_poslednja_strana : QtWidgets.QPushButton

        self.btn_prvi_entitet = self.ui.btn_prvi_entitet
        self.btn_prvi_entitet : QtWidgets.QPushButton

        self.btn_gornji_entitet = self.ui.btn_gornji_entitet
        self.btn_gornji_entitet : QtWidgets.QPushButton

        self.btn_donji_entitet = self.ui.btn_donji_entitet
        self.btn_donji_entitet : QtWidgets.QPushButton

        self.btn_poslednji_entitet = self.ui.btn_poslednji_entitet
        self.btn_poslednji_entitet : QtWidgets.QPushButton

        self.btn_odaberi = self.ui.btn_odaberi
        self.btn_odaberi : QtWidgets.QPushButton

        self.btn_odustani = self.ui.btn_odustani
        self.btn_odustani : QtWidgets.QPushButton

        # polja
        self.strana = 0
        self.entiteti = []   
        self.selektovani_red = None

        # connections
        self.btn_odaberi.clicked.connect(self._odaberi)
        self.btn_odustani.clicked.connect(self.ui.close)
        self.sp_box_broj_strane.valueChanged.connect(lambda x: self.idi_na_stranu(self.sp_box_broj_strane.value()-1))
        self.sp_box_limit.valueChanged.connect(lambda x: self.promeni_limit(self.sp_box_limit.value()))

        self.btn_prva_strana.clicked.connect(lambda _: self.idi_na_stranu(0))
        self.btn_leva_strana.clicked.connect(lambda _: self.idi_na_stranu(0 if self.strana - 1 < 0 else self.strana - 1))
        self.btn_desna_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1 if self.strana + 1 > self.sp_box_broj_strane.maximum()-1 else self.strana + 1))
        self.btn_poslednja_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1))

        self.btn_prvi_entitet.clicked.connect(lambda _: self._selektuj_red(0))
        self.btn_gornji_entitet.clicked.connect(lambda _: self._selektuj_red(self.selektovani_red-1 if self.selektovani_red is not None else 0))
        self.btn_donji_entitet.clicked.connect(lambda _: self._selektuj_red(self.selektovani_red+1 if self.selektovani_red is not None else 0))
        self.btn_poslednji_entitet.clicked.connect(lambda _: self._selektuj_red(self.kontroler.ukupan_broj_entiteta_batcha-1))

        self.btn_prikazi_filtere = self.ui.btn_prikazi_filtere
        self.btn_prikazi_filtere : QtWidgets.QPushButton

        self.btn_primeni_filtere = self.ui.btn_primeni_filtere
        self.btn_primeni_filtere : QtWidgets.QPushButton
        self.btn_primeni_filtere.setVisible(False)

        self.btn_ponisti_filtere = self.ui.btn_ponisti_filtere
        self.btn_ponisti_filtere : QtWidgets.QPushButton
        self.btn_ponisti_filtere.setVisible(False)

        self.btn_prikazi_filtere.clicked.connect(self.prikazi_filtere)
        self.btn_primeni_filtere.clicked.connect(self.primeni_filtere)
        self.btn_ponisti_filtere.clicked.connect(self.ponisti_filtere)

        # methods
        if len(list(filteri.keys())) > 0:
            for kljuc in filteri:
                if kljuc in self.filteri:
                    self.filteri[kljuc] = filteri[kljuc]                    
            self.prikazi_filtere()

        self.primeni_filtere()

        self.dobavi_entitete()
        self.popuni_tabelu()
        self._izgradi_polja_za_filtere()

        self.ui.closeEvent = self._zatvori_dialog
        self.ui.exec()

    def _izgradi_polja_za_filtere(self):
        height = 50
        for index, atribut in enumerate(self.metapodatak_batcha['atributi']):
            horizontal_layout_widget = QtWidgets.QWidget()
            horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

            horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
            horizontal_layout.setContentsMargins(0, 0, 0, 0)

            self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

            self.grid_layout.addWidget(horizontal_layout_widget, index//3, index - index//3*3)

    def _dobavi_polja_spram_tipa(self, atribut, horizontal_layout_widget, horizontal_layout):
        label = QtWidgets.QLabel(horizontal_layout_widget)
        label.setStyleSheet("color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
        label_text = atribut['reprezentacioni_naziv'] + ":"
        label.setText(label_text)
        horizontal_layout.addWidget(label)

        if atribut['tip'] == PodatakTip.STRING.value:
            lineEdit = QtWidgets.QLineEdit(horizontal_layout_widget)
            lineEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")

            if atribut['naziv'] in self.filteri and self.filteri[atribut['naziv']] is not None:
                lineEdit.setText(self.filteri[atribut['naziv']])

            lineEdit.textChanged.connect(lambda _: self.dodeli_filter(atribut, lineEdit.text()))
            horizontal_layout.addWidget(lineEdit)
        elif atribut['tip'] == PodatakTip.TEXT.value:
            textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
            textEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            textEdit.setEnabled(True)
            
            if atribut['naziv'] in self.filteri and self.filteri[atribut['naziv']] is not None:
                textEdit.setText(self.filteri[atribut['naziv']])

            textEdit.textChanged.connect(lambda _: self.dodeli_filter(atribut, textEdit.toPlainText()))             
            horizontal_layout.addWidget(textEdit)
        elif atribut['tip'] == PodatakTip.NUMBER.value:
            spinBox = QtWidgets.QSpinBox(horizontal_layout_widget)
            spinBox.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            
            if atribut['naziv'] in self.filteri and self.filteri[atribut['naziv']] is not None:
                spinBox.setValue(self.filteri[atribut['naziv']])

            spinBox.valueChanged.connect(lambda _: self.dodeli_filter(atribut, spinBox.value()))
            horizontal_layout.addWidget(spinBox)
        elif atribut['tip'] == PodatakTip.DATE.value:
            dateTimeEdit = QtWidgets.QDateTimeEdit(horizontal_layout_widget)
            dateTimeEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            # datetime.date
            # dateTimeEdit.dateChanged.connect(lambda _: print(dateTimeEdit.date().toPyDate()))
            # self.dodeli_filter(atribut, dateTimeEdit.date().toPyDate())

            if atribut['naziv'] in self.filteri and self.filteri[atribut['naziv']] is not None:
                dateTimeEdit.setDate(self.filteri[atribut['naziv']])

            dateTimeEdit.dateChanged.connect(lambda _: self.dodeli_filter(atribut, dateTimeEdit.date().toPyDate())) 
            horizontal_layout.addWidget(dateTimeEdit)
        elif atribut['tip'] == PodatakTip.BOOLEAN.value:
            checkBox = QtWidgets.QCheckBox(horizontal_layout_widget)
            checkBox.setStyleSheet("font: 8pt \"Segoe UI\";")
            # checkBox.stateChanged.connect(lambda _: print(checkBox.isChecked()))

            if atribut['naziv'] in self.filteri and self.filteri[atribut['naziv']] is not None:
                checkBox.setChecked(self.filteri[atribut['naziv']])

            checkBox.stateChanged.connect(lambda _: self.dodeli_filter(atribut, checkBox.isChecked()))
            horizontal_layout.addWidget(checkBox)
        # elif atribut['tip'] == PodatakTip.BLOB.value:
        #     textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
        #     textEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
        #     # textEdit.setReadOnly(True)
        #     textEdit.textChanged.connect(lambda: print(textEdit.toPlainText()))
        #     horizontal_layout.addWidget(textEdit)

        #     pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
        #     pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
        #     pushButton.clicked.connect(lambda _: self._get_blob(textEdit))
        #     pushButton.setText('izaberi')
        #     horizontal_layout.addWidget(pushButton)
        
    def _zatvori_dialog(self, closeEvent):
        closeEvent : QtGui.QCloseEvent

        self.table.selectAll()
        items = self.table.selectedItems()
        for item in items:
            podatak = item.podatak
            podatak : Podatak
            podatak.widgets = list(filter(lambda widget_data: widget_data['widget'] != item, podatak.widgets))

        self.kontroler.otkaci_observere()

        for cb_fn in self.cb_fns:
            cb_fn()

    def _odaberi(self):
        selected_items = self.table.selectedItems()

        rows = []
        for selected_item in selected_items:
            row = selected_item.row()
            # ipak necemo ovo raditi jer u tabeli nema zaista strana
            # row += self.strana * self.kontroler.limit
            if row not in rows:
                rows.append(row)

        referencira = self.lisni_podatak.dobavi_referencira()
        koga_referencira_str = referencira['atribut']

        for row in rows:
            for item in selected_items:
                if row == item.row():
                    lisni_podatak = item.podatak
                    lisni_podatak : CvorInterface
                    entitet = lisni_podatak.dobavi_roditelja()
                    entitet : CvorInterface
                    batch = entitet.dobavi_roditelja()

                    batch : PodatakInterface
                    entitet : PodatakInterface
                    lisni_podatak : PodatakInterface
                    if batch.dobavi_atribut() + "." + entitet.dobavi_atribut() + "." + lisni_podatak.dobavi_atribut() == koga_referencira_str:
                        
                        # # buduci da se ovaj gui poziva samo prilikom izmena entiteta necemo raditi:
                        # referencira['vrednost'] = lisni_podatak
                        # self.lisni_podatak.postavi_referencira(referencira)

                        # lisni_podatak : ListInterface
                        # lisni_podatak.dodaj_referenciran_od(self.lisni_podatak)

                        # buduci da se ovaj gui poziva samo prilikom izmena entiteta radimo:
                        self.lisni_podatak.postavi_privremenu_vrednost(lisni_podatak.dobavi_vrednost())

                        self.ui.close()

    def popuni_tabelu(self):
        self.table.itemSelectionChanged.disconnect(self._na_promenu_selekcije_u_tabeli)
        # prvo otkaci bilo kakvu decu u tabeli
        self.table.selectAll()
        for item in self.table.selectedItems():
            item.podatak.widgets = list(filter(lambda x: x['widget'] != item, item.podatak.widgets))
            item.podatak = None

        self.table.setRowCount(0)
        self.table.itemSelectionChanged.connect(self._na_promenu_selekcije_u_tabeli)

        metapodatak = self.kontroler.dobavi_metapodatak_baze()

        columns = []
        for batch in metapodatak['tabele']:
            if batch['naziv'] == self.kompozitni_podatak.dobavi_atribut():
                columns = list(map(lambda x: x['reprezentacioni_naziv'], batch['atributi']))

                self.table.setColumnCount(len(columns))
                self.table.setHorizontalHeaderLabels(columns)


                rows = []
                for entitet in self.entiteti:
                    entitet : KompozitniPodatak
                    if entitet.dobavi_tip() != PodatakTip.ENTITET:
                        raise RuntimeError("entitet nije tipa ENTITET")

                    row = entitet.dobavi_decu()
                    # for atribut_podatak in entitet.dobavi_decu():
                    #     atribut_podatak : Podatak
                    #     row_data.append(atribut_podatak.dobavi_vrednost())

                    rows.append(row)

                for row in rows:
                    self.table.setRowCount(self.table.rowCount() + 1)
                    row_index = self.table.rowCount() - 1
                    col_index = 0
                    for col in row:
                        col : Podatak

                        table_item = QtWidgets.QTableWidgetItem((str(col.dobavi_vrednost())))
                        table_item.setFlags(table_item.flags() & ~QtCore.Qt.ItemFlag.ItemIsEditable)
                        col.widgets.append({'widget': table_item, 'tipovi': None})
                        table_item.podatak = col

                        self.table.setItem(row_index, col_index, table_item)
                        col_index += 1

                if self.selektovani_red is not None:
                    strana_za_selektovani_red = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
                    if self.strana != strana_za_selektovani_red:
                        self.selektovani_red = self.strana * self.kontroler.limit

                    # print(self.selektovani_red - self.kontroler.limit * self.strana)
                    self.table.selectRow(self.selektovani_red - self.kontroler.limit * self.strana)
                return

        raise RuntimeError('kompozitni podatak nije pronadjen u metapodatku baze')

    def _na_promenu_selekcije_u_tabeli(self):
        selected_items = self.table.selectedItems()

        rows = []
        for selected_item in selected_items:
            row = selected_item.row()
            row += self.strana * self.kontroler.limit
            if row not in rows:
                rows.append(row)

        for row in rows:
            # print(self.selektovani_red, row)

            if self.selektovani_red != row:
                self.selektovani_red = row

                strana_za_selektovani_red = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
                if self.strana != strana_za_selektovani_red:
                    self.selektovani_red = self.strana * self.kontroler.limit

                # print(self.selektovani_red - self.kontroler.limit * self.strana)                
                self.table.selectRow(self.selektovani_red - self.kontroler.limit * self.strana)
            return
        self.selektovani_red = None

    def idi_na_stranu(self, strana):
        # print(self.strana)
        
        if self.strana != strana:
            self.strana = strana

            self.sp_box_broj_strane.setValue(self.strana+1)
            self.dobavi_entitete()
            self.popuni_tabelu()        

    def promeni_limit(self, limit):
        self.kontroler.postavi_limit(limit)
        uspesno, br_strana = self.kontroler.dobavi_broj_mogucih_strana()
        if uspesno:
            # print('br_strana', br_strana)
            self.sp_box_broj_strane.setMaximum(br_strana)

            self.dobavi_entitete()
            self.popuni_tabelu()

    def _selektuj_red(self, red):
        if red < 0:
            red = 0
        elif red > self.kontroler.ukupan_broj_entiteta_batcha - 1:
            red = self.kontroler.ukupan_broj_entiteta_batcha - 1

        self.selektovani_red = red
        strana = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
        # print(strana)

        # self.table.clearSelection()
        if self.strana != strana:
            self.idi_na_stranu(strana)
        else:
            self.table.selectRow(self.selektovani_red - self.kontroler.limit * self.strana)
        
    def _dobavi_start_end_index(self):
        start_index = self.strana * self.kontroler.limit
        end_index = self.kontroler.ukupan_broj_entiteta_batcha - 1 if start_index + self.kontroler.limit - 1 > self.kontroler.ukupan_broj_entiteta_batcha - 1 else start_index + self.kontroler.limit - 1
        return start_index, end_index

    def dobavi_entitete(self):
        # print('idi_na_stranu', self.strana)
        start_index, end_index = self._dobavi_start_end_index()
        # print('poc. index,', start_index, ', krajnji index,', end_index)

        self.entiteti = self.kontroler.dobavi_entitete(start_index, end_index)

    def prikazi_filtere(self):
        self.scroll_area.setVisible(not self.scroll_area.isVisible())
        self.btn_primeni_filtere.setVisible(not self.btn_primeni_filtere.isVisible())
        self.btn_ponisti_filtere.setVisible(not self.btn_ponisti_filtere.isVisible())

        if self.btn_prikazi_filtere.text() == 'prikazi filtere':
            self.btn_prikazi_filtere.setText('sakrij filtere')
        else:
            self.btn_prikazi_filtere.setText('prikazi filtere')

    def dodeli_filter(self, atribut, vrednost):
        if isinstance(vrednost, str):
            if vrednost == '':
                vrednost = None
            # else:
            #     vrednost = '%'+vrednost+'%'
                
        self.filteri[atribut['naziv']] = vrednost

    def primeni_filtere(self):
        self.kontroler.postavi_filtere(self.kompozitni_podatak, self.filteri)

    def ponisti_filtere(self):
        for kljuc in self.filteri:
            self.filteri[kljuc] = None

        # self.prikazi_filtere()
        self.primeni_filtere()
        self._izgradi_polja_za_filtere()
