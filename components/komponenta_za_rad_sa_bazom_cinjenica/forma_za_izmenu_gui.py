from PyQt5 import QtWidgets, QtCore, uic
import os
from threading import Timer

from .business_logic.model.komponenta_kontroler import KomponentaKontroler
from .business_logic.podatak.interface.podatak_interface import PodatakInterface
from .business_logic.podatak.model.podatak_tip import PodatakTip
from .business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface
from .tabela_za_odabir_gui import TabelaZaOdabirGUI

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from PyQt5 import QtGui
    from .business_logic.pattern.behavioural.observer.model.observable import Observable
    from .business_logic.podatak.interface.kompozit_interface import KompozitInterface
    from .business_logic.podatak.interface.list import ListInterface
    from .business_logic.podatak.model.lisni_podatak import LisniPodatak

class FormaZaIzmenuGUI:
    def __init__(self, kompozitni_podatak_batch, kompozitni_podatak_entitet, cb_fns=[]):
        super(FormaZaIzmenuGUI, self).__init__()
        self.cb_fns = cb_fns

        kompozitni_podatak_batch : PodatakInterface
        if kompozitni_podatak_batch.dobavi_tip() != PodatakTip.BATCH:
            raise ValueError('pokusano je instanciranje klase FormaZaIzmenuGUI sa kompozitnim podatkom koji nije tipa BATCH')
        self.kompozitni_podatak_batch = kompozitni_podatak_batch

        kompozitni_podatak_entitet : PodatakInterface
        if kompozitni_podatak_entitet.dobavi_tip() != PodatakTip.ENTITET:
            raise ValueError('pokusano je instanciranje klase FormaZaIzmenuGUI sa kompozitnim podatkom(entitetom) koji nije tipa ENTITET')
        self.kompozitni_podatak_entitet = kompozitni_podatak_entitet

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_bazom_cinjenica', 'ui', 'create_update_delete_dark.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QDialog
        self.ui.setWindowTitle('Izmena entiteta')

        self.komponenta_kontroler = KomponentaKontroler.dobavi_instancu()
        self.komponenta_kontroler : Observable
        self.komponenta_kontroler.zakaci_observer(lambda x : self.proveri_rukovaoca_skladistem(x.rukovalac_skladistem))
        self.rukovalac_skladistem = self.komponenta_kontroler.rukovalac_skladistem
        
        self.observer = lambda rs: self._izmena_kod_rukovalaca_skladistem(rs)
        self.rukovalac_skladistem.zakaci_observer(self.observer)

        self.rukovalac_skladistem : RukovalacSkladistemInterface

        self.metapodaci_o_batchu = None
        for tabela in self.rukovalac_skladistem.dobavi_metapodatak_baze()['tabele']:
            if tabela['naziv'] == self.kompozitni_podatak_batch.dobavi_atribut():
                self.metapodaci_o_batchu = tabela
                break
        if self.metapodaci_o_batchu is None:
            raise RuntimeError('nisu pronadjeni metapodaci o batch-u')

        # ui elements
        self.l_naslov = self.ui.l_naslov
        self.l_naslov : QtWidgets.QLabel
        self.l_naslov.setText(self.kompozitni_podatak_batch.dobavi_reprezentacioni_atribut())

        self.scroll_area = self.ui.scroll_area
        self.scroll_area : QtWidgets.QScrollArea
        self.vbox = QtWidgets.QVBoxLayout()
        self.scroll_area.widget().setLayout(self.vbox)

        self.btn_potvrdi = self.ui.btn_potvrdi
        self.btn_potvrdi : QtWidgets.QPushButton

        self.btn_odustani = self.ui.btn_odustani
        self.btn_odustani : QtWidgets.QPushButton

        self.l_poruka = self.ui.l_poruka
        self.l_poruka : QtWidgets.QLabel
        self.l_poruka.setVisible(False)

        # connections
        self.btn_potvrdi.clicked.connect(self._potvrdi)
        self.btn_odustani.clicked.connect(self.ui.close)

        # polja
        self.entitet_izmenjen = False
        self.gui_observers = []

        # ako zatreba
        self.filteri = {}
        self.kompozitni_podatak_entitet : KompozitInterface
        for dete in self.kompozitni_podatak_entitet.dobavi_decu():
            dete : PodatakInterface
            self.filteri[dete.dobavi_atribut()] = dete.dobavi_vrednost()

        # metode
        self._izgradi_polja()

        self.ui.closeEvent = self._zatvori_dialog
        self.ui.exec()

    def zakaci_observere(self):
        self.rukovalac_skladistem.zakaci_observer(self.observer)

    def zakaci_gui_observere(self):
        for o in self.gui_observers:
            o['observable'].zakaci_observer(o['observer'])

    def otkaci_observere(self):
        self.rukovalac_skladistem.otkaci_observer(self.observer)

    def otkaci_gui_observere(self):
        for o in self.gui_observers:
            o['observable'].otkaci_observer(o['observer'])

    def _izmena_kod_rukovalaca_skladistem(self, rukovalac_skladistem):
        print('_izmena_kod_rukovalaca_skladistem se desila', self)

        greska = self.rukovalac_skladistem.dobavi_gresku()
        if greska:
            self.l_poruka.setVisible(True)
            self.l_poruka.setText(greska)

            timer = Timer(2.0, lambda : self.l_poruka.setVisible(False))
            timer.start()
        else:
            pass

    def _izgradi_polja(self):
        for i in reversed(range(self.vbox.count())): 
            self.vbox.itemAt(i).widget().setParent(None)

        atributi = self.kompozitni_podatak_entitet.dobavi_decu()
        if len(atributi) == 0:
            # dohvati entitet ponovo jer je u medju vremenu unisten
            self.rukovalac_skladistem.postavi_filtere(self.kompozitni_podatak_batch, self.filteri)
            uspesno, entiteti = self.rukovalac_skladistem.dobavi(self.kompozitni_podatak_batch)
            if uspesno and len(entiteti) > 0:
                self.kompozitni_podatak_entitet = entiteti[0]
            else:
                self.ui.close()

        height = 50
        for index, atribut in enumerate(self.kompozitni_podatak_entitet.dobavi_decu()):
            horizontal_layout_widget = QtWidgets.QWidget()
            horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

            horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
            horizontal_layout.setContentsMargins(0, 0, 0, 0)

            self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

            # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            # horizontal_layout.addItem(spacerItem)

            self.vbox.addWidget(horizontal_layout_widget)



        # self.vbox = QtWidgets.QVBoxLayout()

        # height = 50
        # for index, atribut in enumerate(self.kompozitni_podatak_entitet.dobavi_decu()):
        #     horizontal_layout_widget = QtWidgets.QWidget()
        #     horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

        #     horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
        #     horizontal_layout.setContentsMargins(0, 0, 0, 0)

        #     self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

        #     # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        #     # horizontal_layout.addItem(spacerItem)

        #     self.vbox.addWidget(horizontal_layout_widget)

        # self.scroll_area.widget().setLayout(self.vbox)

    def _dobavi_polja_spram_tipa(self, atribut, horizontal_layout_widget, horizontal_layout):
        atribut : PodatakInterface

        label = QtWidgets.QLabel(horizontal_layout_widget)
        label_text = atribut.dobavi_reprezentacioni_atribut() + ":"
        if atribut.dobavi_obavezan():
            label_text = "* " + label_text
        label.setText(label_text)
        horizontal_layout.addWidget(label)

        # provera da li neko referencira atribut
        atributi_referenciran_od = atribut.dobavi_atributi_referenciran_od()
        batchevi_sa_referencama = []
        if len(atributi_referenciran_od) > 0:
            metapodatak_baze = self.rukovalac_skladistem.dobavi_metapodatak_baze()
            
            for atribut_referenciran_od in atributi_referenciran_od:
                parts = atribut_referenciran_od.split(".")
                referencirajuce_batch_ime = parts[0]
                referencirajuci_atribut = parts[2]
                
                for tabela in metapodatak_baze['tabele']:
                    if tabela['naziv'] == referencirajuce_batch_ime:
                        batch = self.rukovalac_skladistem.dobavi_batch(referencirajuce_batch_ime)
                        uspesno, broj_entiteta_koji_ref = self.rukovalac_skladistem.broj_entiteta_batcha(batch, [{atribut.dobavi_atribut(): atribut.dobavi_vrednost()}])
                        if uspesno and broj_entiteta_koji_ref > 0:
                            # postoji potreba za otvaranjem tabele
                            batchevi_sa_referencama.append(batch)
                            break

        fn_za_pozvati_na_izmenu_disabled_vrednosti = []
        disabled = True if len(batchevi_sa_referencama) > 0 else False

        atribut : LisniPodatak
        referencira = atribut.dobavi_referencira()
        if referencira:
            label_vrednost = QtWidgets.QLabel(horizontal_layout_widget)

            if atribut.dobavi_privremenu_vrednost() is not None:
                label_vrednost.setText(str(atribut.dobavi_privremenu_vrednost()))
            elif atribut.dobavi_vrednost() is not None:
                label_vrednost.setText(str(atribut.dobavi_vrednost()))

            atribut : Observable
            observer = lambda lisni_podatak: label_vrednost.setText(str(self._dohvati_vrednost_za_polje(lisni_podatak)))
            atribut.zakaci_observer(observer)
            self.gui_observers.append({'observable': atribut, 'observer': observer})

            horizontal_layout.addWidget(label_vrednost)

            pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
            pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            pushButton.setText('odaberi')
            pushButton.setDisabled(disabled)
            fn_za_pozvati_na_izmenu_disabled_vrednosti.append(pushButton.setDisabled)

            batch_ime = referencira['atribut'].split('.')[0]
            batch = self.rukovalac_skladistem.dobavi_batch(batch_ime)

            pushButton.clicked.connect(lambda _: self._otvori_tabelu_za_odabir_gui(batch, atribut))

            horizontal_layout.addWidget(pushButton)

            if atribut.dobavi_obavezan():
                # referenciranje po hijerarhiji
                label = QtWidgets.QLabel(horizontal_layout_widget)
                label.setText("ref. po hijerarhiji")
                horizontal_layout.addWidget(label)

            elif not atribut.dobavi_obavezan():
                # referenciranje po relaciji
                label = QtWidgets.QLabel(horizontal_layout_widget)
                label.setText("ref. po relaciji")
                horizontal_layout.addWidget(label)
        else:
            # ne referencira
            if atribut.dobavi_tip() == PodatakTip.STRING:
                lineEdit = QtWidgets.QLineEdit(horizontal_layout_widget)
                lineEdit.setDisabled(disabled)
                fn_za_pozvati_na_izmenu_disabled_vrednosti.append(lineEdit.setDisabled)

                if atribut.dobavi_privremenu_vrednost() is not None:
                    lineEdit.setText(atribut.dobavi_privremenu_vrednost())
                elif atribut.dobavi_vrednost() is not None:
                    lineEdit.setText(atribut.dobavi_vrednost())

                lineEdit.textChanged.connect(lambda _: atribut.postavi_privremenu_vrednost(lineEdit.text()))
                horizontal_layout.addWidget(lineEdit)
            elif atribut.dobavi_tip() == PodatakTip.TEXT:
                textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
                textEdit.setDisabled(disabled)
                fn_za_pozvati_na_izmenu_disabled_vrednosti.append(textEdit.setDisabled)

                if atribut.dobavi_privremenu_vrednost() is not None:
                    textEdit.setText(atribut.dobavi_privremenu_vrednost())
                elif atribut.dobavi_vrednost() is not None:
                    textEdit.setText(atribut.dobavi_vrednost())

                textEdit.textChanged.connect(lambda _: atribut.postavi_privremenu_vrednost(textEdit.toPlainText()))                
                horizontal_layout.addWidget(textEdit)
            elif atribut.dobavi_tip() == PodatakTip.NUMBER:
                spinBox = QtWidgets.QSpinBox(horizontal_layout_widget)
                spinBox.setDisabled(disabled)
                fn_za_pozvati_na_izmenu_disabled_vrednosti.append(spinBox.setDisabled)

                if atribut.dobavi_privremenu_vrednost() is not None:
                    spinBox.setValue(atribut.dobavi_privremenu_vrednost())
                elif atribut.dobavi_vrednost() is not None:
                    spinBox.setValue(atribut.dobavi_vrednost())

                spinBox.valueChanged.connect(lambda _: atribut.postavi_privremenu_vrednost(spinBox.value()))
                horizontal_layout.addWidget(spinBox)
            elif atribut.dobavi_tip() == PodatakTip.DATE:
                dateTimeEdit = QtWidgets.QDateTimeEdit(horizontal_layout_widget)
                dateTimeEdit.setDisabled(disabled)
                fn_za_pozvati_na_izmenu_disabled_vrednosti.append(dateTimeEdit.setDisabled)
                # datetime.date

                if atribut.dobavi_privremenu_vrednost() is not None:
                    dateTimeEdit.setDate(atribut.dobavi_privremenu_vrednost())
                elif atribut.dobavi_vrednost() is not None:
                    dateTimeEdit.setDate(atribut.dobavi_vrednost())

                dateTimeEdit.dateChanged.connect(lambda _: atribut.postavi_privremenu_vrednost(dateTimeEdit.date().toPyDate()))
                horizontal_layout.addWidget(dateTimeEdit)
            elif atribut.dobavi_tip() == PodatakTip.BOOLEAN:
                checkBox = QtWidgets.QCheckBox(horizontal_layout_widget)
                checkBox.setDisabled(disabled)
                fn_za_pozvati_na_izmenu_disabled_vrednosti.append(checkBox.setDisabled)

                if atribut.dobavi_privremenu_vrednost() is not None:
                    checkBox.setChecked(atribut.dobavi_privremenu_vrednost())
                elif atribut.dobavi_vrednost() is not None:
                    checkBox.setChecked(atribut.dobavi_vrednost())

                checkBox.stateChanged.connect(lambda _: atribut.postavi_privremenu_vrednost(checkBox.isChecked()))
                horizontal_layout.addWidget(checkBox)
            elif atribut.dobavi_tip() == PodatakTip.BLOB:
                textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
                textEdit.setStyleSheet("font: 8pt \"Segoe UI\";")
                textEdit.setReadOnly(disabled)
                fn_za_pozvati_na_izmenu_disabled_vrednosti.append(textEdit.setReadOnly)

                if atribut.dobavi_privremenu_vrednost() is not None:
                    textEdit.setText(atribut.dobavi_privremenu_vrednost())
                elif atribut.dobavi_vrednost() is not None:
                    textEdit.setText(atribut.dobavi_vrednost())

                horizontal_layout.addWidget(textEdit)
                # atribut.zakaci_observer(lambda lisni_podatak: textEdit.setText(lisni_podatak.dobavi_vrednost()))
                observer = lambda lisni_podatak: textEdit.setText(str(self._dohvati_vrednost_za_polje(lisni_podatak)))
                atribut.zakaci_observer(observer)
                self.gui_observers.append({'observable': atribut, 'observer': observer})

                pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
                pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
                pushButton.clicked.connect(lambda _: self._get_blob(atribut))
                pushButton.setText('izaberi')
                horizontal_layout.addWidget(pushButton) 

        if len(batchevi_sa_referencama) > 0:
            label = QtWidgets.QLabel(horizontal_layout_widget)
            label.setText("deca:")
            horizontal_layout.addWidget(label)

            cb_deca = QtWidgets.QComboBox()
            cb_deca.addItems([batch.dobavi_reprezentacioni_atribut() for batch in batchevi_sa_referencama])
            horizontal_layout.addWidget(cb_deca)

            pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
            pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            
            pushButton.clicked.connect(lambda _: self._otvori_tabelu_gui(list(filter(lambda batch: batch.dobavi_reprezentacioni_atribut() == cb_deca.currentText(), batchevi_sa_referencama))[0], atribut))
            pushButton.setText('otvori tabelu')
            
            horizontal_layout.addWidget(pushButton)


            observer = lambda lisni_podatak: self._da_li_je_polje_dozvoljeno(lisni_podatak, label, cb_deca, pushButton, fn_za_pozvati_na_izmenu_disabled_vrednosti)
            atribut.zakaci_observer(observer)
            self.gui_observers.append({'observable': atribut, 'observer': observer})

    def _da_li_je_polje_dozvoljeno(self, atribut, label, cb_deca, pushButton, fn_za_pozvati_na_izmenu_disabled_vrednosti=None):
        if fn_za_pozvati_na_izmenu_disabled_vrednosti is None:
            fn_za_pozvati_na_izmenu_disabled_vrednosti = []

        atributi_referenciran_od = atribut.dobavi_atributi_referenciran_od()
        batchevi_sa_referencama = []
        if len(atributi_referenciran_od) > 0:
            metapodatak_baze = self.rukovalac_skladistem.dobavi_metapodatak_baze()
            
            for atribut_referenciran_od in atributi_referenciran_od:
                parts = atribut_referenciran_od.split(".")
                referencirajuce_batch_ime = parts[0]
                referencirajuci_atribut = parts[2]
                
                for tabela in metapodatak_baze['tabele']:
                    if tabela['naziv'] == referencirajuce_batch_ime:
                        batch = self.rukovalac_skladistem.dobavi_batch(referencirajuce_batch_ime)
                        uspesno, broj_entiteta_koji_ref = self.rukovalac_skladistem.broj_entiteta_batcha(batch, [{atribut.dobavi_atribut(): atribut.dobavi_vrednost()}])
                        if uspesno and broj_entiteta_koji_ref > 0:
                            # postoji potreba za otvaranjem tabele
                            batchevi_sa_referencama.append(batch)
                            break

        if len(batchevi_sa_referencama) > 0:

            cb_deca.clear()
            cb_deca.addItems([batch.dobavi_reprezentacioni_atribut() for batch in batchevi_sa_referencama])

        else:
            cb_deca.clear()
            label.hide()
            cb_deca.hide()
            pushButton.hide()

            for fn in fn_za_pozvati_na_izmenu_disabled_vrednosti:
                fn(False)

    def _dohvati_vrednost_za_polje(self, atribut):
        if atribut.dobavi_privremenu_vrednost() is not None:
            return atribut.dobavi_privremenu_vrednost()
        return atribut.dobavi_vrednost()

    def _otvori_tabelu_za_odabir_gui(self, batch, atribut):
        filteri = {atribut.dobavi_atribut(): atribut.dobavi_vrednost() if atribut.dobavi_privremenu_vrednost() is None else atribut.dobavi_privremenu_vrednost() }
        
        self.otkaci_observere()
        self.otkaci_gui_observere()
        TabelaZaOdabirGUI(batch, atribut, filteri, [self._izgradi_polja, self.zakaci_observere])

    def _otvori_tabelu_gui(self, batch, atribut):
        filteri = {atribut.dobavi_atribut(): atribut.dobavi_vrednost()}
        from .tabela_gui import TabelaGUI

        self.otkaci_observere()
        self.otkaci_gui_observere()
        TabelaGUI(batch, filteri, [self._izgradi_polja, self.zakaci_observere])

    def _get_blob(self, atribut):
        atribut : ListInterface

        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        # mimeTypeFilters = ["application/json"]
        # dlg.setMimeTypeFilters(mimeTypeFilters)

        if dlg.exec_():
            file_path = dlg.selectedFiles()[0]
            with open(file_path, 'r') as fp:
                blob = fp.read()
                atribut.postavi_vrednost(blob)

    def _zatvori_dialog(self, closeEvent):
        closeEvent : QtGui.QCloseEvent

        for cb_fn in self.cb_fns:
            cb_fn()

        # print('_zatvori_dialog')

    def _potvrdi(self):
        uspesno = self.rukovalac_skladistem.izmeni(self.kompozitni_podatak_batch, self.kompozitni_podatak_entitet)
        if uspesno:
            # ako je uspesno:
            self.entitet_izmenjen = True
            self.ui.close()
            # print('_potvrdi')

    def proveri_rukovaoca_skladistem(self, rukovalac_skladistem):
        if rukovalac_skladistem != self.rukovalac_skladistem:
            self.ui.close()
