from PyQt5 import QtWidgets, QtCore, uic
import os
from threading import Timer

from .business_logic.model.komponenta_kontroler import KomponentaKontroler
from .business_logic.pattern.creational.abstract_factory.abstract_factory import AbstractFactory
from .business_logic.podatak.interface.podatak_interface import PodatakInterface
from .business_logic.podatak.model.podatak_tip import PodatakTip
from .business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface
from .tabela_za_odabir_gui import TabelaZaOdabirGUI

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from PyQt5 import QtGui
    from .business_logic.pattern.creational.abstract_factory.abstract_factory_interface import AbstractFactoryInterface
    from .business_logic.pattern.behavioural.observer.model.observable import Observable
    from .business_logic.podatak.interface.kompozit_interface import KompozitInterface
    from .business_logic.podatak.interface.list import ListInterface
    from .business_logic.podatak.model.lisni_podatak import LisniPodatak

class FormaZaKreiranjeGUI:
    def __init__(self, kompozitni_podatak, cb_fns=[]):
        super(FormaZaKreiranjeGUI, self).__init__()
        self.cb_fns = cb_fns

        kompozitni_podatak : PodatakInterface
        if kompozitni_podatak.dobavi_tip() != PodatakTip.BATCH:
            raise ValueError('pokusano je instanciranje klase FormaZaKreiranjeGUI sa kompozitnim podatkom koji nije tipa BATCH')
        
        self.kompozitni_podatak = kompozitni_podatak

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_bazom_cinjenica', 'ui', 'create_update_delete_dark.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QDialog
        self.ui.setWindowTitle('Kreiranje entiteta')

        self.komponenta_kontroler = KomponentaKontroler.dobavi_instancu()
        self.komponenta_kontroler : Observable
        self.komponenta_kontroler.zakaci_observer(lambda x : self.proveri_rukovaoca_skladistem(x.rukovalac_skladistem))
        self.rukovalac_skladistem = self.komponenta_kontroler.rukovalac_skladistem

        self.observer = lambda rs: self._izmena_kod_rukovalaca_skladistem(rs)
        self.rukovalac_skladistem.zakaci_observer(self.observer)

        self.rukovalac_skladistem : RukovalacSkladistemInterface

        self.metapodaci_o_batchu = None
        for tabela in self.rukovalac_skladistem.dobavi_metapodatak_baze()['tabele']:
            if tabela['naziv'] == self.kompozitni_podatak.dobavi_atribut():
                self.metapodaci_o_batchu = tabela
                break
        if self.metapodaci_o_batchu is None:
            raise RuntimeError('nisu pronadjeni metapodaci o batch-u')

        ab_factory = AbstractFactory.dobavi_instancu()
        ab_factory : AbstractFactoryInterface 
        self.novi_entitet = ab_factory.kreiraj_entitet(self.metapodaci_o_batchu)
        self.novi_entitet : KompozitInterface

        # ui elements
        self.l_naslov = self.ui.l_naslov
        self.l_naslov : QtWidgets.QLabel
        self.l_naslov.setText(self.kompozitni_podatak.dobavi_reprezentacioni_atribut())

        self.scroll_area = self.ui.scroll_area
        self.scroll_area : QtWidgets.QScrollArea

        self.btn_potvrdi = self.ui.btn_potvrdi
        self.btn_potvrdi : QtWidgets.QPushButton

        self.btn_odustani = self.ui.btn_odustani
        self.btn_odustani : QtWidgets.QPushButton

        self.l_poruka = self.ui.l_poruka
        self.l_poruka : QtWidgets.QLabel
        self.l_poruka.setVisible(False)

        # connections
        self.btn_potvrdi.clicked.connect(self._potvrdi)
        self.btn_odustani.clicked.connect(self.ui.close)

        # polja
        self.entitet_sacuvan = False

        # metode
        self._izgradi_polja()

        self.ui.closeEvent = self._zatvori_dialog
        self.ui.exec()

    def _izmena_kod_rukovalaca_skladistem(self, rukovalac_skladistem):
        print('_izmena_kod_rukovalaca_skladistem se desila', self)

        greska = self.rukovalac_skladistem.dobavi_gresku()
        if greska:
            self.l_poruka.setVisible(True)
            self.l_poruka.setText(greska)

            timer = Timer(2.0, lambda : self.l_poruka.setVisible(False))
            timer.start()
        else:
            pass

    def _izgradi_polja(self):
        self.vbox = QtWidgets.QVBoxLayout()

        height = 50
        for index, atribut in enumerate(self.novi_entitet.dobavi_decu()):
            horizontal_layout_widget = QtWidgets.QWidget()
            horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

            horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
            horizontal_layout.setContentsMargins(0, 0, 0, 0)

            self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

            # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            # horizontal_layout.addItem(spacerItem)

            self.vbox.addWidget(horizontal_layout_widget)

        self.scroll_area.widget().setLayout(self.vbox)

    def _dobavi_polja_spram_tipa(self, atribut, horizontal_layout_widget, horizontal_layout):
        atribut : PodatakInterface

        label = QtWidgets.QLabel(horizontal_layout_widget)
        label_text = atribut.dobavi_reprezentacioni_atribut() + ":"
        if atribut.dobavi_obavezan():
            label_text = "* " + label_text
        label.setText(label_text)
        horizontal_layout.addWidget(label)

        atribut : LisniPodatak
        referencira = atribut.dobavi_referencira()
        if referencira:
            label_vrednost = QtWidgets.QLabel(horizontal_layout_widget)
            atribut : Observable
            atribut.zakaci_observer(lambda lisni_podatak: label_vrednost.setText(str(lisni_podatak.dobavi_privremenu_vrednost())))
            horizontal_layout.addWidget(label_vrednost)

            pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
            pushButton.setStyleSheet("""background-color: rgb(255, 255, 255);""")
            pushButton.setText('odaberi')

            batch_ime = referencira['atribut'].split('.')[0]
            batch = self.rukovalac_skladistem.dobavi_batch(batch_ime)
            pushButton.clicked.connect(lambda _: TabelaZaOdabirGUI(batch, atribut))            

            horizontal_layout.addWidget(pushButton)

            if atribut.dobavi_obavezan():
                # referenciranje po hijerarhiji
                label = QtWidgets.QLabel(horizontal_layout_widget)
                label.setText("ref. po hijerarhiji")
                horizontal_layout.addWidget(label)

            elif not atribut.dobavi_obavezan():
                # referenciranje po relaciji
                label = QtWidgets.QLabel(horizontal_layout_widget)
                label.setText("ref. po relaciji")
                horizontal_layout.addWidget(label)
        else:
            # ne referencira
            if atribut.dobavi_tip() == PodatakTip.STRING:
                lineEdit = QtWidgets.QLineEdit(horizontal_layout_widget)
                lineEdit.textChanged.connect(lambda _: atribut.postavi_vrednost(lineEdit.text()))
                horizontal_layout.addWidget(lineEdit)
            elif atribut.dobavi_tip() == PodatakTip.TEXT:
                textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
                textEdit.setEnabled(True)
                textEdit.textChanged.connect(lambda _: atribut.postavi_vrednost(textEdit.toPlainText()))                
                horizontal_layout.addWidget(textEdit)
            elif atribut.dobavi_tip() == PodatakTip.NUMBER:
                spinBox = QtWidgets.QSpinBox(horizontal_layout_widget)
                spinBox.valueChanged.connect(lambda _: atribut.postavi_vrednost(spinBox.value()))
                horizontal_layout.addWidget(spinBox)
            elif atribut.dobavi_tip() == PodatakTip.DATE:
                dateTimeEdit = QtWidgets.QDateTimeEdit(horizontal_layout_widget)
                # datetime.date
                # atribut.postavi_vrednost(dateTimeEdit.date().toPyDate())
                dateTimeEdit.dateChanged.connect(lambda _: atribut.postavi_vrednost(dateTimeEdit.date().toPyDate()))
                horizontal_layout.addWidget(dateTimeEdit)
            elif atribut.dobavi_tip() == PodatakTip.BOOLEAN:
                checkBox = QtWidgets.QCheckBox(horizontal_layout_widget)
                checkBox.stateChanged.connect(lambda _: atribut.postavi_vrednost(checkBox.isChecked()))
                horizontal_layout.addWidget(checkBox)
            elif atribut.dobavi_tip() == PodatakTip.BLOB:
                textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
                textEdit.setStyleSheet("font: 8pt \"Segoe UI\";")
                textEdit.setReadOnly(True)
                horizontal_layout.addWidget(textEdit)

                atribut.zakaci_observer(lambda lisni_podatak: textEdit.setText(str(lisni_podatak.dobavi_vrednost())))

                pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
                pushButton.setStyleSheet("""background-color: rgb(255, 255, 255);""")
                pushButton.clicked.connect(lambda _: self._get_blob(atribut))
                pushButton.setText('izaberi')
                horizontal_layout.addWidget(pushButton)

    def _get_blob(self, atribut):
        atribut : ListInterface

        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        # mimeTypeFilters = ["application/json"]
        # dlg.setMimeTypeFilters(mimeTypeFilters)

        if dlg.exec_():
            file_path = dlg.selectedFiles()[0]
            with open(file_path, 'r') as fp:
                blob = fp.read()
                atribut.postavi_vrednost(blob)

    def _zatvori_dialog(self, closeEvent):
        closeEvent : QtGui.QCloseEvent

        if not self.entitet_sacuvan:
            for atribut in self.novi_entitet.dobavi_decu():
                referenca = atribut.dobavi_referenciran_od()
                if referenca and referenca['vrednost']:
                    atribut : ListInterface
                    atribut.postavi_referencira(None)
        
        self.rukovalac_skladistem : Observable
        self.rukovalac_skladistem.otkaci_observer(self.observer)
        # print('_zatvori_dialog')

        for cb_fn in self.cb_fns:
            cb_fn()

    def _potvrdi(self):
        # sacuvaj entitet
        # pokusaj da se sacuva novi entitet u bazi
        uspesno = self.rukovalac_skladistem.kreiraj(self.kompozitni_podatak, self.novi_entitet)
        if uspesno:
            # ako je uspesno:
            self.entitet_sacuvan = True
            self.ui.close()
            # print('_potvrdi')

    def proveri_rukovaoca_skladistem(self, rukovalac_skladistem):
        if rukovalac_skladistem != self.rukovalac_skladistem:
            self._zatvori_dialog()
