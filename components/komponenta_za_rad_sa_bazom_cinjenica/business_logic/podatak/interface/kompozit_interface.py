from .cvor_interface import CvorInterface

class KompozitInterface(CvorInterface):
    # strukturalni
    def dodaj_dete(self, dete):
        raise NotImplementedError('metoda dodaj_dete nije implementirana')

    def obrisi_dete(self, dete, brisi_po_svim_nivoima=True):
        raise NotImplementedError('metoda obrisi_dete nije implementirana')

    def dobavi_decu(self):
        raise NotImplementedError('metoda dobavi_decu nije implementirana')

    def postavi_decu(self, deca : list):
        raise NotImplementedError('metoda postavi_decu nije implementirana')

    # po hijerarhiji
    def dodaj_dete_po_hijerarhiji(self, dete_po_hijerarhiji):
        raise NotImplementedError('metoda dodaj_dete_po_hijerarhiji nije implementirana')

    def obrisi_dete_po_hijerarhiji(self, dete_po_hijerarhiji):
        raise NotImplementedError('metoda obrisi_dete_po_hijerarhiji nije implementirana')

    def dobavi_decu_po_hijerarhiji(self):
        raise NotImplementedError('metoda dobavi_decu_po_hijerarhiji nije implementirana')

    def postavi_decu_po_hijerarhiji(self, deca_po_hijerarhiji : list):
        raise NotImplementedError('metoda postavi_decu_po_hijerarhiji nije implementirana')

    def dodaj_roditelja_po_hijerarhiji(self, roditelj_po_hijerarhiji):
        raise NotImplementedError('metoda dodaj_roditelja_po_hijerarhiji nije implementirana')

    def obrisi_roditelja_po_hijerarhiji(self, roditelj_po_hijerarhiji):
        raise NotImplementedError('metoda obrisi_roditelja_po_hijerarhiji nije implementirana')

    def dobavi_roditelje_po_hijerarhiji(self):
        raise NotImplementedError('metoda dobavi_roditelje_po_hijerarhiji nije implementirana')

    def postavi_roditelje_po_hijerarhiji(self, roditelji_po_hijerarhiji : list):
        raise NotImplementedError('metoda postavi_roditelje_po_hijerarhiji nije implementirana')

    # po relaciji
    def dodaj_dete_po_relaciji(self, dete_po_relaciji):
        raise NotImplementedError('metoda dodaj_dete_po_relaciji nije implementirana')

    def obrisi_dete_po_relaciji(self, dete_po_relaciji):
        raise NotImplementedError('metoda obrisi_dete_po_relaciji nije implementirana')

    def dobavi_decu_po_relaciji(self):
        raise NotImplementedError('metoda dobavi_decu_po_relaciji nije implementirana')

    def postavi_decu_po_relaciji(self, deca_po_relaciji : list):
        raise NotImplementedError('metoda postavi_decu_po_relaciji nije implementirana')

    def dodaj_roditelja_po_relaciji(self, roditelj_po_relaciji):
        raise NotImplementedError('metoda dodaj_roditelja_po_relaciji nije implementirana')

    def obrisi_roditelja_po_relaciji(self, roditelj_po_relaciji):
        raise NotImplementedError('metoda obrisi_roditelja_po_relaciji nije implementirana')

    def dobavi_roditelje_po_relaciji(self):
        raise NotImplementedError('metoda dobavi_roditelje_po_relaciji nije implementirana')

    def postavi_roditelje_po_relaciji(self, roditelji_po_relaciji : list):
        raise NotImplementedError('metoda postavi_roditelje_po_relaciji nije implementirana')