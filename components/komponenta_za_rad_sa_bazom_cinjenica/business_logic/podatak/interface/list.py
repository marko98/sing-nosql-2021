from .cvor_interface import CvorInterface

class ListInterface(CvorInterface):
    
    def dobavi_vrednost(self):
        raise NotImplementedError('metoda dobavi_vrednost nije implementirana')

    def postavi_vrednost(self, vrednost):
        raise NotImplementedError('metoda postavi_vrednost nije implementirana')

    def dobavi_privremenu_vrednost(self):
        raise NotImplementedError('metoda dobavi_privremenu_vrednost nije implementirana')

    def postavi_privremenu_vrednost(self, privremena_vrednost):
        raise NotImplementedError('metoda postavi_privremenu_vrednost nije implementirana')

    def dobavi_referencira(self):
        raise NotImplementedError('metoda dobavi_referencira nije implementirana')

    def postavi_referencira(self, referencira):
        raise NotImplementedError('metoda postavi_referencira nije implementirana')

    def dobavi_referenciran_od(self):
        raise NotImplementedError('metoda dobavi_referenciran_od nije implementirana')

    def postavi_referenciran_od(self, referenciran_od):
        raise NotImplementedError('metoda postavi_referenciran_od nije implementirana')
    
    def dodaj_referenciran_od(self, referenciran_od):
        raise NotImplementedError('metoda dodaj_referenciran_od nije implementirana')
    
    def obrisi_referenciran_od(self):
        raise NotImplementedError('metoda obrisi_referenciran_od nije implementirana')

    def dobavi_atributi_referenciran_od(self):
        raise NotImplementedError('metoda dobavi_atributi_referenciran_od nije implementirana')

    def postavi_atributi_referenciran_od(self, atributi_referenciran_od):
        raise NotImplementedError('metoda postavi_atributi_referenciran_od nije implementirana')
    
    def dodaj_atribut_referenciran_od(self, atribut_referenciran_od):
        raise NotImplementedError('metoda dodaj_atribut_referenciran_od nije implementirana')
    
    def obrisi_atribut_referenciran_od(self):
        raise NotImplementedError('metoda obrisi_atribut_referenciran_od nije implementirana')

    def dobavi_obavezan(self):
        raise NotImplementedError('metoda dobavi_obavezan nije implementirana')

    def postavi_obavezan(self, obavezan):
        raise NotImplementedError('metoda postavi_obavezan nije implementirana')