class PodatakInterface:
    def dobavi_atribut(self):
        raise NotImplementedError('metoda dobavi_atribut nije implementirana')

    def postavi_atribut(self, atribut):
        raise NotImplementedError('metoda postavi_atribut nije implementirana')

    def dobavi_reprezentacioni_atribut(self):
        raise NotImplementedError('metoda dobavi_reprezentacioni_atribut nije implementirana')

    def postavi_reprezentacioni_atribut(self, reprezentacioni_atribut):
        raise NotImplementedError('metoda postavi_reprezentacioni_atribut nije implementirana')

    def dobavi_tip(self):
        raise NotImplementedError('metoda dobavi_tip nije implementirana')

    def postavi_tip(self, tip):
        raise NotImplementedError('metoda postavi_tip nije implementirana')

    def prikazi_sebe(self):
        raise NotImplementedError('metoda prikazi_sebe nije implementirana')
    