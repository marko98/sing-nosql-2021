from PyQt5 import QtWidgets
import copy

from ..interface.podatak_interface import PodatakInterface
from ..interface.list import ListInterface
from ..interface.deo_stabla_interface import DeoStablaInterface
from ..interface.cvor_interface import CvorInterface

from ..model.podatak_tip import PodatakTip
from ...pattern.behavioural.observer.model.observable import Observable

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ..model.kompozitni_podatak import KompozitniPodatak

class LisniPodatak(Observable, PodatakInterface, ListInterface, DeoStablaInterface, CvorInterface):

    def __init__(self):
        super(LisniPodatak, self).__init__()

        self.atribut = None
        self.reprezentacioni_atribut = None
        self.obavezan = False
        self.jedinstven = False
        self.vrednost = None
        self.privremena_vrednost = None
        self.tip = None
        self.roditelj = None
        self.roditelj : KompozitniPodatak
        self.referencira = None
        self.referenciran_od = []
        self.atributi_referenciran_od = []

        self.widgets = []

    def dobavi_obavezan(self):
        return self.obavezan

    def postavi_obavezan(self, obavezan):
        self.obavezan = obavezan

    def dobavi_jedinstven(self):
        return self.jedinstven

    def postavi_jedinstven(self, jedinstven):
        self.jedinstven = jedinstven

    def __str__(self):
        self.roditelj : PodatakInterface

        return 'tip: {}, atribut: {}, obavezan: {}, jedinstven: {}, vrednost: {}, roditelj_atribut: {}, referencira: {}, dubina: {}, broj_deteta: {}'.format(\
            self.tip, self.atribut, self.obavezan, self.jedinstven, self.vrednost, \
            self.roditelj.dobavi_atribut() if self.roditelj is not None else None, self.referencira, self.dobavi_dubinu(), self.dobavi_broj_deteta())

        # # potpun roditelj
        # return 'tip: {}, atribut: {}, obavezan: {}, jedinstven: {}, vrednost: {}, roditelj: {}, referencira: {}'.format(\
        #     self.tip, self.atribut, self.obavezan, self.jedinstven, self.vrednost, self.roditelj , self.referencira,)

    # ListInterface
    def dobavi_vrednost(self):
        # if self.privremena_vrednost is not None:
        #     return self.privremena_vrednost

        # if self.referencira is not None:
        #     referenca = self.referencira['vrednost']

        #     if referenca is None:
        #         # if self.vrednost is not None:
        #         #     return self.vrednost

        #         # # veza hijerarhije
        #         # if self.obavezan and self.jedinstven:
        #         #     raise ValueError('podatak koji referencira neki drugi podatak nema referencu na njega')
        #         # # ako je preskocio if onda imamo vezu relacije

        #         return self.vrednost
        #     else:
        #         koga = referenca.dobavi_roditelja().dobavi_roditelja().dobavi_atribut() + "." + referenca.dobavi_roditelja().dobavi_atribut() + "." + referenca.dobavi_atribut()
        #         # print(koga)
        #         if koga == self.referencira['atribut']:                    
        #             if self.vrednost == referenca.dobavi_vrednost():
        #                 return self.vrednost
        #             else:
        #                 # raise ValueError('podatak referencira podatak koji ima drugaciju vrednost')
        #                 print('podatak referencira podatak koji ima drugaciju vrednost', self, 'raskacena je vrednost koja je referencirana')
        #                 ref = self.referencira['vrednost']
        #                 ref : LisniPodatak
        #                 ref.postavi_referenciran_od(list(filter(lambda x: x != self, ref.dobavi_referenciran_od())))
        #                 self.referencira['vrednost'] = None
        #         else:
        #             raise ValueError('podatak referencira pogresan tip podatka')
        # else:
        #     return self.vrednost

        return self.vrednost

    def postavi_vrednost(self, vrednost):
        self.vrednost = vrednost
        self.obavesti_observere()

    # def _postavi_vrednost(self, vrednost):
    #     self.vrednost = vrednost + ""
    #     # self.obavesti_observere()

    def dobavi_privremenu_vrednost(self):
        return self.privremena_vrednost

    def postavi_privremenu_vrednost(self, privremena_vrednost):
        if privremena_vrednost == self.vrednost:
            self.privremena_vrednost = None
        else:
            self.privremena_vrednost = privremena_vrednost
        self.obavesti_observere()

    def dobavi_referencira(self):
        return copy.copy(self.referencira)

    def postavi_referencira(self, referencira):
        if referencira is None:
            if self.referencira is not None and self.referencira['vrednost'] is not None:
                ref = self.referencira['vrednost']
                ref : LisniPodatak
                ref.postavi_referenciran_od(list(filter(lambda x: x != self, ref.dobavi_referenciran_od())))

                self.referencira['vrednost'] = None
                # self.postavi_vrednost(None)
        else:
            if self.referencira is None:
                self.referencira = referencira
            else:
            #     if self.referencira['vrednost'] is not None:
            #         ref = self.referencira['vrednost']
            #         ref : LisniPodatak
            #         ref.postavi_referenciran_od(list(filter(lambda x: x != self, ref.dobavi_referenciran_od())))

            #     self.referencira['vrednost'] = referencira['vrednost']
            #     self.postavi_vrednost(self.referencira['vrednost'].dobavi_vrednost())

                if self.referencira['vrednost'] is not None:
                    ref = self.referencira['vrednost']
                    ref : LisniPodatak
                    ref.postavi_referenciran_od(list(filter(lambda x: x != self, ref.dobavi_referenciran_od())))
                
                self.referencira = referencira

    def dobavi_referenciran_od(self):
        return  self.referenciran_od

    def postavi_referenciran_od(self, referenciran_od):
        self.referenciran_od = referenciran_od
        print('self.obavesti_observere() - postavi_referenciran_od')
        self.obavesti_observere()
    
    def dodaj_referenciran_od(self, referenciran_od):
        self.referenciran_od.append(referenciran_od)
        print('self.obavesti_observere() - dodaj_referenciran_od')
        self.obavesti_observere()
    
    def obrisi_referenciran_od(self):
        self.referenciran_od = []
        print('self.obavesti_observere() - obrisi_referenciran_od')
        self.obavesti_observere()

    def dobavi_atributi_referenciran_od(self):
        return self.atributi_referenciran_od

    def postavi_atributi_referenciran_od(self, atributi_referenciran_od):
        self.atributi_referenciran_od = atributi_referenciran_od
    
    def dodaj_atribut_referenciran_od(self, atribut_referenciran_od):
        self.atributi_referenciran_od.append(atribut_referenciran_od)
    
    def obrisi_atribut_referenciran_od(self):
        self.atributi_referenciran_od = []

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        self.atribut = atribut

    def dobavi_reprezentacioni_atribut(self):
        return self.reprezentacioni_atribut

    def postavi_reprezentacioni_atribut(self, reprezentacioni_atribut):
        self.reprezentacioni_atribut = reprezentacioni_atribut

    def dobavi_tip(self):
        return self.tip

    def postavi_tip(self, tip):
        if tip in [PodatakTip.DATABASE, PodatakTip.BATCH, PodatakTip.ENTITET]:
            raise ValueError('tip podatka mora biti PodatakTip')
        self.tip = tip

    def prikazi_sebe(self):
        print(self)

    # CvorInterface
    def dobavi_roditelja(self):
        return self.roditelj

    def postavi_roditelja(self, roditelj):
        # dodaj dete u roditelja i bice mu postavljen roditelj, a roditelju dete
        self.roditelj = roditelj    

    def dobavi_dubinu(self):
        if self.roditelj is None:
            return 0
        else:
            return self.roditelj.dobavi_dubinu() + 1

    def dobavi_redni_broj(self, tip=None):
        if self.roditelj is None:
            # # stavljeno zbog PodatakWidgetInterface -> odkaci_se_od_stabla
            # return 0
            return None
        else:
            if tip is None:
                for index, dete in enumerate(self.roditelj.dobavi_decu()):
                    if dete == self:
                        return index
            else:
                if tip in [PodatakTip.STRING, PodatakTip.NUMBER, PodatakTip.DATE, PodatakTip.BOOLEAN, PodatakTip.BLOB, PodatakTip.TEXT]:
                    filtrirana_deca = list(filter(lambda dete: dete.dobavi_tip() == tip, self.roditelj.dobavi_decu()))
                    for index, dete in enumerate(filtrirana_deca):
                        if dete == self:
                            return index
                else:
                    raise ValueError('vrsi se dobavi_redni_broj po tipu(KompozitniPodatakTip) koji ne odgovara klasi Podatak')
            raise IndexError('Dete \'ima\' roditelja, ali roditelj ne zna za njega')

    # DeoStablaInterface
    def zakaci_se_za_stablo(self, parent_widget, tipovi=None):
        # vec postoji
        for w_data in self.widgets:
            widget = w_data['widget']
            widget : QtWidgets.QWidget
            if widget.parent == parent_widget:
                return

        if tipovi is None or (tipovi is not None and self.dobavi_tip() in tipovi):
            parent_widget : QtWidgets.QWidget
            tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [self.reprezentacioni_atribut])
            tree_widget_item.parent = parent_widget

            tree_widget_item.podatak = self
            # self.widgets.append(tree_widget_item)
            self.widgets.append({'widget': tree_widget_item, 'tipovi': tipovi})

    def odkaci_se_od_stabla(self, parent_widget, tipovi=None):
        if tipovi is None or (tipovi is not None and self.dobavi_tip() in tipovi):
            parent_widget : QtWidgets.QTreeWidgetItem         
            for index, w_data in enumerate(self.widgets):
                widget = w_data['widget']
                widget : QtWidgets.QWidget
                if widget.parent == parent_widget:
                    parent_widget.removeChild(widget)
                    self.widgets.pop(index)
                    return
