import json

from core.business_logic.model.jezgro import Jezgro
from ..pattern.behavioural.observer.model.observable import Observable
from ..rukovalac_skladistem.model.rukovalac_relacionim_skladistem import RukovalacRelacionimSkladistem

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...komponenta_gui import KomponentaGUI
    from ..rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class KomponentaKontroler(Observable):
    instanca = None

    def __init__(self, gui_komponenta):
        super(KomponentaKontroler, self).__init__()
        # print(__name__)

        self.gui_komponenta = gui_komponenta
        self.gui_komponenta : KomponentaGUI        

        self.jezgro = Jezgro.dobavi_instancu()
        # self.jezgro : Observable
        # self.jezgro.zakaci_observer(lambda x : self.postavi_rukovaoca_skladistem(x.rukovalac_skladistem))

        # self.rukovalac_skladistem = self.jezgro.rukovalac_skladistem
        # self.rukovalac_skladistem : RukovalacSkladistemInterface

        # with open ('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.json', 'r') as fp:
        with open ('./meta/informacioni_resurs_generisano.json', 'r') as fp:
            self.metapodatak_baze = json.load(fp)

        self.rukovalac_skladistem = RukovalacRelacionimSkladistem.dobavi_instancu(self.metapodatak_baze)
        self.rukovalac_skladistem : RukovalacSkladistemInterface
    
    @staticmethod
    def dobavi_instancu(gui_komponenta=None):
        if KomponentaKontroler.instanca is not None:
            return KomponentaKontroler.instanca
        else:
            if gui_komponenta is not None:
                KomponentaKontroler.instanca = KomponentaKontroler(gui_komponenta)
                return KomponentaKontroler.instanca
        return None

    def popuni_tree_baza(self):
        baza = self.rukovalac_skladistem.dobavi_kompozitni_podatak_DATABASE()

        self.gui_komponenta.popuni_tree_baza(baza)

        # self.jezgro : Jezgro
        # self.jezgro.postavi_rukovaoca_skladistem()

    def isprazni_tree_baza(self):
        baza = self.rukovalac_skladistem.dobavi_kompozitni_podatak_DATABASE()

        self.gui_komponenta.isprazni_tree_baza(baza)

    def postavi_rukovaoca_skladistem(self, rukovalac_skladistem):
        self.rukovalac_skladistem = rukovalac_skladistem