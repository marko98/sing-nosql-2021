from ..model.komponenta_kontroler import KomponentaKontroler
import math
from threading import Timer

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...tabela_gui import TabelaGUI
    from ...business_logic.pattern.behavioural.observer.model.observable import Observable
    from ...business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class TabelaGUIKontroler:
    def __init__(self, tabela_gui, kompozitni_podatak):
        super(TabelaGUIKontroler, self).__init__()
        # print(__name__)

        self.tabela_gui = tabela_gui
        self.tabela_gui : TabelaGUI

        self.kompozitni_podatak = kompozitni_podatak      

        self.komponenta_kontroler = KomponentaKontroler.dobavi_instancu()
        self.komponenta_kontroler : Observable

        self.observer = lambda x : self.proveri_rukovaoca_skladistem(x.rukovalac_skladistem)
        self.komponenta_kontroler.zakaci_observer(self.observer)

        self.rukovalac_skladistem = self.komponenta_kontroler.rukovalac_skladistem
        self.rukovalac_skladistem : RukovalacSkladistemInterface
        self.rukovalac_skladistem : Observable
        self.rukovalac_skladistem.zakaci_observer(self._izmena_kod_rukovalaca_skladistem)

        self.limit = 4
        self.ukupan_broj_entiteta_batcha = 0
    
    def _izmena_kod_rukovalaca_skladistem(self, rukovalac_skladistem):
        # tabela gui
        print('_izmena_kod_rukovalaca_skladistem se desila', self)

        greska = self.rukovalac_skladistem.dobavi_gresku()
        if greska is not None:
            self.tabela_gui.l_msg.setText(greska)
            self.tabela_gui.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.tabela_gui.l_msg.setVisible(False))
            timer.start()
        else:
            uspesno, broj_mogucih_strana = self.dobavi_broj_mogucih_strana()
            if uspesno:
                self.tabela_gui.sp_box_broj_strane.setMaximum(1 if broj_mogucih_strana == 0 else broj_mogucih_strana)

                self.tabela_gui.dobavi_entitete()
                self.tabela_gui.popuni_tabelu() 

    def zakaci_observere(self):
        # self.jezgro.zakaci_observer(self.observer)
        self.rukovalac_skladistem.zakaci_observer(self._izmena_kod_rukovalaca_skladistem)

    def otkaci_observere(self):
        # self.jezgro.otkaci_observer(self.observer)
        self.rukovalac_skladistem.otkaci_observer(self._izmena_kod_rukovalaca_skladistem)

    def postavi_limit(self, limit):
        self.limit = limit

    def zatvori_dialog(self):
        self.tabela_gui.zatvori_dialog()

    def proveri_rukovaoca_skladistem(self, rukovalac_skladistem):
        if rukovalac_skladistem != self.rukovalac_skladistem:
            self.zatvori_dialog()

    def dobavi_metapodatak_baze(self):
        return self.rukovalac_skladistem.dobavi_metapodatak_baze()

    def dobavi_batch(self, batch_ime):
        return self.rukovalac_skladistem.dobavi_batch(batch_ime)

    def dobavi_batch_po_reprezentacionom_atributu(self, batch_reprezentacioni_naziv):
        return self.rukovalac_skladistem.dobavi_batch_po_reprezentacionom_atributu(batch_reprezentacioni_naziv)
    
    def broj_entiteta_batcha(self):
        uspesno, self.ukupan_broj_entiteta_batcha = self.rukovalac_skladistem.broj_entiteta_batcha(self.kompozitni_podatak)
        return uspesno, self.ukupan_broj_entiteta_batcha

    def dobavi_broj_mogucih_strana(self):
        uspesno, broj_entiteta = self.broj_entiteta_batcha()

        if uspesno:
            br_strana = math.ceil(broj_entiteta / self.limit)
            return uspesno, br_strana
        
        return uspesno, None

    def dobavi_entitete(self, start_index, end_index):
        # print('poc. index,', start_index, ', krajnji index,', end_index)

        uspesno, entiteti = self.rukovalac_skladistem.dobavi(self.kompozitni_podatak, start_index, end_index)
        if uspesno:
            return entiteti
        else:
            return []

    def postavi_filtere(self, kompozitni_podatak, filteri):
        self.rukovalac_skladistem.postavi_filtere(kompozitni_podatak, filteri)

    def obrisi_entitet(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):
        return self.rukovalac_skladistem.obrisi(kompozitni_podatak_batch, kompozitni_podatak_entitet)

    def isprazni_batch_u_skladistu(self, kompozitni_podatak_batch):
        self.rukovalac_skladistem.isprazni_batch_u_skladistu(kompozitni_podatak_batch)