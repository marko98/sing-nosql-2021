import copy
from datetime import datetime
import mysql.connector

from ..interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface
from .parser_fns import kreiraj_bazu, zapis_u_entitet
from ...podatak.model.lisni_podatak import LisniPodatak
from ...podatak.model.podatak_tip import PodatakTip
from ...pattern.behavioural.observer.model.observable import Observable

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...podatak.interface.kompozit_interface import KompozitInterface
    from ...podatak.interface.podatak_interface import PodatakInterface
    from ...podatak.interface.list import ListInterface
    
class RukovalacRelacionimSkladistem(Observable, RukovalacSkladistemInterface):
    instanca = None

    def __init__(self, metapodatak_baze):
        super(RukovalacRelacionimSkladistem, self).__init__()

        self.greska = None

        self.metapodatak_baze = metapodatak_baze
        self.baza = kreiraj_bazu(self.metapodatak_baze)

        self.mysql_konekcija = self._dobavi_konekciju()
        self.kursor = self.mysql_konekcija.cursor()
        self.kursor : mysql.connector.cursor.MySQLCursor

        self.batchovi = []
        self.sirocici = []

        self._postavi_batchove()
        self._proveri_relacije()

    def _dobavi_konekciju(self):
        if self.baza.dobavi_tip() == PodatakTip.DATABASE:
            host, korisnicko_ime, lozinka = None, None, None

            for dete in self.baza.dobavi_decu():
                dete : PodatakInterface
                if dete.dobavi_atribut() == 'HOST':
                    dete : Podatak
                    host = dete.dobavi_vrednost()
                elif dete.dobavi_atribut() == 'KORISNICKO_IME':
                    dete : Podatak
                    korisnicko_ime = dete.dobavi_vrednost()
                elif dete.dobavi_atribut() == 'LOZINKA':
                    dete : Podatak
                    lozinka = dete.dobavi_vrednost()
                
                if host is not None and korisnicko_ime is not None and lozinka is not None:
                    return mysql.connector.connect(
                            host=host,
                            user=korisnicko_ime,
                            password=lozinka
                        )
            raise RuntimeError('Baza ne sadrzi potrebne podtake za konekciju na server baze podataka')
        else:
            raise RuntimeError('Baza je neispravna')

    def _postavi_batchove(self):
        for dete in self.baza.dobavi_decu():
            dete : PodatakInterface
            if dete.dobavi_tip() == PodatakTip.BATCH:

                for tabela in self.metapodatak_baze['tabele']:
                    if tabela['naziv'] == dete.dobavi_atribut():
                        filteri = {}

                        for atribut in tabela['atributi']:
                            if atribut['naziv'] not in filteri:
                                filteri[atribut['naziv']] = None

                        self.batchovi.append({'batch': dete, 'filteri': filteri, "deca": {}})
                        break

    def _proveri_relacije(self):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            batch : KompozitInterface
            if batch.dobavi_tip() == PodatakTip.BATCH:
                for entitet in batch.dobavi_decu():
                    entitet : KompozitInterface
                    if entitet.dobavi_tip() == PodatakTip.ENTITET:
                        for dete in entitet.dobavi_decu():
                            if isinstance(dete.dobavi_tip(), PodatakTip):
                                self._pokusaj_da_postavis_referencu(dete)

    def _pokusaj_da_postavis_referencu(self, dete):
        dete : LisniPodatak
        referencira = dete.dobavi_referencira()
        if referencira is not None and dete.dobavi_vrednost() is not None:
            if referencira['vrednost'] is None or \
                (referencira['atribut'] is not None and referencira['vrednost'] is not None and dete.dobavi_vrednost() != referencira['vrednost'].dobavi_vrednost()):
                
                # referencira i unutar reference vrednost je None, ali podatak ima vrednost - tako da ga mozda mozemo spojiti sa onim kojeg referencira            
                atribut = referencira['atribut']
                parts = atribut.split('.')
                if len(parts) != 3:
                    raise ValueError('postoji potreba za referenciranjem gde nemamo punu putanju do reference')

                referencirani_batch_atribut = parts[0]
                # referencirani_entitet_atribut - mozda je nepotreban
                referencirani_entitet_atribut = parts[1]
                referencirani_podatak_atribut = parts[2]

                # provera batchova
                for batch_data in self.batchovi:
                    referencirani_batch = batch_data['batch']
                    referencirani_batch : KompozitInterface
                    if referencirani_batch.dobavi_tip() == PodatakTip.BATCH and referencirani_batch.dobavi_atribut() == referencirani_batch_atribut:
                        for referencirani_entitet in referencirani_batch.dobavi_decu():
                            referencirani_entitet : KompozitInterface
                            if referencirani_entitet.dobavi_tip() == PodatakTip.ENTITET and referencirani_entitet.dobavi_atribut() == referencirani_entitet_atribut:
                                for referencirano_dete in referencirani_entitet.dobavi_decu():
                                    if isinstance(referencirano_dete.dobavi_tip(), PodatakTip) and referencirano_dete.dobavi_atribut() == referencirani_podatak_atribut:
                                        referencirano_dete : LisniPodatak
                                        vrednost = referencirano_dete.dobavi_vrednost()
                                        if vrednost is not None and vrednost == dete.dobavi_vrednost():
                                            referencira['vrednost'] = referencirano_dete
                                            dete.postavi_referencira(referencira)
                                            referencirano_dete.dodaj_referenciran_od(dete)
                                            return
                
                if dete.dobavi_referencira()['vrednost'] is None:
                    # provera sirocica
                    for siroce_data in self.sirocici:
                        if siroce_data['batch_ime'] == referencirani_batch_atribut:
                            referencirani_entitet = siroce_data['siroce']
                            if referencirani_entitet.dobavi_tip() == PodatakTip.ENTITET and referencirani_entitet.dobavi_atribut() == referencirani_entitet_atribut:
                                for referencirano_dete in referencirani_entitet.dobavi_decu():
                                    if isinstance(referencirano_dete.dobavi_tip(), PodatakTip) and referencirano_dete.dobavi_atribut() == referencirani_podatak_atribut:
                                        referencirano_dete : LisniPodatak
                                        vrednost = referencirano_dete.dobavi_vrednost()
                                        if vrednost is not None and vrednost == dete.dobavi_vrednost():
                                            dete.dobavi_referencira()['vrednost'] = referencirano_dete
                                            referencirano_dete.dodaj_referenciran_od(dete)
                                            return

    @staticmethod
    def dobavi_instancu(metapodatak_baze=None):
        if RukovalacRelacionimSkladistem.instanca is not None:
            return RukovalacRelacionimSkladistem.instanca
        else:
            if metapodatak_baze is None:
                raise ValueError('pokusano instanciranje klase RukovalacRelacionimSkladistem bez metapodataka o relacionoj bazi')
            RukovalacRelacionimSkladistem.instanca = RukovalacRelacionimSkladistem(metapodatak_baze)
            return RukovalacRelacionimSkladistem.instanca

    def _proveri_sirocice_na_osnovu_zapisa(self, zapis, batch):
        for meta_batch in self.metapodatak_baze['tabele']:
            if meta_batch['naziv'] == batch.dobavi_atribut():

                if len(zapis) != len(meta_batch['atributi']):
                    raise RuntimeError('dobijen zapis sa servera nema isti broj vrednosti svojstava kao sta pise u metapodacima')

                atributi_pk_data = []
                for index, atr in enumerate(meta_batch['atributi']):
                    if atr['naziv'] in meta_batch['primarni_kljucevi']:
                        atributi_pk_data.append({'naziv': atr['naziv'], 'index': index})
                
                for index, siroce_data in enumerate(self.sirocici):
                    if siroce_data['batch_ime'] == batch.dobavi_atribut():
                        pronadjeno_siroce = True

                        siroce = siroce_data['siroce']
                        siroce : KompozitniPodatakInterface
                        if siroce.dobavi_tip() == PodatakTip.ENTITET:

                            for dete in siroce.dobavi_decu():
                                dete : Podatak
                                for atr_pk_data in atributi_pk_data:
                                    if dete.dobavi_atribut() == atr_pk_data['naziv']:
                                        if dete.dobavi_vrednost() != zapis[atr_pk_data['index']]:
                                            pronadjeno_siroce = False
                                            break
                                
                                if not pronadjeno_siroce:
                                    break
                            
                            if pronadjeno_siroce:
                                self.sirocici.pop(index) # izbacujemo ga iz liste buduci da vise nije siroce
                                return siroce
                        else:     
                            raise ValueError('postoji siroce koje nije entitet')
        return None

    # RukovalacSkladistemInterface
    # CRUD -----------------------------
    def dobavi(self, kompozitni_podatak_batch, start_index=0, end_index=0):
        kompozitni_podatak_batch : KompozitInterface
        if kompozitni_podatak_batch.dobavi_tip() == PodatakTip.BATCH:

            batch_deca = None
            for batch_data in self.batchovi:
                batch = batch_data['batch']
                batch : PodatakInterface
                kompozitni_podatak_batch : PodatakInterface
                if batch.dobavi_atribut() == kompozitni_podatak_batch.dobavi_atribut():
                    batch_deca = batch_data['deca']
                    break
            if batch_deca is None:
                raise RuntimeError('pokusavamo da dohvatimo entitete za batch koji ne postoji u skladistu')

            sf = []
            s_i = None
            for i in range(start_index, end_index + 1):
                if i in batch_deca:
                    if s_i is not None:
                        sf.append([s_i, i-1])
                        s_i = None
                else:
                    if s_i is None:
                        s_i = i
            if s_i is not None:
                sf.append([s_i, end_index])
                s_i = None

            self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))
            # sql = 'SELECT * FROM {} LIMIT {}, {}'.format(kompozitni_podatak_batch.dobavi_atribut(), len(kompozitni_podatak_batch.dobavi_decu()), self.limit,)
            # self.kursor.execute(sql)
            for _sf in sf:
                _start_index = _sf[0]
                _end_index = _sf[1]

                for tabela in self.metapodatak_baze['tabele']:
                    if tabela['naziv'] == kompozitni_podatak_batch.dobavi_atribut():
                        ime_procedure = tabela['operacije']['dobavi']['naziv']
                        args = tabela['operacije']['dobavi']['args']
                        # limit
                        args[-2]['LIMIT'] = _end_index - _start_index + 1
                        # offset
                        args[-1]['OFFSET'] = _start_index

                        for recnik in args[:-2]:
                            for kljuc in batch_data['filteri']:
                                if kljuc in recnik:
                                    recnik[kljuc] = batch_data['filteri'][kljuc]

                        args = list(map(lambda recnik: recnik[list(recnik)[0]], args))
                        try:
                            print('procedura ' + ime_procedure + ' pozvana')
                            self.kursor.callproc(ime_procedure, args)
                        except Exception as e:
                            if isinstance(e, mysql.connector.errors.IntegrityError):
                                e : mysql.connector.errors.IntegrityError
                                self.greska = e.msg
                            elif isinstance(e, mysql.connector.errors.DataError):
                                e : mysql.connector.errors.DataError
                                self.greska = e.msg
                            elif isinstance(e, mysql.connector.errors.ProgrammingError):
                                e : mysql.connector.errors.ProgrammingError
                                self.greska = e.msg
                            else:
                                self.greska = 'doslo je do greske - dobavi'

                            self.obavesti_observere()
                            return False, []

                        for result in self.kursor.stored_results():

                            brojac = _start_index
                            zapisi = result.fetchall()
                            for zapis in zapisi:
                                # proveravamo da li je mozda vec dohvacen entitet ali je u sirocicima
                                entitet = self._proveri_sirocice_na_osnovu_zapisa(zapis, kompozitni_podatak_batch)
                                if entitet is None:
                                    entitet = zapis_u_entitet(kompozitni_podatak_batch.dobavi_atribut(), zapis, self.metapodatak_baze)
                                kompozitni_podatak_batch.dodaj_dete(entitet)
                                
                                # # za testiranje _proveri_sirocice_na_osnovu_zapisa
                                # entitet = zapis_u_entitet(kompozitni_podatak_batch.dobavi_atribut(), zapis, self.metapodatak_baze)
                                # self.sirocici.append({'batch_ime': kompozitni_podatak_batch.dobavi_atribut(), 'siroce': entitet})
                                # entitet = self._proveri_sirocice_na_osnovu_zapisa(zapis, kompozitni_podatak_batch)

                                # for dete in entitet.dobavi_decu():
                                    # self._pokusaj_da_postavis_referencu(dete)                                

                                batch_deca[brojac] = entitet
                                brojac += 1

                            self._proveri_relacije()

            entiteti = []
            for i in range(start_index, end_index + 1):
                if i in batch_deca:
                    entiteti.append(batch_deca[i])
            return True, entiteti
        else:
            raise RuntimeError('pokusano dobavljanje entiteta za kompozitni podatak koji nije tipa BATCH')

    def kreiraj(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):
        # 1) proveri da li svi obavezni atributi imaju neku vrednost
        for dete in kompozitni_podatak_entitet.dobavi_decu():
            dete : LisniPodatak

            if dete.dobavi_obavezan():
                # oni koji ne referenciraju treba da imaju vrednost
                if dete.dobavi_referencira() is None and dete.dobavi_vrednost() is None:
                    self.greska = 'Molimo popunite sva obavezna polja(*)'
                    self.obavesti_observere()
                    return False
                # oni koji referenciraju treba da imaju privremenu vrednost
                elif dete.dobavi_referencira() and dete.dobavi_privremenu_vrednost() is None:
                    self.greska = 'Molimo popunite sva obavezna polja(*), \n ref. po hijerarhiji'
                    self.obavesti_observere()
                    return False

        # # 1) provera da li svi obavezni atributi imaju vrednosti postavljene
        # for dete in kompozitni_podatak_entitet.dobavi_decu():
        #     dete : LisniPodatak

        #     vrednost = dete.dobavi_vrednost()
        #     if vrednost is None:
        #         vrednost = dete.dobavi_privremenu_vrednost()

        #     if dete.dobavi_obavezan() and vrednost is None:
        #         self.greska = 'Molimo popunite sva obavezna polja(*)'
        #         self.obavesti_observere()
        #         return False
        # # 2) provera referenci po hijerarhiji
        # for dete in kompozitni_podatak_entitet.dobavi_decu():
        #     dete : LisniPodatak
        #     referencira = dete.dobavi_referencira()
        #     if dete.dobavi_obavezan() and referencira:
        #         if referencira['vrednost'] is None:
        #             self.greska = 'Molimo popunite sva obavezna polja(*), \n ref. po hijerarhiji'
        #             self.obavesti_observere()
        #             return False
        #         else:
        #             # za svaki slucaj mozda i ne treba
        #             ref = referencira['vrednost']
        #             ref : ListInterface
        #             dete.postavi_vrednost(ref.dobavi_vrednost())

        kompozitni_podatak_batch : PodatakInterface
        if kompozitni_podatak_batch.dobavi_tip() == PodatakTip.BATCH:
            for tabela in self.metapodatak_baze['tabele']:
                if tabela['naziv'] == kompozitni_podatak_batch.dobavi_atribut():
                    ime_procedure = tabela['operacije']['kreiraj']['naziv']
                    args = tabela['operacije']['kreiraj']['args']

                    kompozitni_podatak_entitet : KompozitInterface
                    if len(args) != len(kompozitni_podatak_entitet.dobavi_decu()):
                        raise RuntimeError('pokusano je kreiranje entiteta koji nema onoliko atributa koliko procedura zahteva')

                    args = []
                    for dete in kompozitni_podatak_entitet.dobavi_decu():
                        dete : LisniPodatak

                        print('pitanje da li je dobro - kreiraj')
                        vrednost = dete.dobavi_vrednost()
                        if vrednost is None:
                            vrednost = dete.dobavi_privremenu_vrednost()
                        args.append(vrednost)

                    try:
                        self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))

                        self.kursor.callproc(ime_procedure, args)
                        self.mysql_konekcija.commit()

                        # kreiraj procedura nece nista vratiti
                        # for result in self.kursor.stored_results():
                        #     broj_entiteta = result.fetchone()[0]
                        #     print('hi')

                        # uspesno, broj_entiteta = self.broj_entiteta_batcha(kompozitni_podatak_batch)
                        # if uspesno:
                        #     index_novog_entiteta = broj_entiteta - 1

                        for batch_data in self.batchovi:
                            batch = batch_data['batch']
                            batch : PodatakInterface
                            if batch.dobavi_atribut() == kompozitni_podatak_batch.dobavi_atribut():
                                # ovaj pristup ne mozemo vise koristiti (pogledaj dole objasnjenje)
                                # recnik_dece = batch_data['deca']
                                # index_prethodnika = index_novog_entiteta - 1
                                # if index_prethodnika in recnik_dece:
                                #     # znaci da smo dohvatili na klijentu sve entitete pre novo kreiranog i
                                #     # da mozemo i njega sada dodati i vezati ga za roditelja
                                #     kompozitni_podatak_batch.dodaj_dete(kompozitni_podatak_entitet)
                                #     recnik_dece[index_novog_entiteta] = kompozitni_podatak_entitet
                                # else:
                                #     # znaci da nismo dohvatili na klijentu sve entitete pre novo kreiranog i
                                #     # da cemo ga smestiti u sirocice
                                #     self.sirocici.append({'batch_ime': batch.dobavi_atribut(), 'siroce': kompozitni_podatak_entitet})

                                # moramo od sve dece napraviti sirocice jer na serveru mysql-u moze doci
                                # do promene redosleda zapisa usled vrednosti identifikatora neke tabele
                                # a buduci da se mi oslanjamo na redosled moramo svu decu pretvoriti u sirocice
                                batch : KompozitInterface
                                for dete in batch.dobavi_decu():
                                    batch.obrisi_dete(dete, False)
                                    self.sirocici.append({'batch_ime': batch.dobavi_atribut(), 'siroce': dete})
                                batch_data['deca'] = {}

                                self.obavesti_observere()
                                return True
                    except Exception as e:
                        if isinstance(e, mysql.connector.errors.IntegrityError):
                            e : mysql.connector.errors.IntegrityError
                            self.greska = e.msg

                        elif isinstance(e, mysql.connector.errors.DataError):
                            e : mysql.connector.errors.DataError
                            self.greska = e.msg

                        elif isinstance(e, mysql.connector.errors.ProgrammingError):
                            e : mysql.connector.errors.ProgrammingError
                            self.greska = e.msg
                        else:
                            self.greska = 'doslo je do greske - kreiraj'

                        self.obavesti_observere()
                        return False

        self.greska = 'doslo je do greske - kreiraj'
        self.obavesti_observere()
        return False

    def izmeni(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):

        # 1. svaki atribut u entitetu koji ima privremenu vrednost, koji se menja ne sme da ima druge atribute koji ga 
        # referenciraju
        kompozitni_podatak_entitet : PodatakTip.ENTITET
        for dete in kompozitni_podatak_entitet.dobavi_decu():
            dete : LisniPodatak
            if dete.dobavi_privremenu_vrednost() is not None:
                
                # provera da li postoji u bazi entiteti koji referenciraju ovaj atribut
                atributi_referenciran_od = dete.dobavi_atributi_referenciran_od()
                if len(atributi_referenciran_od) > 0:
                    
                    for atribut_referenciran_od in atributi_referenciran_od:
                        parts = atribut_referenciran_od.split(".")
                        referencirajuce_batch_ime = parts[0]
                        referencirajuci_atribut = parts[2]

                        for batch_data in self.batchovi:
                            batch = batch_data['batch']
                            batch : PodatakInterface
                            if batch.dobavi_atribut() == referencirajuce_batch_ime:
                                uspesno, broj_entiteta_koji_ref = self.broj_entiteta_batcha(batch, [{dete.dobavi_atribut(): dete.dobavi_vrednost()}])
                                if uspesno and broj_entiteta_koji_ref > 0:                                    
                                    # ovaj atribut je referenciran od strane drugih entiteta u bazi
                                    self.greska = 'atribut je referenciran od strane drugih entiteta u bazi'
                                    self.obavesti_observere()
                                    return False

        _args = {}
        for dete in kompozitni_podatak_entitet.dobavi_decu():
            dete : LisniPodatak
            _args[dete.dobavi_atribut()] = dete.dobavi_privremenu_vrednost()
            _args["W_"+dete.dobavi_atribut()] = dete.dobavi_vrednost()

        kompozitni_podatak_batch : PodatakInterface
        if kompozitni_podatak_batch.dobavi_tip() == PodatakTip.BATCH:
            for tabela in self.metapodatak_baze['tabele']:
                if tabela['naziv'] == kompozitni_podatak_batch.dobavi_atribut():
                    ime_procedure = tabela['operacije']['izmeni']['naziv']
                    args = tabela['operacije']['izmeni']['args']

                    if len(args) != len(_args.keys()):
                        self.greska = 'prilikom izmene doslo je do greske oko broja argumenta procedure za izmenu'
                        self.obavesti_observere()
                        return False

                    for recnik in args:
                        for kljuc in recnik:
                            recnik[kljuc] = _args[kljuc]

                    args = list(map(lambda recnik: recnik[list(recnik)[0]], args))
                    # for index, arg in enumerate(args):
                    #     if isinstance(arg, datetime.date):
                    #         args[index] = arg.strftime('%Y-%m-%d')
                    try:
                        self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))
                        print('procedura ' + ime_procedure + ' pozvana')
                        self.kursor.callproc(ime_procedure, args)
                        self.mysql_konekcija.commit()
                    except Exception as e:
                        if isinstance(e, mysql.connector.errors.IntegrityError):
                            e : mysql.connector.errors.IntegrityError
                            self.greska = e.msg
                        elif isinstance(e, mysql.connector.errors.DataError):
                            e : mysql.connector.errors.DataError
                            self.greska = e.msg
                        elif isinstance(e, mysql.connector.errors.ProgrammingError):
                            e : mysql.connector.errors.ProgrammingError
                            self.greska = e.msg
                        else:
                            self.greska = 'doslo je do greske - izmeni'

                        self.obavesti_observere()
                        return False

                    for dete in kompozitni_podatak_entitet.dobavi_decu():
                        privremena_vrednost = dete.dobavi_privremenu_vrednost()
                        if privremena_vrednost is not None:

                            # brojac = 0
                            # while dete.vrednost != privremena_vrednost:
                            #     print(brojac)
                            #     brojac+=1

                            #     # dete._postavi_vrednost(privremena_vrednost)
                            #     dete.postavi_vrednost(privremena_vrednost)
                            #     # dete.vrednost = privremena_vrednost

                            dete.postavi_vrednost(privremena_vrednost)
                            dete.postavi_privremenu_vrednost(None)

                        # 2. ukini reference koje ima dete
                        referencira = dete.dobavi_referencira()
                        if referencira is not None:
                            # ref = referencira['vrednost']
                            # ref : LisniPodatak
                            # ref.postavi_referenciran_od(list(filter(lambda x: x != dete, ref.dobavi_referenciran_od())))
                            # referencira['vrednost'] = None
                            # dete.postavi_referencira(referencira)
                            dete.postavi_referencira(None)

                    self._proveri_relacije()

                    self.obavesti_observere()
                    return True

            self.greska = 'doslo je do greske - izmeni, nije pronadjen metapodatak o batch-u'
            self.obavesti_observere()
            return False
        else:
            raise RuntimeError('pokusano dobavljanje entiteta za kompozitni podatak koji nije tipa BATCH')

    def obrisi(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):
        # 1) da li entitet neko referencira u bazi
        kompozitni_podatak_entitet : KompozitInterface
        for atribut in kompozitni_podatak_entitet.dobavi_decu():
            atribut : ListInterface
            # provera da li neko referencira atribut entiteta
            atributi_referenciran_od = atribut.dobavi_atributi_referenciran_od()
            if len(atributi_referenciran_od) > 0:                
                for atribut_referenciran_od in atributi_referenciran_od:
                    parts = atribut_referenciran_od.split(".")
                    referencirajuce_batch_ime = parts[0]
                    referencirajuci_atribut = parts[2]
                    
                    for tabela in self.metapodatak_baze['tabele']:
                        if tabela['naziv'] == referencirajuce_batch_ime:
                            batch = self.dobavi_batch(referencirajuce_batch_ime)
                            uspesno, broj_entiteta_koji_ref = self.broj_entiteta_batcha(batch, [{atribut.dobavi_atribut(): atribut.dobavi_vrednost()}])
                            if uspesno and broj_entiteta_koji_ref > 0:
                                # postoji referenciranje
                                self.greska = 'pokusano je brisanje entiteta koji je referenciran od drugih'
                                self.obavesti_observere()
                                return False

        # 2) obrisi entitet
        for tabela in self.metapodatak_baze['tabele']:
            kompozitni_podatak_batch : PodatakInterface
            if tabela['naziv'] == kompozitni_podatak_batch.dobavi_atribut():
                opeacija_obrisi = tabela['operacije']['obrisi']
                ime_procedure = opeacija_obrisi['naziv']
                args = opeacija_obrisi['args']

                for recnik in args:
                    for atribut in kompozitni_podatak_entitet.dobavi_decu():
                        atribut : PodatakInterface
                        if atribut.dobavi_atribut() in recnik:
                            recnik[atribut.dobavi_atribut()] = atribut.dobavi_vrednost()
                            break

                args = list(map(lambda recnik: recnik[list(recnik)[0]], args))
                try:
                    self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))
                    print('procedura ' + ime_procedure + ' pozvana')
                    self.kursor.callproc(ime_procedure, args)
                    self.mysql_konekcija.commit()

                    # 3) raskaci veze sa onima koje on referencira
                    for atribut in kompozitni_podatak_entitet.dobavi_decu():
                        atribut : ListInterface
                        if atribut.dobavi_referencira() is not None:
                            atribut.postavi_referencira(None)

                    # 4) izbaci dete iz self.batchovi -> deca
                    for batch_data in self.batchovi:
                        batch = batch_data['batch']
                        batch : PodatakInterface
                        if batch.dobavi_atribut() == kompozitni_podatak_batch.dobavi_atribut():
                            for kljuc in batch_data['deca']:
                                entitet = batch_data['deca'][kljuc]
                                entitet : PodatakInterface
                                if entitet == kompozitni_podatak_entitet:
                                    del batch_data['deca'][kljuc]
                                    break
                            break

                    # 5) raskaci ga od roditelja
                    kompozitni_podatak_batch : KompozitInterface
                    kompozitni_podatak_batch.obrisi_dete(kompozitni_podatak_entitet)

                    self.obavesti_observere()
                    return True
                except Exception as e:
                    if isinstance(e, mysql.connector.errors.IntegrityError):
                        e : mysql.connector.errors.IntegrityError
                        self.greska = e.msg

                    elif isinstance(e, mysql.connector.errors.DataError):
                        e : mysql.connector.errors.DataError
                        self.greska = e.msg

                    elif isinstance(e, mysql.connector.errors.ProgrammingError):
                        e : mysql.connector.errors.ProgrammingError
                        self.greska = e.msg
                    else:
                        self.greska = 'doslo je do greske - obrisi'

                    self.obavesti_observere()
                    return False

        self.greska = 'pokusano je brisanje entiteta za ciji batch ne postoje metapodaci'
        self.obavesti_observere()
        return False

    # -----------------------------

    # BITNO!!! dodat deo za obnovu konekcije
    def isprazni_batch_u_skladistu(self, kompozitni_podatak_batch):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            batch : PodatakInterface
            kompozitni_podatak_batch : PodatakInterface
            if batch.dobavi_atribut() == kompozitni_podatak_batch.dobavi_atribut():
                kompozitni_podatak_batch : KompozitInterface
                for dete in kompozitni_podatak_batch.dobavi_decu():
                    kompozitni_podatak_batch.obrisi_dete(dete)
                batch_data['deca'] = {}
        
        self.mysql_konekcija = self._dobavi_konekciju()
        self.kursor = self.mysql_konekcija.cursor()

    def dobavi_kompozitni_podatak_DATABASE(self):
        return self.baza

    def dobavi_metapodatak_baze(self):
        return self.metapodatak_baze

    def dobavi_batch(self, batch_ime):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            if batch.dobavi_atribut() == batch_ime:
                return batch
        raise RuntimeError('zatrazeni batch po atributu ne postoji u bazi')

    def dobavi_batch_po_reprezentacionom_atributu(self, reprezentacioni_atribut):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            if batch.dobavi_reprezentacioni_atribut() == reprezentacioni_atribut:
                return batch
        raise RuntimeError('zatrazeni batch po reprezentacionom atributu ne postoji u bazi')

    def broj_entiteta_batcha(self, kompozitni_podatak, filter_args=None):
        if filter_args is None:
            filter_args=[]

        kompozitni_podatak : PodatakInterface
        if kompozitni_podatak.dobavi_tip() == PodatakTip.BATCH:
            for tabela in self.metapodatak_baze['tabele']:
                if tabela['naziv'] == kompozitni_podatak.dobavi_atribut():
                    ime_procedure = tabela['operacije']['prebroj']['naziv']

                    args = tabela['operacije']['prebroj']['args']

                    if len(filter_args) > 0:
                        for filter_arg_dict in filter_args:
                            filter_arg_key = list(filter_arg_dict)[0]
                            for arg_dict in args:
                                if filter_arg_key in arg_dict:
                                    arg_dict[filter_arg_key] = filter_arg_dict[filter_arg_key]
                    else:
                        for batch_data in self.batchovi:
                            batch = batch_data['batch']
                            if batch.dobavi_atribut() == kompozitni_podatak.dobavi_atribut():
                                for recnik in args:
                                    for kljuc in batch_data['filteri']:
                                        if kljuc in recnik:
                                            recnik[kljuc] = batch_data['filteri'][kljuc]
                                break

                    # prebaci logiku na server, napisi proceduru
                    self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))
                    # print('procedura ' + ime_procedure + ' pozvana')                    
                    args = list(map(lambda recnik: recnik[list(recnik)[0]], args))
                    try:
                        self.kursor.callproc(ime_procedure, args)
                        broj_entiteta = 0
                        for result in self.kursor.stored_results():
                            broj_entiteta = result.fetchone()[0]
                        return True, broj_entiteta
                    except Exception as e:
                        if isinstance(e, mysql.connector.errors.IntegrityError):
                            e : mysql.connector.errors.IntegrityError
                            self.greska = e.msg
                        elif isinstance(e, mysql.connector.errors.DataError):
                            e : mysql.connector.errors.DataError
                            self.greska = e.msg
                        elif isinstance(e, mysql.connector.errors.ProgrammingError):
                            e : mysql.connector.errors.ProgrammingError
                            self.greska = e.msg
                        else:
                            self.greska = 'doslo je do greske - broj_entiteta_batcha'

                        self.obavesti_observere()
                        return False, 0
        return False, 0

    def postavi_filtere(self, kompozitni_podatak_batch, filteri=None):
        if filteri is None:
            filteri = {}

        kompozitni_podatak_batch : KompozitInterface
        if kompozitni_podatak_batch.dobavi_tip() == PodatakTip.BATCH:

            for batch_data in self.batchovi:
                batch = batch_data['batch']
                batch : PodatakInterface
                kompozitni_podatak_batch : PodatakInterface
                if batch.dobavi_atribut() == kompozitni_podatak_batch.dobavi_atribut():
                    
                    # da li je doslo do promene filtera?
                    if len(list(batch_data['filteri'].keys())) != len(list(filteri.keys())):
                        raise RuntimeError('filteri istog batch-a imaju razlicit broj kljuceva')

                    if list(batch_data['filteri'].keys()) != list(filteri.keys()):
                        raise RuntimeError('filteri istog batch-a imaju razlicite kljuceve')

                    filteri_su_razliciti = False
                    for kljuc in batch_data['filteri']:
                        if batch_data['filteri'][kljuc] != filteri[kljuc]:
                            filteri_su_razliciti = True

                    if filteri_su_razliciti:
                        # copy.deepcopy(filteri) ako ne stavim ovo pamti po referenci i onda se 
                        # po samom pozivu metode vec setuje potencijalno razlicit filter za batch_data['filteri']
                        batch_data['filteri'] = copy.deepcopy(filteri)

                        # doslo je do promene filtera
                        # 1) raskaci decu od batch-a
                        batch : KompozitInterface
                        for dete in batch.dobavi_decu():
                            batch.obrisi_dete(dete)
                        # 2) u batch_data deca staviti {}
                        batch_data['deca'] = {}
                        self.obavesti_observere()

                    break
                
    def dobavi_gresku(self):
        return self.greska

    # Observable override
    def obavesti_observere(self):
        for observer in self.observers:
            observer(self)
        self.greska = None