from ...pattern.creational.abstract_factory.abstract_factory import AbstractFactory
from ...podatak.model.podatak_tip import PodatakTip
from ...podatak.model.lisni_podatak import LisniPodatak
from ...podatak.model.kompozitni_podatak import KompozitniPodatak
import decimal

from datetime import date

def kreiraj_bazu(metapodatak):
    ab_factory = AbstractFactory.dobavi_instancu()
    ab_factory : AbstractFactory

    # baza
    konekcija = metapodatak['konekcija']
    host = ab_factory.kreiraj_lisni_podatak(PodatakTip.STRING)
    host.postavi_atribut('HOST')
    host.postavi_reprezentacioni_atribut('Host')
    host.postavi_obavezan(True)
    host.postavi_jedinstven(True)
    host.postavi_vrednost(konekcija['host'])

    korisnicko_ime = ab_factory.kreiraj_lisni_podatak(PodatakTip.STRING)
    korisnicko_ime.postavi_atribut('KORISNICKO_IME')
    korisnicko_ime.postavi_reprezentacioni_atribut('Korisnicko ime')
    korisnicko_ime.postavi_obavezan(True)
    korisnicko_ime.postavi_jedinstven(True)
    korisnicko_ime.postavi_vrednost(konekcija['korisnicko_ime'])

    lozinka = ab_factory.kreiraj_lisni_podatak(PodatakTip.STRING)
    lozinka.postavi_atribut('LOZINKA')
    lozinka.postavi_reprezentacioni_atribut('Lozinka')
    lozinka.postavi_obavezan(True)
    lozinka.postavi_jedinstven(True)
    lozinka.postavi_vrednost(konekcija['lozinka'])

    db = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.DATABASE)
    db.postavi_atribut(metapodatak['db_naziv'])
    db.postavi_reprezentacioni_atribut(metapodatak['db_naziv'].replace("_", " ").lower().capitalize())
    db.postavi_decu([host, korisnicko_ime, lozinka])

    # tabela - batch
    kreirani_batchovi = []
    for tabela in metapodatak['tabele']:
        batch = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.BATCH)
        batch.postavi_atribut(tabela['naziv'])
        batch.postavi_reprezentacioni_atribut(tabela['reprezentacioni_naziv'])

        # za citanje atributa specificnih za tabelu imacemo ucitan json metapodatak u coru i 
        # potom cemo pristupati njemu za generisanje entiteta, tj atributa unutar njega

        kreirani_batchovi.append({'naziv': tabela['naziv'], \
            'deca': tabela['deca'], 'roditelji': tabela['roditelji'], 'batch': batch})
    
    for batch_data in kreirani_batchovi:
        batch = batch_data['batch']

        for dete_data in batch_data['deca']['po_hijerarhiji']:
            dete = None
            dete_naziv = dete_data['naziv']
            for kreirani_batch in kreirani_batchovi:
                if kreirani_batch['naziv'] == dete_naziv:
                    dete = kreirani_batch['batch']
                    break
            if dete is None:
                raise ValueError('database_parser, dete po hijerarhiji je izgubljeno')
            batch.dodaj_dete_po_hijerarhiji(dete)

        for dete_data in batch_data['deca']['po_relaciji']:
            dete = None
            dete_naziv = dete_data['naziv']
            for kreirani_batch in kreirani_batchovi:
                if kreirani_batch['naziv'] == dete_naziv:
                    dete = kreirani_batch['batch']
                    break
            if dete is None:
                raise ValueError('database_parser, dete po relaciji je izgubljeno')
            batch.dodaj_dete_po_relaciji(dete)
            
    batchovi = [batch_data['batch'] for batch_data in kreirani_batchovi]

    for batch in batchovi:
        db.dodaj_dete(batch)

    # print(metapodatak)
    return db

def zapis_u_entitet(batch_ime, zapis, metapodatak):
    for batch in metapodatak['tabele']:
        if batch['naziv'] == batch_ime:
            ab_factory = AbstractFactory.dobavi_instancu()

            if len(zapis) == len(batch['atributi']):
                entitet = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.ENTITET)
                entitet : KompozitniPodatak
                entitet.postavi_atribut(batch_ime + '_ENTITET')
                entitet.postavi_reprezentacioni_atribut(batch_ime.lower().capitalize() + " entitet")

                deca_podaci = []
                for index, atribut in enumerate(batch['atributi']):
                    tip = _proveri_tip(atribut['tip'], zapis[index])

                    if isinstance(zapis[index], decimal.Decimal):
                        zapis = list(zapis)
                        zapis[index] = float(zapis[index])
                        zapis = tuple(zapis)

                    podatak = ab_factory.kreiraj_lisni_podatak(tip)
                    podatak : Podatak
                    podatak.postavi_atribut(atribut['naziv'])
                    podatak.postavi_reprezentacioni_atribut(atribut['reprezentacioni_naziv'])
                    podatak.postavi_jedinstven(atribut['jedinstven'])
                    podatak.postavi_obavezan(atribut['obavezan'])
                    podatak.postavi_vrednost(zapis[index])
                    deca_podaci.append(podatak)

                entitet.postavi_decu(deca_podaci)

                roditelji = batch['roditelji']['po_hijerarhiji'] + batch['roditelji']['po_relaciji']
                if len(roditelji) > 0:
                    for roditelj in roditelji:
                        atribut = roditelj['naziv']+"."+roditelj['naziv']+"_ENTITET"
                        for referenca in roditelj['reference']:

                            for dete in entitet.dobavi_decu():
                                dete : Podatak
                                if dete.dobavi_atribut() == referenca['atribut']:
                                    dete.postavi_referencira({'atribut': atribut+"."+referenca['roditeljski_atribut'], 'vrednost': None})
                                    break

                deca = batch['deca']['po_hijerarhiji'] + batch['deca']['po_relaciji']
                if len(deca) > 0:
                    for dete in deca:
                        atribut_referenciran_od = dete['naziv']+"."+dete['naziv']+"_ENTITET"
                        for referenca in dete['reference']:

                            for dete in entitet.dobavi_decu():
                                dete : Podatak
                                if dete.dobavi_atribut() == referenca['atribut']:
                                    dete : LisniPodatak
                                    dete.dodaj_atribut_referenciran_od(atribut_referenciran_od+"."+referenca['detetov_atribut'])
                                    break

                return entitet
            raise RuntimeError('zapis nije u skladu sa metapodatkom')
    raise RuntimeError('u metapodatku nema informacija o prosledjenom batch-u')

def _proveri_tip(metapodatak_tip, vrednost):
    if metapodatak_tip == 'STRING':
        if vrednost is None:
            return PodatakTip.STRING
        elif isinstance(vrednost, str):
            return PodatakTip.STRING
    elif metapodatak_tip == 'DATE':
        if vrednost is None:
            return PodatakTip.DATE
        elif isinstance(vrednost, date):
            return PodatakTip.DATE
    elif metapodatak_tip == 'BOOLEAN':
        if vrednost is None:
            return PodatakTip.BOOLEAN
        elif isinstance(vrednost, bool):
            return PodatakTip.BOOLEAN
    elif metapodatak_tip == 'BLOB':
        if vrednost is None:
            return PodatakTip.BLOB
        elif isinstance(vrednost, str):
            return PodatakTip.BLOB
    elif metapodatak_tip == 'TEXT':
        if vrednost is None:
            return PodatakTip.TEXT
        elif isinstance(vrednost, str):
            return PodatakTip.TEXT
    elif metapodatak_tip == 'NUMBER':
        if vrednost is None:
            return PodatakTip.NUMBER
        else:
            if isinstance(vrednost, decimal.Decimal):
                return PodatakTip.NUMBER
            if isinstance(vrednost, int):
                return PodatakTip.NUMBER

    raise RuntimeError('_proveri_tip - tip nije pronadjen ili vrednost nije trazenog tipa')