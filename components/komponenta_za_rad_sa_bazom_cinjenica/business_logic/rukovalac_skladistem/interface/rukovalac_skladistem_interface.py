from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...podatak.interface.kompozit_interface import KompozitInterface

class RukovalacSkladistemInterface:
    def dobavi(self, kompozitni_podatak_batch, start_index=0, end_index=0):
        kompozitni_podatak : KompozitInterface
        raise NotImplementedError('metoda dobavi nije implementirana')

    def kreiraj(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):
        raise NotImplementedError('metoda kreiraj nije implementirana')

    def izmeni(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):
        raise NotImplementedError('metoda izmeni nije implementirana')

    def obrisi(self, kompozitni_podatak_batch, kompozitni_podatak_entitet):
        raise NotImplementedError('metoda obrisi nije implementirana')

    def isprazni_batch_u_skladistu(self, kompozitni_podatak_batch):
        raise NotImplementedError('metoda isprazni_batch_u_skladistu nije implementirana')

    def dobavi_kompozitni_podatak_DATABASE(self):
        raise NotImplementedError('metoda dobavi_kompozitni_podatak_DATABASE nije implementirana')

    def dobavi_metapodatak_baze(self):
        raise NotImplementedError('metoda dobavi_metapodatak_baze nije implementirana')

    def dobavi_batch(self, batch_ime):
        raise NotImplementedError('metoda dobavi_batch nije implementirana')

    def dobavi_batch_po_reprezentacionom_atributu(self, reprezentacioni_atribut):
        raise NotImplementedError('metoda dobavi_batch_po_reprezentacionom_atributu nije implementirana')

    def broj_entiteta_batcha(self, kompozitni_podatak, filter_args=[]):
        raise NotImplementedError('metoda broj_entiteta_batcha nije implementirana')

    def postavi_filtere(self, kompozitni_podatak_batch, filteri={}):
        raise NotImplementedError('metoda postavi_filtere nije implementirana')

    def dobavi_gresku(self):
        raise NotImplementedError('metoda dobavi_gresku nije implementirana')