from .abstract_factory_interface import AbstractFactoryInterface
from ....podatak.model.lisni_podatak import LisniPodatak
from ....podatak.model.podatak_tip import PodatakTip
from ....podatak.model.kompozitni_podatak import KompozitniPodatak

class AbstractFactory(AbstractFactoryInterface):
    instanca = None

    def __init__(self):
        super(AbstractFactory, self).__init__()

    @staticmethod
    def dobavi_instancu():
        if AbstractFactory.instanca is None:
            AbstractFactory.instanca = AbstractFactory()
        return AbstractFactory.instanca

    def kreiraj_kompozitni_podatak(self, tip : PodatakTip = None):
        kompozitni_podatak = KompozitniPodatak()
        if tip is not None and tip in [PodatakTip.DATABASE, PodatakTip.BATCH, PodatakTip.ENTITET]:
            kompozitni_podatak.postavi_tip(tip)
        return kompozitni_podatak

    def kreiraj_lisni_podatak(self, tip : PodatakTip = None):
        lisni_podatak = LisniPodatak()
        if tip is not None and tip in [PodatakTip.STRING, PodatakTip.NUMBER, PodatakTip.DATE, PodatakTip.BOOLEAN, PodatakTip.BLOB, PodatakTip.TEXT]:
            lisni_podatak.postavi_tip(tip)
        return lisni_podatak

    def kreiraj_entitet(self, meta_podatak_batcha):
        batch_ime = meta_podatak_batcha['naziv']

        entitet = self.kreiraj_kompozitni_podatak(PodatakTip.ENTITET)
        entitet : KompozitniPodatak
        entitet.postavi_atribut(batch_ime + '_ENTITET')
        entitet.postavi_reprezentacioni_atribut(batch_ime.lower().capitalize() + " entitet")

        deca_podaci = []
        for index, atribut in enumerate(meta_podatak_batcha['atributi']):

            tip = None
            if atribut['tip'] == 'STRING':
                tip = PodatakTip.STRING
            elif atribut['tip'] == 'DATE':
                tip = PodatakTip.DATE
            elif atribut['tip'] == 'BOOLEAN':
                tip = PodatakTip.BOOLEAN
            elif atribut['tip'] == 'BLOB':
                tip = PodatakTip.BLOB
            elif atribut['tip'] == 'TEXT':
                tip = PodatakTip.TEXT
            elif atribut['tip'] == 'NUMBER':
                tip = PodatakTip.NUMBER
            else:
                raise RuntimeError('pokusano kreiranje entiteta koji ima atribut nepoznatog tipa')

            podatak = self.kreiraj_lisni_podatak(tip)
            podatak : Podatak
            podatak.postavi_atribut(atribut['naziv'])
            podatak.postavi_reprezentacioni_atribut(atribut['reprezentacioni_naziv'])
            podatak.postavi_jedinstven(atribut['jedinstven'])
            podatak.postavi_obavezan(atribut['obavezan'])
            deca_podaci.append(podatak)

        entitet.postavi_decu(deca_podaci)

        roditelji = meta_podatak_batcha['roditelji']['po_hijerarhiji'] + meta_podatak_batcha['roditelji']['po_relaciji']
        if len(roditelji) > 0:
            for roditelj in roditelji:
                atribut = roditelj['naziv']+"."+roditelj['naziv']+"_ENTITET"
                for referenca in roditelj['reference']:

                    for dete in entitet.dobavi_decu():
                        dete : Podatak
                        if dete.dobavi_atribut() == referenca['atribut']:
                            dete.postavi_referencira({'atribut': atribut+"."+referenca['roditeljski_atribut'], 'vrednost': None})
                            break
        
        deca = meta_podatak_batcha['deca']['po_hijerarhiji'] + meta_podatak_batcha['deca']['po_relaciji']
        if len(deca) > 0:
            for dete in deca:
                atribut_referenciran_od = dete['naziv']+"."+dete['naziv']+"_ENTITET"
                for referenca in dete['reference']:

                    for dete in entitet.dobavi_decu():
                        dete : Podatak
                        if dete.dobavi_atribut() == referenca['atribut']:
                            dete : LisniPodatak
                            dete.dodaj_atribut_referenciran_od(atribut_referenciran_od+"."+referenca['detetov_atribut'])
                            break

        return entitet