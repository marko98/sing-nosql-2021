class AbstractFactoryInterface:

    def kreiraj_kompozitni_podatak(self):
        raise NotImplementedError('metoda kreiraj_kompozitni_podatak nije implementirana')
        
    def kreiraj_lisni_podatak(self):
        raise NotImplementedError('metoda kreiraj_lisni_podatak nije implementirana')

    def kreiraj_entitet(self, meta_podatak_batcha):
        raise NotImplementedError('metoda kreiraj_entitet nije implementirana')