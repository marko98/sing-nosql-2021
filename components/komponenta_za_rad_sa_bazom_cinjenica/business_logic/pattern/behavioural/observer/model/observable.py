class Observable:

    def __init__(self):
        super(Observable, self).__init__()
        self.observers = []

    def zakaci_observer(self, observer):
        self.observers.append(observer)

    def otkaci_observer(self, observer):
        for index, _observer in enumerate(self.observers):
            if _observer == observer:
                self.observers.pop(index)

    def obavesti_observere(self):
        for observer in self.observers:
            observer(self)