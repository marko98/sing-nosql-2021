from PyQt5 import QtWidgets, uic
import os

from core.business_logic.model.komponenta import Komponenta
from .business_logic.podatak.model.podatak_tip import PodatakTip
from .business_logic.model.komponenta_kontroler import KomponentaKontroler
from .tabela_gui import TabelaGUI

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .business_logic.podatak.model.kompozitni_podatak import KompozitniPodatak

class KomponentaGUI(Komponenta):

    def __init__(self, metapodaci):
        super(KomponentaGUI, self).__init__(metapodaci)

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_bazom_cinjenica', 'ui', 'komponenta_dark.ui')
        print('ucitava se komponenta:', self, ', ui loading...')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QWidget
        # with open(os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_bazom_cinjenica', 'css', 'komponenta.css'), 'r') as fp:
        #     self.ui.setStyleSheet(fp.read())
        print('ucitana')

        # kontroler
        self.kontroler = KomponentaKontroler.dobavi_instancu(self)
        self.kontroler : KomponentaKontroler

        # ui elements
        self.tree_baza = self.ui.tree_baza
        self.tree_baza : QtWidgets.QTreeWidget
        # dodajemo header(colonu)
        # self.tree_baza.setHeaderLabels(["baze"])
        self.tree_baza.setHeaderHidden(True)
        self.tree_baza.itemSelectionChanged.connect(self._na_promenu_selekcije_u_stablu)

        self.btn_popuni_gui = self.ui.btn_popuni_gui
        self.btn_popuni_gui : QtWidgets.QPushButton
        self.btn_popuni_gui.clicked.connect(self.kontroler.popuni_tree_baza)
        self.btn_popuni_gui.hide()

        self.btn_isprazni_gui = self.ui.btn_isprazni_gui
        self.btn_isprazni_gui : QtWidgets.QPushButton
        self.btn_isprazni_gui.clicked.connect(self.kontroler.isprazni_tree_baza)
        self.btn_isprazni_gui.hide()

        self.btn_otvori_tabelu = self.ui.btn_otvori_tabelu
        self.btn_otvori_tabelu : QtWidgets.QPushButton
        self.btn_otvori_tabelu.hide()
        self.btn_otvori_tabelu.clicked.connect(self.otvori_tabelu)

        # methods
        self.kontroler.popuni_tree_baza()

    def dobavi_qwidget(self, parent = None):
        if parent is not None:
            self.ui.setParent(parent)
        return self.ui

    def popuni_tree_baza(self, kompozitni_podatak):
        kompozitni_podatak : KompozitniPodatak
        kompozitni_podatak.zakaci_se_za_stablo(self.tree_baza.invisibleRootItem(), [PodatakTip.DATABASE, PodatakTip.BATCH])

        # batch = QtWidgets.QTreeWidgetItem(self.tree_baza, ['DRZAVE'])
        # ent_0 = QtWidgets.QTreeWidgetItem(batch, ['ent_0'])
        # ent_1 = QtWidgets.QTreeWidgetItem(batch, ['ent_1'])

        # print(self.tree_baza.indexFromItem(batch).row())
        # print(self.tree_baza.indexFromItem(batch).column())
        # print(self.tree_baza.indexFromItem(ent_0).row())
        # print(self.tree_baza.indexFromItem(ent_0).column())
        # print(self.tree_baza.indexFromItem(ent_1).row())
        # print(self.tree_baza.indexFromItem(ent_1).column())

        # batch.removeChild(batch.child(batch.indexOfChild(ent_0)))

    def isprazni_tree_baza(self, kompozitni_podatak):
        kompozitni_podatak : KompozitniPodatak
        kompozitni_podatak.odkaci_se_od_stabla(self.tree_baza.invisibleRootItem(), [PodatakTip.DATABASE, PodatakTip.BATCH])

    def _na_promenu_selekcije_u_stablu(self):        
        if len(self.tree_baza.selectedItems()) > 0:
            selected_item = self.tree_baza.selectedItems()[0]

            podatak = selected_item.podatak
            podatak : KompozitniPodatak
            if podatak.dobavi_tip() == PodatakTip.BATCH:
                self.btn_otvori_tabelu.show()
                return

        self.tree_baza.clearSelection()
        self.btn_otvori_tabelu.hide()

    def otvori_tabelu(self):
        selected_indexes = self.tree_baza.selectedIndexes()
        if len(selected_indexes) == 1:
            selected_item = self.tree_baza.itemFromIndex(selected_indexes[0])

            podatak = selected_item.podatak
            podatak : KompozitniPodatak
            if podatak.dobavi_tip() == PodatakTip.BATCH:
                tabela_gui = TabelaGUI(selected_item.podatak)