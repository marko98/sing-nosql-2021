from PyQt5 import QtWidgets, uic, QtCore
import os
import json
from threading import Timer
import decimal
from datetime import datetime, date
import math
import copy

from core.business_logic.model.komponenta import Komponenta
from .business_logic.model.komponenta_kontroler import KomponentaKontroler
from .business_logic.model.podatak_tip import PodatakTip

# from typing import TYPE_CHECKING
# if TYPE_CHECKING:

class KomponentaGUI(Komponenta):

    def __init__(self, metapodaci):
        super(KomponentaGUI, self).__init__(metapodaci)

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_dokument_orijentisanom_bazom', 'ui', 'komponenta_izmenjeno_dark.ui')
        print('ucitava se komponenta:', self, ', ui loading...')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QWidget
        # with open(os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_dokument_orijentisanom_bazom', 'css', 'komponenta.css'), 'r') as fp:
        #     self.ui.setStyleSheet(fp.read())
        print('ucitana')

        # polja
        self.kontroler = KomponentaKontroler.dobavi_instancu(self)
        self.kontroler : KomponentaKontroler

        self.meta_opis_dokumeta = None
        self.filteri = {}

        self.strana_mysql = 0
        self.selektovani_red_mysql = None
        self.zapisi = []

        self.strana_mongo = 0
        self.selektovani_red_mongo = None
        self.dokumenti = []

        self.pks = []
        self.mysql_nazivi_polja_u_reprezentacione_nazive = []
        self.kolone_mysql = []
        self.kolone_mongo = []

        # selektovni red za dokument u tree widget-u
        self.selektovani_red = None
        self.dokumenti_za_kreiranje = []

        # ui elements
        self.btn_prikazi_filtere = self.ui.btn_prikazi_filtere
        self.btn_prikazi_filtere : QtWidgets.QPushButton

        self.btn_primeni_filtere = self.ui.btn_primeni_filtere
        self.btn_primeni_filtere : QtWidgets.QPushButton
        self.btn_primeni_filtere.setVisible(False)

        self.btn_ponisti_filtere = self.ui.btn_ponisti_filtere
        self.btn_ponisti_filtere : QtWidgets.QPushButton
        self.btn_ponisti_filtere.setVisible(False)

        self.scroll_area = self.ui.scroll_area
        self.scroll_area : QtWidgets.QScrollArea
        self.scroll_area.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scroll_area.setVisible(False)

        self.grid_layout = QtWidgets.QGridLayout(self.scroll_area.widget())
        self.grid_layout : QtWidgets.QGridLayout

        self.tabela_mysql = self.ui.tabela_mysql
        self.tabela_mysql : QtWidgets.QTableWidget
        self.tabela_mysql.setSortingEnabled(True)
        # self.tabela_mysql.itemSelectionChanged.connect(self._na_promenu_selekcije_u_tabeli)
        self.tabela_mysql.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)

        self.tree = self.ui.tree
        self.tree : QtWidgets.QTreeWidget()
        self.tree.setSortingEnabled(True)
        self.tree.itemSelectionChanged.connect(self._na_promenu_selekcije_u_stablu)
        self.tree.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.tree.setSortingEnabled(False)
        self.tree.setHeaderLabels(['atribut', 'vrednost'])
        self.tree.hide()

        self.sp_box_broj_strane = self.ui.sp_box_broj_strane
        self.sp_box_broj_strane : QtWidgets.QSpinBox
        self.sp_box_broj_strane.setMinimum(1)

        self.sp_box_limit = self.ui.sp_box_limit
        self.sp_box_limit : QtWidgets.QSpinBox
        self.sp_box_limit.setValue(self.kontroler.limit_mysql)
        self.sp_box_limit.setMinimum(1)
        self.sp_box_limit.setMaximum(20)

        self.btn_prva_strana = self.ui.btn_prva_strana
        self.btn_prva_strana : QtWidgets.QPushButton

        self.btn_leva_strana = self.ui.btn_leva_strana
        self.btn_leva_strana : QtWidgets.QPushButton

        self.btn_desna_strana = self.ui.btn_desna_strana
        self.btn_desna_strana : QtWidgets.QPushButton

        self.btn_poslednja_strana = self.ui.btn_poslednja_strana
        self.btn_poslednja_strana : QtWidgets.QPushButton

        self.btn_refresh = self.ui.btn_refresh
        self.btn_refresh : QtWidgets.QPushButton

        self.btn_reconnect = self.ui.btn_reconnect
        self.btn_reconnect : QtWidgets.QPushButton

        self.tabela_mongo = self.ui.tabela_mongo
        self.tabela_mongo : QtWidgets.QTableWidget
        self.tabela_mongo.setSortingEnabled(True)
        # self.tabela_mongo.itemSelectionChanged.connect(self._na_promenu_selekcije_u_tabeli)
        self.tabela_mongo.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)

        self.sp_box_broj_strane_2 = self.ui.sp_box_broj_strane_2
        self.sp_box_broj_strane_2 : QtWidgets.QSpinBox
        self.sp_box_broj_strane_2.setMinimum(1)

        self.sp_box_limit_2 = self.ui.sp_box_limit_2
        self.sp_box_limit_2 : QtWidgets.QSpinBox
        self.sp_box_limit_2.setValue(self.kontroler.limit_mongo)
        self.sp_box_limit_2.setMinimum(1)
        self.sp_box_limit_2.setMaximum(20)

        self.btn_prva_strana_2 = self.ui.btn_prva_strana_2
        self.btn_prva_strana_2 : QtWidgets.QPushButton

        self.btn_leva_strana_2 = self.ui.btn_leva_strana_2
        self.btn_leva_strana_2 : QtWidgets.QPushButton

        self.btn_desna_strana_2 = self.ui.btn_desna_strana_2
        self.btn_desna_strana_2 : QtWidgets.QPushButton

        self.btn_poslednja_strana_2 = self.ui.btn_poslednja_strana_2
        self.btn_poslednja_strana_2 : QtWidgets.QPushButton

        self.btn_refresh_2 = self.ui.btn_refresh_2
        self.btn_refresh_2 : QtWidgets.QPushButton

        self.btn_reconnect_2 = self.ui.btn_reconnect_2
        self.btn_reconnect_2 : QtWidgets.QPushButton

        self.btn_kreiraj_dokument = self.ui.btn_kreiraj_dokument
        self.btn_kreiraj_dokument : QtWidgets.QPushButton
        self.btn_kreiraj_dokument.hide()

        self.btn_izgradi_dokument = self.ui.btn_izgradi_dokument
        self.btn_izgradi_dokument : QtWidgets.QPushButton
        self.btn_izgradi_dokument.hide()

        self.btn_unesi_meta_opis_dokumenta = self.ui.btn_unesi_meta_opis_dokumenta
        self.btn_unesi_meta_opis_dokumenta : QtWidgets.QPushButton

        self.l_msg = self.ui.l_msg
        self.l_msg : QtWidgets.QLabel
        self.l_msg.setVisible(False)

        # connections
        self.btn_unesi_meta_opis_dokumenta.clicked.connect(self._dobavi_meta_opis_dokumenta)
        self.btn_prikazi_filtere.clicked.connect(self.prikazi_filtere)
        self.btn_primeni_filtere.clicked.connect(self.primeni_filtere)
        self.btn_ponisti_filtere.clicked.connect(self.ponisti_filtere)

        # 1
        self.btn_prva_strana.clicked.connect(lambda _: self.idi_na_stranu(0, False))
        self.btn_leva_strana.clicked.connect(lambda _: self.idi_na_stranu(0 if self.strana_mysql - 1 < 0 else self.strana_mysql - 1, False))
        self.btn_desna_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1 if self.strana_mysql + 1 > self.sp_box_broj_strane.maximum()-1 else self.strana_mysql + 1, False))
        self.btn_poslednja_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1, False))

        self.sp_box_broj_strane.valueChanged.connect(lambda x: self.idi_na_stranu(self.sp_box_broj_strane.value()-1, False))
        self.sp_box_limit.valueChanged.connect(lambda x: self.promeni_limit(self.sp_box_limit.value(), False))
        self.btn_refresh.clicked.connect(lambda _: self._refresh(False))
        self.btn_reconnect.clicked.connect(self.ponovi_konektovanje_na_bazu)

        # 2
        self.btn_prva_strana_2.clicked.connect(lambda _: self.idi_na_stranu(0))
        self.btn_leva_strana_2.clicked.connect(lambda _: self.idi_na_stranu(0 if self.strana_mongo - 1 < 0 else self.strana_mongo - 1))
        self.btn_desna_strana_2.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane_2.maximum()-1 if self.strana_mongo + 1 > self.sp_box_broj_strane_2.maximum()-1 else self.strana_mongo + 1))
        self.btn_poslednja_strana_2.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane_2.maximum()-1))

        self.sp_box_broj_strane_2.valueChanged.connect(lambda x: self.idi_na_stranu(self.sp_box_broj_strane_2.value()-1))
        self.sp_box_limit_2.valueChanged.connect(lambda x: self.promeni_limit(self.sp_box_limit_2.value()))
        self.btn_refresh_2.clicked.connect(lambda _: self._refresh(True))
        self.btn_reconnect_2.clicked.connect(self.ponovi_konektovanje_na_bazu)

        # 
        self.btn_kreiraj_dokument.clicked.connect(self.kreiraj_dokument)
        self.btn_izgradi_dokument.clicked.connect(self.izgradi_dokument_iz_selektovanih_zapisa)

    def promeni_limit(self, limit, rukovalac_dokumentalistickim_skladistem=True):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.kontroler.postavi_limit(limit, rukovalac_dokumentalistickim_skladistem)
            
            br_strana = self.kontroler.dobavi_broj_mogucih_strana(rukovalac_dokumentalistickim_skladistem)
            # print('br_strana', br_strana)
            if rukovalac_dokumentalistickim_skladistem:
                self.sp_box_broj_strane_2.setMaximum(br_strana)

                self.dobavi_dokumente()
                self.popuni_tabelu_mongo()
            else:
                self.sp_box_broj_strane.setMaximum(br_strana)

                self.dobavi_zapise()
                self.popuni_tabelu_mysql()

    def _proveri_broj_mogucih_strana(self):
        broj_mogucih_strana = self.kontroler.dobavi_broj_mogucih_strana(rukovalac_dokumentalistickim_skladistem=False)
        self.sp_box_broj_strane.setMaximum(1 if broj_mogucih_strana == 0 else broj_mogucih_strana)

        broj_mogucih_strana = self.kontroler.dobavi_broj_mogucih_strana(rukovalac_dokumentalistickim_skladistem=True)
        self.sp_box_broj_strane_2.setMaximum(1 if broj_mogucih_strana == 0 else broj_mogucih_strana)

    def _na_promenu_selekcije_u_stablu(self):        
        if len(self.tree.selectedItems()) > 0:
            selected_item = self.tree.selectedItems()[0]

            for i in range(self.tree.topLevelItemCount()):
                if selected_item == self.tree.topLevelItem(i):
                    self.selektovani_red = i
                    self.btn_kreiraj_dokument.show()
                    return

        self.btn_kreiraj_dokument.hide()
        self.selektovani_red = None

    def _izgradi_polja_za_filtere(self):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            height = 50
            for index, atribut in enumerate(self.meta_opis_dokumeta['parametri']):
                horizontal_layout_widget = QtWidgets.QWidget()
                horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

                horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
                horizontal_layout.setContentsMargins(0, 0, 0, 0)

                self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

                self.grid_layout.addWidget(horizontal_layout_widget, index//3, index - index//3*3)

    def _dobavi_polja_spram_tipa(self, atribut, horizontal_layout_widget, horizontal_layout):
        label = QtWidgets.QLabel(horizontal_layout_widget)
        label.setStyleSheet("color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
        label_text = atribut['reprezentacioni_atribut'] + ":"
        label.setText(label_text)
        horizontal_layout.addWidget(label)

        if atribut['tip'] == PodatakTip.STRING.value:
            lineEdit = QtWidgets.QLineEdit(horizontal_layout_widget)
            lineEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")

            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                lineEdit.setText(self.filteri[atribut['atribut']])

            lineEdit.textChanged.connect(lambda _: self.dodeli_filter(atribut, lineEdit.text()))
            horizontal_layout.addWidget(lineEdit)
        elif atribut['tip'] == PodatakTip.TEXT.value:
            textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
            textEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            textEdit.setEnabled(True)
            
            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                textEdit.setText(self.filteri[atribut['atribut']])

            textEdit.textChanged.connect(lambda _: self.dodeli_filter(atribut, textEdit.toPlainText()))             
            horizontal_layout.addWidget(textEdit)
        elif atribut['tip'] == PodatakTip.NUMBER.value:
            spinBox = QtWidgets.QSpinBox(horizontal_layout_widget)
            spinBox.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            
            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                spinBox.setValue(self.filteri[atribut['atribut']])

            spinBox.valueChanged.connect(lambda _: self.dodeli_filter(atribut, spinBox.value()))
            horizontal_layout.addWidget(spinBox)
        elif atribut['tip'] == PodatakTip.DATE.value:
            dateTimeEdit = QtWidgets.QDateTimeEdit(horizontal_layout_widget)
            dateTimeEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            # datetime.date
            # dateTimeEdit.dateChanged.connect(lambda _: print(dateTimeEdit.date().toPyDate()))
            # self.dodeli_filter(atribut, dateTimeEdit.date().toPyDate())

            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                dateTimeEdit.setDate(self.filteri[atribut['atribut']])

            dateTimeEdit.dateChanged.connect(lambda _: self.dodeli_filter(atribut, dateTimeEdit.date().toPyDate())) 
            horizontal_layout.addWidget(dateTimeEdit)
        elif atribut['tip'] == PodatakTip.BOOLEAN.value:
            checkBox = QtWidgets.QCheckBox(horizontal_layout_widget)
            checkBox.setStyleSheet("font: 8pt \"Segoe UI\";")
            
            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                checkBox.setChecked(self.filteri[atribut['atribut']])

            checkBox.stateChanged.connect(lambda _: self.dodeli_filter(atribut, checkBox.isChecked()))
            horizontal_layout.addWidget(checkBox)
       
    def dobavi_qwidget(self, parent = None):
        if parent is not None:
            self.ui.setParent(parent)
        return self.ui

    def popuni_tabelu_mysql(self):
        self.tabela_mysql.setRowCount(0)
        # while self.tabela_mysql.rowCount() > 0:
        #     self.tabela_mysql.removeRow(0)

        self.tabela_mysql.setColumnCount(len(self.kolone_mysql))
        self.tabela_mysql.setHorizontalHeaderLabels(self.kolone_mysql)

        for row in self.zapisi:
            self.tabela_mysql.setRowCount(self.tabela_mysql.rowCount() + 1)
            row_index = self.tabela_mysql.rowCount() - 1
            col_index = 0
            for col in row:
                col : Podatak

                table_item = QtWidgets.QTableWidgetItem((str(col)))
                table_item.setFlags(table_item.flags() & ~QtCore.Qt.ItemFlag.ItemIsEditable)

                self.tabela_mysql.setItem(row_index, col_index, table_item)
                col_index += 1

    def popuni_tabelu_mongo(self):
        self.tabela_mongo.setRowCount(0)
        # while self.tabela_mongo.rowCount() > 0:
        #     self.tabela_mongo.removeRow(0)

        self.tabela_mongo.setColumnCount(len(self.kolone_mongo))
        self.tabela_mongo.setHorizontalHeaderLabels(self.kolone_mongo)

        for doc in self.dokumenti:
            zapisi = self.izgradi_zapise(self.meta_opis_dokumeta['dokument'], doc)

            _zapisi = []
            for zapis in zapisi:
                _zapis = []
                for kljuc in self.kolone_mongo:
                    _zapis.append(zapis[kljuc.lower()])
                _zapisi.append(_zapis)

            for row in _zapisi:
                self.tabela_mongo.setRowCount(self.tabela_mongo.rowCount() + 1)
                row_index = self.tabela_mongo.rowCount() - 1
                col_index = 0
                for col in row:
                    col : Podatak

                    table_item = QtWidgets.QTableWidgetItem((str(col)))
                    table_item.setFlags(table_item.flags() & ~QtCore.Qt.ItemFlag.ItemIsEditable)

                    self.tabela_mongo.setItem(row_index, col_index, table_item)
                    col_index += 1

    def izgradi_zapise(self, metaopis_dokumenta, doc, zapisi=None):
        if zapisi is None:
            zapisi = [{}]

        meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
        for kljuc in doc:
            if kljuc == '_id':
                podatak = str(doc[kljuc])
                for _zapis in zapisi:
                    _zapis['id'] = podatak
            if kljuc in metaopis_dokumenta:
                podatak = doc[kljuc]
                metapodatak = metaopis_dokumenta[kljuc]
                if isinstance(podatak, dict):
                    zapisi = self.izgradi_zapise(metapodatak, podatak, zapisi)
                elif isinstance(podatak, list):
                    _zapisi = []
                    for _podatak in podatak:
                        _zapisi += self.izgradi_zapise(metapodatak, _podatak, copy.deepcopy(zapisi))
                    zapisi = _zapisi
                else:
                    if kljuc not in meta_kljucevi:
                        _kljuc = metapodatak.split('=')[1]
                        for _zapis in zapisi:
                            _zapis[_kljuc] = podatak
        
        return zapisi

    def _dobavi_meta_opis_dokumenta(self):
        atribut : ListInterface

        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        mimeTypeFilters = ["application/json"]
        dlg.setMimeTypeFilters(mimeTypeFilters)

        if dlg.exec_():
            file_path = dlg.selectedFiles()[0]
            with open(file_path, 'r') as fp:
                self.meta_opis_dokumeta = json.load(fp)
                self.kontroler.postavi_meta_opis_dokumenta(self.meta_opis_dokumeta)
                self.mysql_nazivi_polja_u_reprezentacione_nazive = self.dobavi_nazive_mysql_polja(self.meta_opis_dokumeta['dokument'])

                self.kolone_mysql = [rep_naziv.capitalize() for mysql_polje, rep_naziv in self.mysql_nazivi_polja_u_reprezentacione_nazive]
                self.kolone_mongo = ['id']+[rep_naziv.capitalize() for mysql_polje, rep_naziv in self.dobavi_nazive_kolone_za_mongo(self.meta_opis_dokumeta['dokument'])]

                self._proveri_broj_mogucih_strana()
                self._izgradi_polja_za_filtere()

                self.dobavi_dokumente()
                self.popuni_tabelu_mongo()

                self.dobavi_zapise()
                self.popuni_tabelu_mysql()
                # self.popuni_stablo()
                
                self.btn_izgradi_dokument.show()

    def dobavi_nazive_mysql_polja(self, tabela, roditeljska_tabela=None):
        naziv_tabele = tabela["_tabela_naziv"]
        tip = tabela['_tip'] if '_tip' in tabela else None
        od = tabela['_od'] if '_od' in tabela else None
        do = tabela['_do'] if '_do' in tabela else None

        meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
        fields = []
        for kljuc in tabela:
            podatak = tabela[kljuc]
            if isinstance(podatak, dict):
                _fields = self.dobavi_nazive_mysql_polja(podatak, tabela)
                fields += _fields
            else:
                if kljuc in ['_pks']:
                    for pk in podatak:
                        if pk.split('=')[0] not in self.pks:
                            self.pks.append(pk.split('=')[0])
                            fields.append([pk.split('=')[0], pk.split('=')[1]])
                if kljuc not in meta_kljucevi and podatak.split('=')[0] not in self.pks:
                    mysql_naziv_polja, reprezentacioni_naziv = podatak.split('=')
                    fields.append([mysql_naziv_polja, reprezentacioni_naziv])
        return fields

    def dobavi_nazive_kolone_za_mongo(self, tabela, roditeljska_tabela=None):
        naziv_tabele = tabela["_tabela_naziv"]
        tip = tabela['_tip'] if '_tip' in tabela else None
        od = tabela['_od'] if '_od' in tabela else None
        do = tabela['_do'] if '_do' in tabela else None

        meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
        fields = []
        for kljuc in tabela:
            podatak = tabela[kljuc]
            if isinstance(podatak, dict):
                _fields = self.dobavi_nazive_mysql_polja(podatak, tabela)
                fields += _fields
            else:
                # if kljuc in ['_pks']:
                #     for pk in podatak:
                #         if pk.split('=')[0] not in self.pks:
                #             self.pks.append(pk.split('=')[0])
                #             fields.append([pk.split('=')[0], pk.split('=')[1]])
                if kljuc not in meta_kljucevi and podatak.split('=')[0] not in self.pks:
                    mysql_naziv_polja, reprezentacioni_naziv = podatak.split('=')
                    fields.append([mysql_naziv_polja, reprezentacioni_naziv])
        return fields

    def ponovi_konektovanje_na_bazu(self):
        self.kontroler.postavi_meta_opis_dokumenta(self.meta_opis_dokumeta)
        self._proveri_broj_mogucih_strana()
        self._izgradi_polja_za_filtere()

        self.tree.hide()

        # self.dobavi_dokumente()
        # self.popuni_stablo()

    def _dobavi_start_end_index(self, rukovalac_dokumentalistickim_skladistem=True):
        if rukovalac_dokumentalistickim_skladistem:
            start_index = self.strana_mongo * self.kontroler.limit_mongo
            self.kontroler.dobavi_ukupan_broj_dokumenata()
            end_index = self.kontroler.ukupan_broj_dokumenata - 1 if start_index + self.kontroler.limit_mongo - 1 > self.kontroler.ukupan_broj_dokumenata - 1 else start_index + self.kontroler.limit_mongo - 1
        else:
            start_index = self.strana_mysql * self.kontroler.limit_mysql
            self.kontroler.dobavi_ukupan_broj_zapisa()
            end_index = self.kontroler.ukupan_broj_zapisa - 1 if start_index + self.kontroler.limit_mysql - 1 > self.kontroler.ukupan_broj_zapisa - 1 else start_index + self.kontroler.limit_mysql - 1

        return start_index, end_index

    def dobavi_dokumente(self):
        # print('idi_na_stranu', self.strana)
        start_index, end_index = self._dobavi_start_end_index()
        # print('poc. index,', start_index, ', krajnji index,', end_index)

        self.dokumenti = self.kontroler.dobavi_dokumente(start_index, end_index)

    def dobavi_zapise(self):
        # print('idi_na_stranu', self.strana)
        start_index, end_index = self._dobavi_start_end_index(False)
        # print('poc. index,', start_index, ', krajnji index,', end_index)

        self.zapisi = self.kontroler.dobavi_zapise(start_index, end_index)

    def prikazi_filtere(self):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.scroll_area.setVisible(not self.scroll_area.isVisible())
            self.btn_primeni_filtere.setVisible(not self.btn_primeni_filtere.isVisible())
            self.btn_ponisti_filtere.setVisible(not self.btn_ponisti_filtere.isVisible())

            if self.btn_prikazi_filtere.text() == 'prikazi filtere':
                self.btn_prikazi_filtere.setText('sakrij filtere')
            else:
                self.btn_prikazi_filtere.setText('prikazi filtere')

    def dodeli_filter(self, atribut, vrednost):
        if isinstance(vrednost, str):
            if vrednost == '':
                vrednost = None
            # else:
            #     vrednost = '%'+vrednost+'%'
                
        self.filteri[atribut['atribut']] = vrednost

    def primeni_filtere(self, rukovalac_dokumentalistickim_skladistem=False):
        self.kontroler.postavi_filtere(self.filteri, rukovalac_dokumentalistickim_skladistem)

        self.dobavi_zapise()
        self.popuni_tabelu_mysql()
        # self.popuni_stablo()

    def ponisti_filtere(self):
        for kljuc in self.filteri:
            self.filteri[kljuc] = None

        self.primeni_filtere()
        self._izgradi_polja_za_filtere()

    # ------------------------
    def popuni_stablo(self):
        if not self.tree.isVisible():
            self.tree.show()

        self.tree.clear()

        for index, dokument in enumerate(self.dokumenti_za_kreiranje):
            # for kljuc in dokument:
            #     _dokument = dokument[kljuc]
            self.tree : QtWidgets.QWidget
            tree_widget_item = QtWidgets.QTreeWidgetItem(self.tree, [str(index), None])
            tree_widget_item.parent = self.tree
            self.zakaci_se_za_stablo(tree_widget_item, dokument)

        if self.selektovani_red is not None:
            strana_za_selektovani_red = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
            if self.strana != strana_za_selektovani_red:
                self.selektovani_red = self.strana * self.kontroler.limit

            # print(self.selektovani_red - self.kontroler.limit * self.strana)
            self.tree.setCurrentItem(self.tree.topLevelItem(self.selektovani_red - self.kontroler.limit * self.strana))

    def zakaci_se_za_stablo(self, parent_widget, dokument):
        for kljuc in dokument:
            parent_widget : QtWidgets.QWidget
            if isinstance(dokument[kljuc], dict):
                # recnik
                tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [kljuc, None])
                tree_widget_item.parent = parent_widget
                self.zakaci_se_za_stablo(tree_widget_item, dokument[kljuc])
            elif isinstance(dokument[kljuc], list):
                # lista
                tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [kljuc, None])
                tree_widget_item.parent = parent_widget

                for index, _dokument in enumerate(dokument[kljuc]):
                    _tree_widget_item = QtWidgets.QTreeWidgetItem(tree_widget_item, [str(index), None])
                    _tree_widget_item.parent = tree_widget_item
                    self.zakaci_se_za_stablo(_tree_widget_item, _dokument)
            else:
                # jednostavan dokument
                tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [kljuc, str(dokument[kljuc])])
                tree_widget_item.parent = parent_widget

    def idi_na_stranu(self, strana, rukovalac_dokumentalistickim_skladistem=True):        
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            if strana < 0:
                strana = 0

            if rukovalac_dokumentalistickim_skladistem:
                if self.strana_mongo != strana:
                    self.strana_mongo = strana

                self.sp_box_broj_strane_2.setValue(self.strana_mongo+1)
                self.dobavi_dokumente()
                self.popuni_tabelu_mongo()
            else:
                if self.strana_mysql != strana:
                    self.strana_mysql = strana

                self.sp_box_broj_strane.setValue(self.strana_mysql+1)
                self.dobavi_zapise()
                self.popuni_tabelu_mysql()

    def _refresh(self, rukovalac_dokumentalistickim_skladistem=True):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            if rukovalac_dokumentalistickim_skladistem:
                self.kontroler.isprazni_dokumente()
                self.dobavi_dokumente()
                self.popuni_tabelu_mongo()
            else:
                self.tree.hide()

                self.kontroler.isprazni_zapise()
                self.dobavi_zapise()
                self.popuni_tabelu_mysql()

    def kreiraj_dokument(self):
        if self.selektovani_red is None:
            self.l_msg.setText('molimo odaberite dokument koji zelite da unesete u bazu')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.kontroler.kreiraj_dokument(self.dokumenti_za_kreiranje[self.selektovani_red])
            self._refresh(True)
            
            self.l_msg.setText('odabrani dokument je uspesno unet u bazu dokumenata')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()

            self.tree.clearSelection()

    def izgradi_dokument_iz_selektovanih_zapisa(self):
        selected_indexes = self.tabela_mysql.selectedIndexes()
        rows = []
        for index in selected_indexes:
            if index.row() not in rows:
                rows.append(index.row())

        selektovani_zapisi = []
        for row in rows:
            selektovani_zapisi.append(self.zapisi[row])

        _zapisi = []
        for zapis in selektovani_zapisi:
            _zapis = {}
            for index, podatak in enumerate(zapis):
                _zapis[self.kolone_mysql[index]] = podatak
            _novi_zapis = {}
            for mysql_naziv, reprezentacioni_naziv in self.mysql_nazivi_polja_u_reprezentacione_nazive:
                if reprezentacioni_naziv.capitalize() in _zapis:
                    _novi_zapis[mysql_naziv] = _zapis[reprezentacioni_naziv.capitalize()]
            _zapisi.append(_novi_zapis)

        self.dokumenti_za_kreiranje = list(map(lambda dokument_zapisi: dokument_zapisi['doc'], self.kontroler.izgradi_dokument(self.meta_opis_dokumeta['dokument'], _zapisi)))

        self.popuni_stablo()
