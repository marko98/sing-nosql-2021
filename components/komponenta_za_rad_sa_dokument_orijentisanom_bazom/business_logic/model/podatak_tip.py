# https://docs.python.org/3/library/enum.html
from enum import Enum

class PodatakTip(Enum):
    DATABASE = 'DATABASE'
    BATCH = 'BATCH'
    ENTITET = 'ENTITET'
    STRING = 'STRING'
    NUMBER = 'NUMBER'
    DATE = 'DATE'
    BOOLEAN = 'BOOLEAN'
    BLOB = 'BLOB'
    TEXT = 'TEXT'