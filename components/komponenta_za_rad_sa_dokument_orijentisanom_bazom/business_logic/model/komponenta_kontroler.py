import json
import math
import copy

from core.business_logic.model.jezgro import Jezgro
from ..pattern.behavioural.observer.model.observable import Observable
from ..rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface
from ..rukovalac_skladistem.model.rukovalac_dokumentalistickim_skladistem import RukovalacDokumentalistickimSkladistem
from ..rukovalac_skladistem.model.rukovalac_relacionim_skladistem import RukovalacRelacionimSkladistem

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...komponenta_gui import KomponentaGUI

class KomponentaKontroler(Observable):
    instanca = None

    def __init__(self, gui_komponenta):
        super(KomponentaKontroler, self).__init__()
        # print(__name__)
        self.meta_opis_dokumeta = None

        self.gui_komponenta = gui_komponenta
        self.gui_komponenta : KomponentaGUI

        self.jezgro = Jezgro.dobavi_instancu()
        self.jezgro : Observable

        self.rukovalac_dokumentalistickim_skladistem = None
        self.rukovalac_dokumentalistickim_skladistem : RukovalacSkladistemInterface

        self.rukovalac_relacionim_skladistem = None
        self.rukovalac_relacionim_skladistem : RukovalacSkladistemInterface

        self.limit_mysql = 4
        self.ukupan_broj_zapisa = 0

        self.limit_mongo = 4
        self.ukupan_broj_dokumenata = 0
    
    @staticmethod
    def dobavi_instancu(gui_komponenta=None):
        if KomponentaKontroler.instanca is not None:
            return KomponentaKontroler.instanca
        else:
            if gui_komponenta is not None:
                KomponentaKontroler.instanca = KomponentaKontroler(gui_komponenta)
                return KomponentaKontroler.instanca
        return None

    def postavi_meta_opis_dokumenta(self, meta_opis_dokumeta):
        self.meta_opis_dokumeta = meta_opis_dokumeta
        self.rukovalac_dokumentalistickim_skladistem = RukovalacDokumentalistickimSkladistem(self.meta_opis_dokumeta)
        self.rukovalac_relacionim_skladistem = RukovalacRelacionimSkladistem(self.meta_opis_dokumeta)

    def postavi_filtere(self, filteri=None, rukovalac_dokumentalistickim_skladistem=True):
        if rukovalac_dokumentalistickim_skladistem:
            self.rukovalac_dokumentalistickim_skladistem.postavi_filtere(filteri)
        else:
            self.rukovalac_relacionim_skladistem.postavi_filtere(filteri)

    def dobavi_dokumente(self, start_index, end_index):
        return self.rukovalac_dokumentalistickim_skladistem.dobavi(start_index, end_index)

    def dobavi_zapise(self, start_index, end_index):
        return self.rukovalac_relacionim_skladistem.dobavi(start_index, end_index)

    def dobavi_ukupan_broj_dokumenata(self):
        self.ukupan_broj_dokumenata = self.rukovalac_dokumentalistickim_skladistem.dobavi_ukupan_broj_entiteta()

    def dobavi_ukupan_broj_zapisa(self):
        self.ukupan_broj_zapisa = self.rukovalac_relacionim_skladistem.dobavi_ukupan_broj_entiteta()

    def dobavi_broj_mogucih_strana(self, rukovalac_dokumentalistickim_skladistem=True):
        if rukovalac_dokumentalistickim_skladistem:
            self.dobavi_ukupan_broj_dokumenata()

            br_strana = math.ceil(self.ukupan_broj_dokumenata / self.limit_mongo)
            return br_strana
        else:
            self.dobavi_ukupan_broj_zapisa()

            br_strana = math.ceil(self.ukupan_broj_zapisa / self.limit_mysql)
            return br_strana

    def postavi_limit(self, limit, rukovalac_dokumentalistickim_skladistem=True):
        if rukovalac_dokumentalistickim_skladistem:
            self.limit_mongo = limit
        else:
            self.limit_mysql = limit

    def isprazni_zapise(self):
        self.rukovalac_relacionim_skladistem.zapisi = {}

    def isprazni_dokumente(self):
        self.rukovalac_dokumentalistickim_skladistem.dokumenti = {}

    def kreiraj_dokument(self, dokument):
        self.rukovalac_dokumentalistickim_skladistem.kreiraj(copy.deepcopy(dokument))

    def izgradi_dokument(self, metaopis_dokumenta, zapisi):
        return self.rukovalac_dokumentalistickim_skladistem.izgradi_dokument(metaopis_dokumenta, zapisi)
