import mysql
import pymongo
import decimal
from datetime import datetime, date

from ..interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class RukovalacDokumentalistickimSkladistem(RukovalacSkladistemInterface):

    def __init__(self, meta_opis_dokumeta):
        self.meta_opis_dokumeta = meta_opis_dokumeta
        self.baza_dokumenata = self.meta_opis_dokumeta['baza_dokumenata']

        self.mongo_client = pymongo.MongoClient(host=self.baza_dokumenata['konekcija']['host'], port=self.baza_dokumenata['konekcija']['port'])
        self.mongo_db = self.mongo_client[self.baza_dokumenata['db_naziv']]
        self.mongo_kolekcija = self.mongo_db[self.meta_opis_dokumeta['kolekcija_naziv']]

        self.filteri = {}
        self.dokumenti = {}

        self.greska = None

    def dobavi(self, start_index=0, end_index=0):
        sf = []
        s_i = None
        for i in range(start_index, end_index + 1):
            if i in self.dokumenti:
                if s_i is not None:
                    sf.append([s_i, i-1])
                    s_i = None
            else:
                if s_i is None:
                    s_i = i
        if s_i is not None:
            sf.append([s_i, end_index])
            s_i = None

        for _sf in sf:
            _start_index = _sf[0]
            _end_index = _sf[1]
            limit = _end_index - _start_index + 1

            kursor = self.mongo_kolekcija.find().skip(_start_index).limit(limit)
            brojac = _start_index
            for dokument in kursor:
                self.dokumenti[brojac] = dokument
                brojac += 1

        dokumenti = []
        for i in range(start_index, end_index + 1):
            if i in self.dokumenti:
                dokumenti.append(self.dokumenti[i])
        return dokumenti

    def kreiraj(self, dokument):
        self.mongo_kolekcija.insert_one(dokument)

    def postavi_filtere(self, filteri=None):
        if filteri is not None:
            self.filteri = filteri
        
        self.dokumenti = {}

    def dobavi_ukupan_broj_entiteta(self):
        try:
            return self.mongo_kolekcija.find().count(True)
        except Exception as e:
            self.greska = 'doslo je do greske - broj_zapisa'

        raise RuntimeError(self.greska)

    def da_li_dokument_postoji(self, doc, docs=None):
        if docs is None:
            return False, None

        for _doc_zapisi in docs:
            _doc = _doc_zapisi['doc']
            if list(doc['_meta']['_pks']) == list(_doc['_meta']['_pks']) and \
                list(map(lambda kljuc: doc['_meta']['_pks'][kljuc], list(doc['_meta']['_pks']))) == list(map(lambda kljuc: _doc['_meta']['_pks'][kljuc], list(_doc['_meta']['_pks']))):
                # exists
                return True, _doc_zapisi
        return False, None

    def izgradi_dokument(self, metaopis_dokumenta, zapisi, roditeljski_doc=None):
        docs = []
        # docs_i_zapisi = {}

        for zapis_recnik in zapisi:
            doc = {"_meta": {"_pks": {}}}
            for pk in metaopis_dokumenta['_pks']:
                doc['_meta']["_pks"][pk.split('=')[0]] = zapis_recnik[pk.split('=')[0]]

            exists, original_doc_zapisi = self.da_li_dokument_postoji(doc, docs)
            if not exists:
                docs.append({'doc':doc, 'zapisi': [zapis_recnik]})
            else:
                original_doc_zapisi['zapisi'].append(zapis_recnik)

        for doc_zapisi in docs:
            # dodaj ostala polja
            doc = doc_zapisi['doc']

            # if len(doc_zapisi['zapisi']) > 1:
            zapis_recnik = doc_zapisi['zapisi'][0]

            meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
            for kljuc in metaopis_dokumenta:
                podatak = metaopis_dokumenta[kljuc]
                if isinstance(podatak, dict):
                    if podatak['_tip'] == "RECNIK":
                        doc_child = self.izgradi_dokument(podatak, doc_zapisi['zapisi'], doc)[0]['doc']
                        doc[kljuc] = doc_child
                    else:
                        # lista
                        doc[kljuc] = list(map(lambda doc_zapisi: doc_zapisi['doc'], self.izgradi_dokument(podatak, doc_zapisi['zapisi'], doc)))
                else:
                    if kljuc in ['_tabela_naziv']:
                        doc['_meta'][kljuc] = podatak

                        if roditeljski_doc is not None:
                            doc['_meta']['referencira'] = roditeljski_doc['_meta']['_tabela_naziv']
                    elif kljuc not in meta_kljucevi:
                        doc[kljuc] = zapis_recnik[podatak.split("=")[0]]
        return docs

    def obradi_podatak_iz_mysql(self, podatak):
        if isinstance(podatak, decimal.Decimal):
            return float(podatak)
        elif isinstance(podatak, date):
            # podatak : date
            # return "{}-{}-{}".format(podatak.year, podatak.month, podatak.day)
            return datetime(podatak.year, podatak.month, podatak.day)
        return podatak

    def dobavi_nazive_mysql_polja(self, tabela, roditeljska_tabela=None):
        naziv_tabele = tabela["_tabela_naziv"]
        tip = tabela['_tip'] if '_tip' in tabela else None
        od = tabela['_od'] if '_od' in tabela else None
        do = tabela['_do'] if '_do' in tabela else None

        meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
        fields = []
        for kljuc in tabela:
            podatak = tabela[kljuc]
            if isinstance(podatak, dict):
                _fields = self.dobavi_nazive_mysql_polja(podatak, tabela)
                fields += _fields
            else:
                if kljuc in ['_pks']:
                    for pk in podatak:
                        if pk.split('=')[0] not in self.pks:
                            self.pks.append(pk.split('=')[0])
                            fields.append(pk.split('=')[0])
                if kljuc not in meta_kljucevi and podatak.split('=')[0] not in self.pks:
                    fields.append(podatak.split('=')[0])
        return fields
