import mysql
import decimal
from datetime import datetime, date

from ..interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class RukovalacRelacionimSkladistem(RukovalacSkladistemInterface):

    def __init__(self, meta_opis_dokumeta):
        self.meta_opis_dokumeta = meta_opis_dokumeta
        self.baza_cinjenica = self.meta_opis_dokumeta['baza_cinjenica']

        # self.pks = []
        # self.nazivi_mysql_polja = self.dobavi_nazive_mysql_polja(self.meta_opis_dokumeta['dokument'])

        self.mysql_konekcija = mysql.connector.connect(host=self.baza_cinjenica['konekcija']['host'],
                                                        user=self.baza_cinjenica['konekcija']['korisnicko_ime'],
                                                        password=self.baza_cinjenica['konekcija']['lozinka'],)
        self.kursor = self.mysql_konekcija.cursor()
        self.kursor : mysql.connector.cursor.MySQLCursor

        self.filteri = {}
        self.zapisi = {}

        self.greska = None

    def dobavi(self, start_index=0, end_index=0):
        sf = []
        s_i = None
        for i in range(start_index, end_index + 1):
            if i in self.zapisi:
                if s_i is not None:
                    sf.append([s_i, i-1])
                    s_i = None
            else:
                if s_i is None:
                    s_i = i
        if s_i is not None:
            sf.append([s_i, end_index])
            s_i = None

        self.kursor.execute('USE {}'.format(self.baza_cinjenica['db_naziv'],))
        for _sf in sf:
            _start_index = _sf[0]
            _end_index = _sf[1]

            dobavi_procedura = self.meta_opis_dokumeta['operacije']['dobavi']
            naziv_procedure = dobavi_procedura['naziv']
            args = dobavi_procedura['args']

            # limit
            args[-2]['LIMIT'] = _end_index - _start_index + 1
            # offset
            args[-1]['OFFSET'] = _start_index

            for recnik in args[:-2]:
                kljuc = list(recnik)[0]
                if kljuc in self.filteri:
                    recnik[kljuc] = self.filteri[kljuc]
            
            args = list(map(lambda recnik: recnik[list(recnik)[0]], args))
            try:
                print('procedura ' + naziv_procedure + ' pozvana')
                self.kursor.callproc(naziv_procedure, args)
            except Exception as e:
                if isinstance(e, mysql.connector.errors.IntegrityError):
                    e : mysql.connector.errors.IntegrityError
                    self.greska = e.msg
                elif isinstance(e, mysql.connector.errors.DataError):
                    e : mysql.connector.errors.DataError
                    self.greska = e.msg
                elif isinstance(e, mysql.connector.errors.ProgrammingError):
                    e : mysql.connector.errors.ProgrammingError
                    self.greska = e.msg
                else:
                    self.greska = 'doslo je do greske - dobavi'
            
                raise RuntimeError(self.greska)

            for result in self.kursor.stored_results():
                brojac = _start_index
                for zapis in result.fetchall():
                    zapis = list(map(lambda podatak: self.obradi_podatak_iz_mysql(podatak), list(zapis)))
                    self.zapisi[brojac] = zapis
                    brojac += 1

        zapisi = []
        for i in range(start_index, end_index + 1):
            if i in self.zapisi:
                zapisi.append(self.zapisi[i])
        return zapisi

    def obradi_podatak_iz_mysql(self, podatak):
        if isinstance(podatak, decimal.Decimal):
            return float(podatak)
        elif isinstance(podatak, date):
            # podatak : date
            # return "{}-{}-{}".format(podatak.year, podatak.month, podatak.day)
            return datetime(podatak.year, podatak.month, podatak.day)
        return podatak

    def postavi_filtere(self, filteri=None):
        if filteri is not None:
            self.filteri = filteri
        
        self.zapisi = {}
    
    def dobavi_ukupan_broj_entiteta(self):
        prebroj_procedura = self.meta_opis_dokumeta['operacije']['prebroj']
        naziv_procedure = prebroj_procedura['naziv']
        args = prebroj_procedura['args']

        # prebaci logiku na server, napisi proceduru
        self.kursor.execute('USE {}'.format(self.baza_cinjenica['db_naziv'],))
        # print('procedura ' + ime_procedure + ' pozvana')                    
        args = list(map(lambda recnik: recnik[list(recnik)[0]], args))
        try:
            self.kursor.callproc(naziv_procedure, args)
            broj_zapisa = 0
            for result in self.kursor.stored_results():
                broj_zapisa = result.fetchone()[0]
            return broj_zapisa
        except Exception as e:
            if isinstance(e, mysql.connector.errors.IntegrityError):
                e : mysql.connector.errors.IntegrityError
                self.greska = e.msg
            elif isinstance(e, mysql.connector.errors.DataError):
                e : mysql.connector.errors.DataError
                self.greska = e.msg
            elif isinstance(e, mysql.connector.errors.ProgrammingError):
                e : mysql.connector.errors.ProgrammingError
                self.greska = e.msg
            else:
                self.greska = 'doslo je do greske - broj_zapisa'

        raise RuntimeError(self.greska)
