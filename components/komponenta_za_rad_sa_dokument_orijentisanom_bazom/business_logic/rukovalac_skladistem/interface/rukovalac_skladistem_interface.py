# from typing import TYPE_CHECKING
# if TYPE_CHECKING:

class RukovalacSkladistemInterface:
    def dobavi(self, start_index=0, end_index=0):
        raise NotImplementedError('metoda dobavi nije implementirana')

    def postavi_filtere(self, filteri=None):
        raise NotImplementedError('metoda postavi_filtere nije implementirana')
    
    def dobavi_ukupan_broj_entiteta(self):
        raise NotImplementedError('metoda dobavi_ukupan_broj_entiteta nije implementirana')

    def kreiraj(self, entitet):
        raise NotImplementedError('metoda kreiraj nije implementirana')

    # def isprazni_batch_u_skladistu(self, kompozitni_podatak_batch):
    #     raise NotImplementedError('metoda isprazni_batch_u_skladistu nije implementirana')

    # def dobavi_metapodatak_baze(self):
    #     raise NotImplementedError('metoda dobavi_metapodatak_baze nije implementirana')

    # def broj_entiteta_batcha(self, kompozitni_podatak, filter_args=[]):
    #     raise NotImplementedError('metoda broj_entiteta_batcha nije implementirana')