from PyQt5 import QtWidgets, uic
import os

from .business_logic.model.jezgro import Jezgro

class JezgroGUI:

    def __init__(self):
        super(JezgroGUI, self).__init__()

        self.ui_path = os.path.join(os.getcwd(), 'core', 'ui', 'jezgro_dark.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QMainWindow

        self.jezgro = Jezgro.dobavi_instancu()

        # ui
        self.tab_widget = self.ui.tab_widget
        self.tab_widget : QtWidgets.QTabWidget

        self.baza_cinjenica = self.ui.baza_cinjenica
        self.baza_cinjenica : QtWidgets.QWidget
        self.layout_baza_cinjenica = QtWidgets.QVBoxLayout(self.baza_cinjenica)
        self.layout_baza_cinjenica.addWidget(self.jezgro.obavesti('projekat.komponenta_za_rad_sa_bazom_cinjenica'))

        self.dokument_orijentisana_baza = self.ui.dokument_orijentisana_baza
        self.dokument_orijentisana_baza : QtWidgets.QWidget
        self.layout_dokument_orijentisana_baza = QtWidgets.QVBoxLayout(self.dokument_orijentisana_baza)
        self.layout_dokument_orijentisana_baza.addWidget(self.jezgro.obavesti('projekat.komponenta_za_rad_sa_dokument_orijentisanom_bazom'))

        self.ui.show()