import os
import json
import importlib
import inspect

# pylint: disable=unused-variable

class RegistarKomponenti:
    instanca = None

    def __init__(self):
        super(RegistarKomponenti, self).__init__()

        self.komponente = []
        self._dobavi_podatke_o_komponentama()

    @staticmethod
    def dobavi_instancu():
        if RegistarKomponenti.instanca is None:
            RegistarKomponenti.instanca = RegistarKomponenti()
        return RegistarKomponenti.instanca

    # def _ukljuci_komponente(self, putanja='components'):
    #     komponente = []
    #     print('Ucitane komponente:')

    #     for root, folderi, fajlovi in os.walk(putanja):
    #         for folder in folderi:
    #             folder_putanja = os.path.join(putanja, folder)
    #             metapodaci_putanja = os.path.join(folder_putanja, 'metapodaci.json')
    #             komponenta_putanja = os.path.join(folder_putanja, 'komponenta').replace(os.path.sep, '.')
    #             if os.path.exists(metapodaci_putanja):
    #                 with open(metapodaci_putanja, 'r') as fp:
    #                     metapodaci = json.load(fp)
    #                     komponenta_modul = importlib.import_module(komponenta_putanja)
                        
    #                     clanovi = inspect.getmembers(komponenta_modul)
    #                     if clanovi[0][0] == 'Komponenta':
    #                         komponenta = clanovi[1][1](metapodaci)
    #                         print('\t', komponenta)                     
    #                         komponente.append(komponenta)
    #                     else:
    #                         raise ValueError('Klasa Komponenta nije pronadjena')

    #                     # pronadjena = False
    #                     # for clan in inspect.getmembers(komponenta_modul):
    #                     #     if clan[0] == 'Komponenta':
    #                     #         komponenta = komponenta_modul.Komponenta(metapodaci)
    #                     #         print(komponenta)
    #                     #         pronadjena = True
    #                     #         komponente.append(komponenta)
    #                     # if not pronadjena:
    #                     #     raise ValueError('Klasa Komponenta nije pronadjena')
    #         break
    
    #     return komponente

    def _dobavi_podatke_o_komponentama(self, putanja='components'):
        podaci_o_komponentama = []
        print('dobavljene komponente:')

        for root, folderi, fajlovi in os.walk(putanja):
            for folder in folderi:
                folder_putanja = os.path.join(putanja, folder)
                metapodaci_putanja = os.path.join(folder_putanja, 'metapodaci.json')
                komponenta_putanja = os.path.join(folder_putanja, 'komponenta_gui').replace(os.path.sep, '.')
                if os.path.exists(metapodaci_putanja):
                    with open(metapodaci_putanja, 'r') as fp:
                        metapodaci = json.load(fp)
                        podaci_o_komponentama.append({"metapodaci": metapodaci, "komponenta_putanja": komponenta_putanja, 'ucitana': False, "instanca": None})
                        print('\t', metapodaci['ime'])
            break
    
        self.komponente = podaci_o_komponentama

    def _ucitaj_komponentu(self, podaci_o_komponenti):
        metapodaci, komponenta_putanja = podaci_o_komponenti['metapodaci'], podaci_o_komponenti['komponenta_putanja']

        komponenta_modul = importlib.import_module(komponenta_putanja)
        clanovi = inspect.getmembers(komponenta_modul)
        if 'Komponenta' in [clan[0] for clan in clanovi]:
            for clan in clanovi:
                if 'KomponentaGUI' in clan[0]:
                    komponenta = clan[1](metapodaci)
                    # print('Ucitana komponenta', komponenta)
                    podaci_o_komponenti['ucitana'] = True
                    podaci_o_komponenti['instanca'] = komponenta
                    break
        else:
            raise ValueError('Klasa Komponenta nije pronadjena')


    def iskljuci_komponentu(self, simbolicko_ime):
        try:
            podaci_o_komponenti = list(filter(lambda komponenta: komponenta["metapodaci"]["simbolicko_ime"] == simbolicko_ime, self.komponente))[0]
            podaci_o_komponenti['ucitana'] = False
            podaci_o_komponenti['instanca'] = None
        except IndexError as _:
            print('Komponenta sa simbolickim imenom: {} nije pronadjena'.format(simbolicko_ime,))

    def dobavi_komponentu(self, simbolicko_ime):
        try:
            podaci_o_komponenti = list(filter(lambda komponenta: komponenta["metapodaci"]["simbolicko_ime"] == simbolicko_ime, self.komponente))[0]
            if podaci_o_komponenti['ucitana'] is False:
                komponenta = self._ucitaj_komponentu(podaci_o_komponenti)
            return podaci_o_komponenti["instanca"]
        except IndexError as _:
            print('Komponenta sa simbolickim imenom: {} nije pronadjena'.format(simbolicko_ime,))