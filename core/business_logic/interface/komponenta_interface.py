class KomponentaInterface:
    @property
    def ime(self):
        raise NotImplementedError('metoda ime nije implementirana')

    @property
    def opis(self):
        raise NotImplementedError('metoda opis nije implementirana')
    
    @property
    def simbolicko_ime(self):
        raise NotImplementedError('metoda simbolicko_ime nije implementirana')

    @property
    def verzija_jezgra(self):
        raise NotImplementedError('metoda verzija_jezgra nije implementirana')

    @property
    def verzija(self):
        raise NotImplementedError('metoda verzija nije implementirana')

    @property
    def kategorija(self):
        raise NotImplementedError('metoda kategorija nije implementirana')

    @property
    def odobrena(self):
        raise NotImplementedError('metoda odobrena nije implementirana')

    @odobrena.setter
    def odobrena(self, vrednost):
        raise NotImplementedError('metoda odobrena nije implementirana')

    def dobavi_qwidget(self):
        raise NotImplementedError('metoda dobavi_qwidget nije implementirana')