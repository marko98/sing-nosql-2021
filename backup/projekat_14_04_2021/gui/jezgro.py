from PyQt5 import uic
import os

class JezgroGUI:

    def __init__(self):
        super(JezgroGUI, self).__init__()

        self.ui_path = os.path.join(os.getcwd(), 'gui', 'jezgro.ui')
        self.ui = None

    def load_ui(self):
        self.ui = uic.loadUi(self.ui_path)