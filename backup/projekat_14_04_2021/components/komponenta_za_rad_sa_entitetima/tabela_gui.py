from PyQt5 import QtWidgets, uic
import os

from business_logic.podatak.model.kompozitni_podatak_tip import KompozitniPodatakTip
from business_logic.podatak.model.podatak_tip import PodatakTip
from .business_logic.model.tabela_gui_kontroler import TabelaGUIKontroler

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from PyQt5 import QtGui
    from ...business_logic.podatak.model.kompozitni_podatak import KompozitniPodatak
    from ...business_logic.podatak.model.podatak import Podatak

class TabelaGUI:
    instance = []

    def __init__(self, kompozitni_podatak):
        super(TabelaGUI, self).__init__()

        kompozitni_podatak : KompozitniPodatak
        if kompozitni_podatak.dobavi_tip() != KompozitniPodatakTip.BATCH:
            raise ValueError('pokusano je instanciranje klase TabelaGUI sa kompozitnim podatkom koji nije tipa BATCH')
        
        self.kompozitni_podatak = kompozitni_podatak

        self.kontroler = TabelaGUIKontroler(self)

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_entitetima', 'ui', 'tabel.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QDialog

        # ui elements
        self.cb_roditelji = self.ui.cb_roditelji
        self.cb_roditelji : QtWidgets.QComboBox

        self.cb_deca = self.ui.cb_deca
        self.cb_deca : QtWidgets.QComboBox

        self.btn_otvori_roditelj_tabelu = self.ui.btn_otvori_roditelj_tabelu
        self.btn_otvori_roditelj_tabelu : QtWidgets.QPushButton

        self.btn_otvori_dete_tabelu = self.ui.btn_otvori_dete_tabelu
        self.btn_otvori_dete_tabelu : QtWidgets.QPushButton

        self.table = self.ui.table
        self.table : QtWidgets.QTableWidget
        self.table.setSortingEnabled(True)

        self.btn_zatvori_dialog = self.ui.btn_zatvori_dialog
        self.btn_zatvori_dialog : QtWidgets.QPushButton
        
        self.label_naziv_batcha = self.ui.label_naziv_batcha
        self.label_naziv_batcha : QtWidgets.QLabel
        self.label_naziv_batcha.setText(self.kompozitni_podatak.dobavi_reprezentacioni_atribut())

        self.btn_obrisi = self.ui.btn_obrisi
        self.btn_obrisi : QtWidgets.QPushButton

        # methods
        self.popuni_cb_roditelji()
        self.popuni_cb_deca()
        self.popuni_tabelu()

        # connections
        self.btn_otvori_roditelj_tabelu.clicked.connect(lambda _: self.otvori_tabelu(self.cb_roditelji.currentText()))
        self.btn_otvori_dete_tabelu.clicked.connect(lambda _: self.otvori_tabelu(self.cb_deca.currentText()))
        self.btn_zatvori_dialog.clicked.connect(self.zatvori_dialog)
        self.btn_obrisi.clicked.connect(self.obrisi)

        TabelaGUI.instance.append(self)

        self.ui.closeEvent = self.zatvori_dialog
        self.ui.exec()
        
    def zatvori_dialog(self, closeEvent):
        closeEvent : QtGui.QCloseEvent

        self.table.selectAll()
        items = self.table.selectedItems()
        for item in items:
            podatak = item.podatak
            podatak : Podatak
            podatak.widgets = list(filter(lambda widget_data: widget_data['widget'] != item, podatak.widgets))

        for index, tabela_gui in enumerate(TabelaGUI.instance):
            if tabela_gui == self:
                TabelaGUI.instance.pop(index)
        self.ui.close()

    def popuni_cb_roditelji(self):
        roditelji = self.kompozitni_podatak.dobavi_roditelje_po_hijerarhiji() + self.kompozitni_podatak.dobavi_roditelje_po_relaciji()

        # proverimo da li vec postoji instanca sa nekim od roditelja, ako postoji necemo ga prikazati medju roditeljima
        otvoreni_roditelji = list(map(lambda x: x.kompozitni_podatak, TabelaGUI.instance))
        roditelji = list(filter(lambda x: x not in otvoreni_roditelji, roditelji))

        if len(roditelji) == 0:
            self.ui.label_roditelji.setVisible(False)
            self.cb_roditelji.setVisible(False)
            self.btn_otvori_roditelj_tabelu.setVisible(False)

        self.cb_roditelji.addItems(list(map(lambda x: x.dobavi_reprezentacioni_atribut(), roditelji)))

    def popuni_cb_deca(self):
        deca = self.kompozitni_podatak.dobavi_decu_po_hijerarhiji() + self.kompozitni_podatak.dobavi_decu_po_relaciji()

        # proverimo da li vec postoji instanca sa nekim od dece, ako postoji necemo ga prikazati medju decom
        otvorena_deca = list(map(lambda x: x.kompozitni_podatak, TabelaGUI.instance))
        deca = list(filter(lambda x: x not in otvorena_deca, deca))

        if len(deca) == 0:
            self.ui.label_deca.setVisible(False)
            self.cb_deca.setVisible(False)
            self.btn_otvori_dete_tabelu.setVisible(False)

        self.cb_deca.addItems(list(map(lambda x: x.dobavi_reprezentacioni_atribut(), deca)))

    def popuni_tabelu(self):
        metapodatak = self.kontroler.dobavi_metapodatak_baze()

        columns = []
        for batch in metapodatak['tabele']:
            if batch['naziv'] == self.kompozitni_podatak.dobavi_atribut():
                columns = list(map(lambda x: x['reprezentacioni_naziv'], batch['atributi']))

                self.table.setColumnCount(len(columns))
                self.table.setHorizontalHeaderLabels(columns)

                rows = []
                for entitet in self.kompozitni_podatak.dobavi_decu():
                    entitet : KompozitniPodatak
                    if entitet.dobavi_tip() != KompozitniPodatakTip.ENTITET:
                        raise RuntimeError("entitet nije tipa ENTITET")

                    row = entitet.dobavi_decu()
                    # for atribut_podatak in entitet.dobavi_decu():
                    #     atribut_podatak : Podatak
                    #     row_data.append(atribut_podatak.dobavi_vrednost())

                    rows.append(row)

                for row in rows:
                    self.table.setRowCount(self.table.rowCount() + 1)
                    row_index = self.table.rowCount() - 1
                    col_index = 0
                    for col in row:
                        col : Podatak

                        table_item = QtWidgets.QTableWidgetItem((str(col.dobavi_vrednost())))
                        col.widgets.append({'widget': table_item, 'tipovi': None})
                        table_item.podatak = col

                        self.table.setItem(row_index, col_index, table_item)
                        col_index += 1

                return

        raise RuntimeError('kompozitni podatak nije pronadjen u metapodatku baze')

    def otvori_tabelu(self, batch_reprezentacioni_naziv):
        batch = self.kontroler.dobavi_batch_po_reprezentacionom_atributu(batch_reprezentacioni_naziv)
        TabelaGUI(batch)

    def obrisi(self):
        entiteti = []
        
        selected_items = self.table.selectedItems()
        for table_item in selected_items:
            podatak = table_item.podatak
            podatak : Podatak
            
            entitet = podatak.dobavi_roditelja()
            if entitet.dobavi_tip() != KompozitniPodatakTip.ENTITET:
                raise RuntimeError('u tabeli se nalazi podatak koji za roditelja nema kompozitni podatak tipa ENTITET')
            
            if entitet not in entiteti:
                entiteti.append(entitet)



        print('obrisi')
