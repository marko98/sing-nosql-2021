from core.model.jezgro import Jezgro

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...tabela_gui import TabelaGUI
    from business_logic.pattern.behavioural.observer.model.observable import Observable
    from business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class TabelaGUIKontroler:
    def __init__(self, tabela_gui):
        super(TabelaGUIKontroler, self).__init__()
        # print(__name__)

        self.tabela_gui = tabela_gui
        self.tabela_gui : TabelaGUI        

        self.jezgro = Jezgro.dobavi_instancu()
        self.jezgro : Observable
        self.jezgro.zakaci_observer(lambda x : self.proveri_rukovaoca_skladistem(x.rukovalac_skladistem))

        self.rukovalac_skladistem = self.jezgro.rukovalac_skladistem
        self.rukovalac_skladistem : RukovalacSkladistemInterface

    def zatvori_dialog(self):
        self.tabela_gui.zatvori_dialog()

    def proveri_rukovaoca_skladistem(self, rukovalac_skladistem):
        if rukovalac_skladistem != self.rukovalac_skladistem:
            self.zatvori_dialog()

    def dobavi_metapodatak_baze(self):
        return self.rukovalac_skladistem.dobavi_metapodatak_baze()

    def dobavi_batch(self, batch_ime):
        return self.rukovalac_skladistem.dobavi_batch(batch_ime)

    def dobavi_batch_po_reprezentacionom_atributu(self, batch_reprezentacioni_naziv):
        return self.rukovalac_skladistem.dobavi_batch_po_reprezentacionom_atributu(batch_reprezentacioni_naziv)