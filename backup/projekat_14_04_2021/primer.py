import mysql.connector

mydb = mysql.connector.connect(
    host='localhost',
    user='root',
    password='root'
)

mycursor = mydb.cursor()

mycursor.execute('USE lazna_db')

mycursor.execute("SHOW TABLES")
for x in mycursor:
    print(x)
print('---------')

# unutar workbench-a
# SELECT * FROM lazna_db.region ORDER BY RE_NAZIV LIMIT 2, 2
# SELECT * FROM lazna_db.region LIMIT 0, 2

# 'SELECT * FROM region ORDER BY RE_NAZIV ASC LIMIT 0, 2'
# 'SELECT * FROM region ORDER BY RE_NAZIV DESC LIMIT 0, 2'
sql = 'SELECT * FROM region ORDER BY RE_NAZIV ASC LIMIT 0, 2'
# sql = "SELECT * FROM naseljeno_mesto where NM_NAZIV = 'APATIN'"
mycursor.execute(sql)
result = mycursor.fetchall()
print(result)

sql = 'SELECT * FROM region LIMIT 2, 2'
mycursor.execute(sql)
print(mycursor.fetchall())
print('done')