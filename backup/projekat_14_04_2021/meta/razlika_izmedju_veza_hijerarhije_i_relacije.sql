drop schema IF EXISTS `lazna_db`;

create schema IF NOT EXISTS `lazna_db` DEFAULT CHARACTER SET utf8 ;
USE `lazna_db` ;

create table DRZAVA 
(
 DR_IDENTIFIKATOR char(3) not null,
 DR_NAZIV varchar(10),
 DR_GODINA_OSNIVANJA date,
 primary key (DR_IDENTIFIKATOR)
);

create table NASELJENO_MESTO 
(
 NM_IDENTIFIKATOR char(5) not null,
 DR_IDENTIFIKATOR char(3) not null,
 NM_NAZIV varchar(10),
 primary key (NM_IDENTIFIKATOR, DR_IDENTIFIKATOR)
);

create table REGION 
(
 RE_IDENTIFIKATOR char(10) not null,
 DR_IDENTIFIKATOR char(3),
 RE_NAZIV varchar(10),
 primary key (RE_IDENTIFIKATOR)
);

alter table NASELJENO_MESTO add constraint FK_NASELJENO_MESTO_0 foreign key (DR_IDENTIFIKATOR) references DRZAVA (DR_IDENTIFIKATOR);

alter table REGION add constraint FK_REGION_0 foreign key (DR_IDENTIFIKATOR) references DRZAVA (DR_IDENTIFIKATOR);
