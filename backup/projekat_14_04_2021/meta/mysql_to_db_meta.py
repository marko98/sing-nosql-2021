# parser je osetljiv na strukturu sql file-a!!!

import json
import copy

procedure = []

lista_skracenica = []
def most_frequent(List):
    return max(set(List), key = List.count)

def popuni_lista_skracenica(tables):
    for table in tables:
        table_name = table.pop(0).replace("create table ", "")
        table.pop(0)
        table.pop()

        reprezentacioni_naziv_tabele = table_name.replace('_', ' ').lower().capitalize()

        skracenice = []
        for line in table:
            parts = line.split(' ')
            parts = list(filter(lambda x: x != '', parts))

            reprezentacioni_naziv = parts[0].split('_')
            skracenice.append(reprezentacioni_naziv.pop(0))

        lista_skracenica.append({'skracenica': most_frequent(skracenice), 'reprezentacioni_naziv_tabele': reprezentacioni_naziv_tabele})

def get_type(tip):
    # types: string, date, boolean, number, blob, text

    _tip = 'STRING'

    if 'date' in tip:
        _tip = 'DATE'
    elif 'bool' in tip:
        _tip = 'BOOLEAN'
    elif 'blob' in tip:
        _tip = 'BLOB'
    elif 'text' in tip:
        _tip = 'TEXT'
    elif any(_t in tip for _t in ['int', 'numeric', 'smallint', 'bigint', 'decimal']):
        _tip = 'NUMBER'

    return _tip

def get_table(table : list):
    table_name = table.pop(0).replace("create table ", "")
    
    table.pop(0)
    table.pop()

    primary_keys = []
    if 'primary key' in table[-1]:
        primary_keys = table.pop().split('(')[1].replace(')', '').split(', ')

    # -----------------------------
    skracenice = []
    for line in table:
        parts = line.split(' ')
        parts = list(filter(lambda x: x != '', parts))

        reprezentacioni_naziv = parts[0].split('_')
        skracenice.append(reprezentacioni_naziv.pop(0))

    skracenica_tabele = most_frequent(skracenice)
    # -----------------------------

    atributi = []
    for line in table:
        atr = {'obavezan': False, 'jedinstven': False}

        parts = line.split(' ')
        parts = list(filter(lambda x: x != '', parts))

        for index, part in enumerate(parts):
            if index == 0:
                atr['naziv'] = part

                reprezentacioni_naziv = part.split('_')
                skracenica = reprezentacioni_naziv.pop(0)
                reprezentacioni_naziv = ' '.join(reprezentacioni_naziv).lower().capitalize()

                if skracenica != skracenica_tabele:
                    for _sk in lista_skracenica:
                        if _sk['skracenica'] == skracenica:
                            reprezentacioni_naziv += ' - ' + _sk['reprezentacioni_naziv_tabele']
                            break

                atr['reprezentacioni_naziv'] = reprezentacioni_naziv
            elif index == 1:
                atr['tip'] = get_type(part)
            elif index == 2 or index == 3:
                atr['obavezan'] = True

        atributi.append(atr)

    for primary_key in primary_keys:
        for atr in atributi:
            if atr['naziv'] == primary_key:
                atr['jedinstven'] = True

    reprezentacioni_naziv = table_name.replace('_', ' ').lower().capitalize()
    

    return {"naziv": table_name, 'reprezentacioni_naziv': reprezentacioni_naziv, 'atributi': atributi, \
        'primarni_kljucevi': primary_keys, 'deca': {'po_hijerarhiji': [], 'po_relaciji': []}, 'roditelji': {'po_hijerarhiji': [], 'po_relaciji': []}}
    
def add_parent_child_relation(foreign_key_line, tables : list):
    # da li ima potrebe staviti po kom atributu su povezani, jer kako bismo to uradili u dokumentalistickoj bazi?
    child_table_name = foreign_key_line.split(' ')[2]
    child_table_referencing_attributes = foreign_key_line.split('(')[1].split(')')[0].split(', ')
    parent_table_name = foreign_key_line.split('references ')[1].split(' ')[0]
    parent_table_referenced_attributes = foreign_key_line.split('(')[2].split(')')[0].split(', ')

    brojac = 0

    dete_po_relaciji = True
    for table in tables:
        if table['naziv'] == child_table_name:
            if all(child_table_referencing_attribute in table['primarni_kljucevi'] for child_table_referencing_attribute in child_table_referencing_attributes):
                dete_po_relaciji = False

    for table in tables:
        if table['naziv'] == child_table_name:
            # table['roditelji'].append(parent_table_name)
            
            # table['roditelji'].append({'naziv': parent_table_name, 'referencirajuci_atributi': child_table_referencing_attributes, \
            #     'referencirani_atributi_roditelja': parent_table_referenced_attributes})

            reference = []
            for index, referencirajuci_atribut in enumerate(child_table_referencing_attributes):
                reference.append({"atribut": referencirajuci_atribut, 'roditeljski_atribut': parent_table_referenced_attributes[index]})

            if not dete_po_relaciji:
                table['roditelji']['po_hijerarhiji'].append({'naziv': parent_table_name, 'reference': reference})
            else:
                table['roditelji']['po_relaciji'].append({'naziv': parent_table_name, 'reference': reference})

            brojac += 1
    
        if table['naziv'] == parent_table_name:
            if not dete_po_relaciji:
                table['deca']['po_hijerarhiji'].append(child_table_name)
            else:
                table['deca']['po_relaciji'].append(child_table_name)
            brojac += 1

        if brojac == 2:
            break

def create_procedures(db_name, table):
    naziv_dobavi_procedure, args_dobavi_procedure = select_procedure(db_name, table)
    return {"dobavi": {'naziv': naziv_dobavi_procedure, 'args': args_dobavi_procedure}}

def select_procedure(db_name, table):
    # DROP PROCEDURE IF EXISTS `dobavi_drzava`; CREATE PROCEDURE `dobavi_drzava`(IN DR_IDENTIFIKATOR CHAR(3), IN DR_NAZIV VARCHAR(10), IN DR_GODINA_OSNIVANJA DATE) BEGIN END // 

    table_name = table.pop(0).split(' ')[-1].replace('`', '')
    table.pop(0)
    table.pop()
    pks = table.pop()

    procedure_name = 'dobavi_' + table_name.lower()

    params = ""
    conditions = ""
    params_value = []
    for index, line in enumerate(table):
        parts = line.split(' ')
        parts = list(filter(lambda x: x != '', parts))
        
        param_name = "_" + parts[0]
        param_type = parts[1].replace(',', '').upper()

        params += "IN " + param_name + " " + param_type + ", "
        # if index + 1 != len(table):
        #     params += ', '
        params_value.append(None)

        conditions += " IF " + param_name + " IS NOT NULL THEN " + \
                "IF @WHERE_CLAUSE = 'WHERE ' THEN " + \
                "SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, '" + parts[0] + " LIKE ', " + param_name + ", ' '); " + \
                "ELSE " + \
                "SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, 'AND " + parts[0] + " LIKE ', " + param_name + ", ' '); " + \
                "END IF; " + \
                "END IF;"
    params += "IN _LIMIT INT, IN _OFFSET INT"
    params_value.append(None)
    params_value.append(None)

    conditions += " IF (_LIMIT IS NULL) THEN SET _LIMIT := 5; END IF; IF (_OFFSET IS NULL) THEN SET _OFFSET := 0; END IF;"
    conditions += " IF @WHERE_CLAUSE != 'WHERE ' THEN SET @SQL_CLAUSE := CONCAT(@SQL_CLAUSE, @WHERE_CLAUSE, 'LIMIT ', _LIMIT, ' OFFSET ', _OFFSET); ELSE SET @SQL_CLAUSE := CONCAT(@SQL_CLAUSE, 'LIMIT ', _LIMIT, ' OFFSET ', _OFFSET); END IF;"

    sql_clause = "SET @SQL_CLAUSE := 'SELECT * FROM " + table_name + " ';"
    where_clause = "SET @WHERE_CLAUSE := 'WHERE ';"
    sql = """DROP PROCEDURE IF EXISTS `""" + procedure_name + """` // CREATE PROCEDURE `""" + procedure_name + """`(""" + params + """) BEGIN """ + \
    sql_clause + """ """ + where_clause + """ """ + conditions + """ """ + \
    "PREPARE sql_cl FROM @SQL_CLAUSE; " + "EXECUTE sql_cl; " + "DEALLOCATE PREPARE sql_cl;" + \
    """ END //"""

    procedure.append(sql)
    
    return procedure_name, params_value

# with open('./meta/MySQLSkript_2001_BazaCinjenica.sql', 'r') as fp:
with open('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.sql', 'r') as fp:
    lines = list(filter(lambda line: line != '\n', fp.readlines()))

    # db name
    for line in lines:
        if line.startswith('create schema'):
            db_name = line.split('`')[1]

    # konekcija
    konekcija = {'host': 'localhost', 'korisnicko_ime': 'root', 'lozinka': 'root'}

    # tabele
    tables = []
    table_lines = []
    add_to_table_lines = False
    for line in lines:
        if line.startswith('create table'):
            add_to_table_lines = True
        
        if add_to_table_lines:
            table_lines.append(line.strip())

        if add_to_table_lines is True and line.startswith(');\n'):
            tables.append(table_lines)
            table_lines = []
            add_to_table_lines = False
        
    popuni_lista_skracenica(copy.deepcopy(tables))

    _tables = []
    for table in tables:
        _table = copy.deepcopy(table)
        table = get_table(table)
        table['operacije'] = create_procedures(db_name, _table)
        _tables.append(table)
    tables = _tables

    # strani kljucevi
    foreign_key_lines = []
    foreign_key_line = ''
    for line in lines:
        line = line.strip()
        if 'alter table' in line and 'foreign key' in line or foreign_key_line != '':
            if ';' in line:
                foreign_key_line += line
                foreign_key_lines.append(foreign_key_line)
                foreign_key_line = ''
            else:
                foreign_key_line += line + ' '

    for foreign_key_line in foreign_key_lines:
        add_parent_child_relation(foreign_key_line, tables)

    # with open('./meta/informacioni_resurs_generisano.json', 'w') as outfile:
    with open('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.json', 'w') as outfile:
        json.dump({'db_naziv': db_name, 'konekcija': konekcija, 'tabele': tables}, outfile)

    # with open('./meta/informacioni_resurs_generisano_procedure.sql', 'w') as outfile:
    with open('./meta/razlika_izmedju_veza_hijerarhije_i_relacije_procedure.sql', 'w') as outfile:
        sql = """USE `""" + db_name + """`;
DELIMITER // 
"""

        for procedura in procedure:
            sql += procedura + """
"""

        sql += """DELIMITER ;"""

        outfile.write(sql)
