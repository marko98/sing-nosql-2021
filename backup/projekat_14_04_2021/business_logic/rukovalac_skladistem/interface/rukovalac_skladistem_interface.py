from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from business_logic.podatak.interface.kompozitni_podatak_interface import KompozitniPodatakInterface

class RukovalacSkladistemInterface:
    def dobavi(self, kompozitni_podatak):
        kompozitni_podatak : KompozitniPodatakInterface
        raise NotImplementedError('metoda dobavi nije implementirana')

    def kreiraj(self):
        raise NotImplementedError('metoda kreiraj nije implementirana')

    def izmeni(self):
        raise NotImplementedError('metoda izmeni nije implementirana')

    def obrisi(self):
        raise NotImplementedError('metoda obrisi nije implementirana')

    def dobavi_kompozitni_podatak_DATABASE(self):
        raise NotImplementedError('metoda dobavi_kompozitni_podatak_DATABASE nije implementirana')

    def dobavi_metapodatak_baze(self):
        raise NotImplementedError('metoda dobavi_metapodatak_baze nije implementirana')

    def dobavi_batch(self, batch_ime):
        raise NotImplementedError('metoda dobavi_batch nije implementirana')

    def dobavi_batch_po_reprezentacionom_atributu(self, reprezentacioni_atribut):
        raise NotImplementedError('metoda dobavi_batch_po_reprezentacionom_atributu nije implementirana')