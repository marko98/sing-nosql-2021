from .abstract_factory_interface import AbstractFactoryInterface
from ....model.podatak import Podatak
from ....model.podatak_tip import PodatakTip
from ....model.kompozitni_podatak import KompozitniPodatak
from ....model.kompozitni_podatak_tip import KompozitniPodatakTip

class AbstractFactory(AbstractFactoryInterface):
    instanca = None

    def __init__(self):
        super(AbstractFactory, self).__init__()

    @staticmethod
    def dobavi_instancu():
        if AbstractFactory.instanca is None:
            AbstractFactory.instanca = AbstractFactory()
        return AbstractFactory.instanca

    def kreiraj_kompozitni_podatak(self, tip : KompozitniPodatakTip = None):
        kompozitni_podatak = KompozitniPodatak()
        if isinstance(tip, KompozitniPodatakTip) and tip is not None:
            kompozitni_podatak.postavi_tip(tip)
        return kompozitni_podatak

    def kreiraj_podatak(self, tip : PodatakTip = None):
        podatak = Podatak()
        if isinstance(tip, PodatakTip) and tip is not None:
            podatak.postavi_tip(tip)
        return podatak