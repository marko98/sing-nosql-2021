class AbstractFactoryInterface:

    def kreiraj_kompozitni_podatak(self):
        raise NotImplementedError('metoda kreiraj_kompozitni_podatak nije implementirana')
        
    def kreiraj_podatak(self):
        raise NotImplementedError('metoda kreiraj_podatak nije implementirana')