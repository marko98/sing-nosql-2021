from PyQt5 import QtWidgets

from ..interface.podatak_interface import PodatakInterface
from .podatak_tip import PodatakTip

from typing import TYPE_CHECKING
if TYPE_CHECKING:    
    from .kompozitni_podatak import KompozitniPodatak

class Podatak(PodatakInterface):

    def __init__(self):
        super(Podatak, self).__init__()

        self.atribut = None
        self.reprezentacioni_atribut = None
        self.obavezan = False
        self.jedinstven = False
        self.vrednost = None
        self.tip = None
        self.roditelj = None
        self.roditelj : KompozitniPodatak
        self.referencira = None

        self.widgets = []

    def dobavi_obavezan(self):
        return self.obavezan

    def postavi_obavezan(self, obavezan):
        self.obavezan = obavezan

    def dobavi_jedinstven(self):
        return self.jedinstven

    def postavi_jedinstven(self, jedinstven):
        self.jedinstven = jedinstven

    def dobavi_referencira(self):
        return self.referencira

    def postavi_referencira(self, referencira):
        self.referencira = referencira

    def dobavi_vrednost(self):
        if self.referencira is not None:
            referenca = self.referencira['vrednost']

            if referenca is None:
                if self.vrednost is not None:
                    return self.vrednost

                # veza hijerarhije
                if self.obavezan and self.jedinstven:
                    raise ValueError('podatak koji referencira neki drugi podatak nema referencu na njega')
                # ako je preskocio if onda imamo vezu relacije
            else:
                koga = referenca.dobavi_roditelja().dobavi_roditelja().dobavi_atribut() + "." + referenca.dobavi_roditelja().dobavi_atribut() + "." + referenca.dobavi_atribut()
                # print(koga)
                if koga == self.referencira['atribut']:
                    self.vrednost = referenca.dobavi_vrednost()
                else:
                    raise ValueError('podatak referencira pogresan podatak')

        return self.vrednost

    def postavi_vrednost(self, vrednost):
        self.vrednost = vrednost

    def __str__(self):
        self.roditelj : PodatakInterface

        return 'tip: {}, atribut: {}, obavezan: {}, jedinstven: {}, vrednost: {}, roditelj_atribut: {}, referencira: {}, dubina: {}, broj_deteta: {}'.format(\
            self.tip, self.atribut, self.obavezan, self.jedinstven, self.vrednost, \
            self.roditelj.dobavi_atribut() if self.roditelj is not None else None, self.referencira, self.dobavi_dubinu(), self.dobavi_broj_deteta())

        # # potpun roditelj
        # return 'tip: {}, atribut: {}, obavezan: {}, jedinstven: {}, vrednost: {}, roditelj: {}, referencira: {}'.format(\
        #     self.tip, self.atribut, self.obavezan, self.jedinstven, self.vrednost, self.roditelj , self.referencira,)

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        self.atribut = atribut

    def dobavi_reprezentacioni_atribut(self):
        return self.reprezentacioni_atribut

    def postavi_reprezentacioni_atribut(self, reprezentacioni_atribut):
        self.reprezentacioni_atribut = reprezentacioni_atribut

    def dobavi_tip(self):
        return self.tip

    def postavi_tip(self, tip):
        if not isinstance(tip, PodatakTip):
            raise ValueError('tip podatka mora biti PodatakTip')
        self.tip = tip

    def dobavi_roditelja(self):
        return self.roditelj

    def postavi_roditelja(self, roditelj):
        # dodaj dete u roditelja i bice mu postavljen roditelj, a roditelju dete
        self.roditelj = roditelj

    def prikazi_podatke(self):
        print(self)

    def dobavi_dubinu(self):
        if self.roditelj is None:
            return 0
        else:
            return self.roditelj.dobavi_dubinu() + 1

    def dobavi_broj_deteta(self, tip=None):
        if self.roditelj is None:
            # # stavljeno zbog PodatakWidgetInterface -> odkaci_se_od_stabla
            # return 0
            return None
        else:
            if tip is None:
                for index, dete in enumerate(self.roditelj.dobavi_decu()):
                    if dete == self:
                        return index
            else:
                if isinstance(tip, PodatakTip):
                    filtrirana_deca = list(filter(lambda dete: dete.dobavi_tip() == tip, self.roditelj.dobavi_decu()))
                    for index, dete in enumerate(filtrirana_deca):
                        if dete == self:
                            return index
                else:
                    raise ValueError('vrsi se dobavi_broj_deteta po tipu(KompozitniPodatakTip) koji ne odgovara klasi Podatak')
            raise IndexError('Dete \'ima\' roditelja, ali roditelj ne zna za njega')

    # PodatakWidgetInterface
    def zakaci_se_za_stablo(self, parent_widget, tipovi=None):
        # vec postoji
        for w_data in self.widgets:
            widget = w_data['widget']
            widget : QtWidgets.QWidget
            if widget.parent == parent_widget:
                return

        if tipovi is None or (tipovi is not None and self.dobavi_tip() in tipovi):
            parent_widget : QtWidgets.QWidget
            tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [self.reprezentacioni_atribut])
            tree_widget_item.parent = parent_widget

            tree_widget_item.podatak = self
            # self.widgets.append(tree_widget_item)
            self.widgets.append({'widget': tree_widget_item, 'tipovi': tipovi})

    def odkaci_se_od_stabla(self, parent_widget, tipovi=None):
        if tipovi is None or (tipovi is not None and self.dobavi_tip() in tipovi):
            parent_widget : QtWidgets.QTreeWidgetItem         
            for index, w_data in enumerate(self.widgets):
                widget = w_data['widget']
                widget : QtWidgets.QWidget
                if widget.parent == parent_widget:
                    parent_widget.removeChild(widget)
                    self.widgets.pop(index)
                    return
