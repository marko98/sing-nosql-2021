class PodatakWidgetInterface:

    def zakaci_se_za_stablo(self, widget, tipovi=None):
        raise NotImplementedError('metoda zakaci_se_za_stablo nije implementirana')

    def odkaci_se_od_stabla(self, widget, tipovi=None):
        raise NotImplementedError('metoda odkaci_se_od_stabla nije implementirana')