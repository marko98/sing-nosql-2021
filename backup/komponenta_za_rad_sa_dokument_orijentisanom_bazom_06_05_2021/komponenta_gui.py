from PyQt5 import QtWidgets, uic, QtCore
import os
import json
from threading import Timer
import decimal
from datetime import datetime, date
import math

from core.business_logic.model.komponenta import Komponenta
from .business_logic.model.komponenta_kontroler import KomponentaKontroler
from .business_logic.model.podatak_tip import PodatakTip

# from typing import TYPE_CHECKING
# if TYPE_CHECKING:

class KomponentaGUI(Komponenta):

    def __init__(self, metapodaci):
        super(KomponentaGUI, self).__init__(metapodaci)

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_dokument_orijentisanom_bazom', 'ui', 'komponenta_dark.ui')
        print('ucitava se komponenta:', self, ', ui loading...')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QWidget
        # with open(os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_dokument_orijentisanom_bazom', 'css', 'komponenta.css'), 'r') as fp:
        #     self.ui.setStyleSheet(fp.read())
        print('ucitana')

        # polja
        self.kontroler = KomponentaKontroler.dobavi_instancu(self)
        self.kontroler : KomponentaKontroler

        self.meta_opis_dokumeta = None
        self.filteri = {}

        self.strana = 0
        self.selektovani_red = None
        self.dokumenti = []

        # ui elements
        self.btn_prikazi_filtere = self.ui.btn_prikazi_filtere
        self.btn_prikazi_filtere : QtWidgets.QPushButton

        self.btn_primeni_filtere = self.ui.btn_primeni_filtere
        self.btn_primeni_filtere : QtWidgets.QPushButton
        self.btn_primeni_filtere.setVisible(False)

        self.btn_ponisti_filtere = self.ui.btn_ponisti_filtere
        self.btn_ponisti_filtere : QtWidgets.QPushButton
        self.btn_ponisti_filtere.setVisible(False)

        self.scroll_area = self.ui.scroll_area
        self.scroll_area : QtWidgets.QScrollArea
        self.scroll_area.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scroll_area.setVisible(False)

        self.grid_layout = QtWidgets.QGridLayout(self.scroll_area.widget())
        self.grid_layout : QtWidgets.QGridLayout

        # self.table = self.ui.table
        # self.table : QtWidgets.QTableWidget
        # self.table.setSortingEnabled(True)
        # self.table.itemSelectionChanged.connect(self._na_promenu_selekcije_u_tabeli)
        # self.table.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.tree = self.ui.tree
        self.tree : QtWidgets.QTreeWidget()
        self.tree.setSortingEnabled(True)
        self.tree.itemSelectionChanged.connect(self._na_promenu_selekcije_u_stablu)
        self.tree.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.tree.setSortingEnabled(False)
        self.tree.setHeaderLabels(['atribut', 'vrednost'])

        self.sp_box_broj_strane = self.ui.sp_box_broj_strane
        self.sp_box_broj_strane : QtWidgets.QSpinBox
        self.sp_box_broj_strane.setMinimum(1)

        self.sp_box_limit = self.ui.sp_box_limit
        self.sp_box_limit : QtWidgets.QSpinBox
        self.sp_box_limit.setValue(self.kontroler.limit)
        self.sp_box_limit.setMinimum(1)
        self.sp_box_limit.setMaximum(20)

        self.btn_prva_strana = self.ui.btn_prva_strana
        self.btn_prva_strana : QtWidgets.QPushButton

        self.btn_leva_strana = self.ui.btn_leva_strana
        self.btn_leva_strana : QtWidgets.QPushButton

        self.btn_desna_strana = self.ui.btn_desna_strana
        self.btn_desna_strana : QtWidgets.QPushButton

        self.btn_poslednja_strana = self.ui.btn_poslednja_strana
        self.btn_poslednja_strana : QtWidgets.QPushButton

        self.btn_refresh = self.ui.btn_refresh
        self.btn_refresh : QtWidgets.QPushButton

        self.btn_reconnect = self.ui.btn_reconnect
        self.btn_reconnect : QtWidgets.QPushButton

        self.btn_kreiraj_dokument = self.ui.btn_kreiraj_dokument
        self.btn_kreiraj_dokument : QtWidgets.QPushButton
        self.btn_kreiraj_dokument.hide()

        self.btn_unesi_meta_opis_dokumenta = self.ui.btn_unesi_meta_opis_dokumenta
        self.btn_unesi_meta_opis_dokumenta : QtWidgets.QPushButton

        self.l_msg = self.ui.l_msg
        self.l_msg : QtWidgets.QLabel
        self.l_msg.setVisible(False)

        # connections
        self.btn_unesi_meta_opis_dokumenta.clicked.connect(self._dobavi_meta_opis_dokumenta)
        self.btn_prikazi_filtere.clicked.connect(self.prikazi_filtere)
        self.btn_primeni_filtere.clicked.connect(self.primeni_filtere)
        self.btn_ponisti_filtere.clicked.connect(self.ponisti_filtere)

        self.btn_prva_strana.clicked.connect(lambda _: self.idi_na_stranu(0))
        self.btn_leva_strana.clicked.connect(lambda _: self.idi_na_stranu(0 if self.strana - 1 < 0 else self.strana - 1))
        self.btn_desna_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1 if self.strana + 1 > self.sp_box_broj_strane.maximum()-1 else self.strana + 1))
        self.btn_poslednja_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1))

        self.sp_box_broj_strane.valueChanged.connect(lambda x: self.idi_na_stranu(self.sp_box_broj_strane.value()-1))
        self.sp_box_limit.valueChanged.connect(lambda x: self.promeni_limit(self.sp_box_limit.value()))

        self.btn_refresh.clicked.connect(self._refresh)
        self.btn_kreiraj_dokument.clicked.connect(self.kreiraj_dokument)

        self.btn_reconnect.clicked.connect(self.ponovi_konektovanje_na_bazu)

    def promeni_limit(self, limit):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.kontroler.postavi_limit(limit)
            
            br_strana = self.kontroler.dobavi_broj_mogucih_strana()
            # print('br_strana', br_strana)
            self.sp_box_broj_strane.setMaximum(br_strana)

            self.dobavi_dokumente()
            self.popuni_stablo()

    def _proveri_broj_mogucih_strana(self):
        broj_mogucih_strana = self.kontroler.dobavi_broj_mogucih_strana()
        self.sp_box_broj_strane.setMaximum(1 if broj_mogucih_strana == 0 else broj_mogucih_strana)

    def _na_promenu_selekcije_u_stablu(self):        
        if len(self.tree.selectedItems()) > 0:
            selected_item = self.tree.selectedItems()[0]

            for i in range(self.tree.topLevelItemCount()):
                if selected_item == self.tree.topLevelItem(i):
                    self.selektovani_red = i
                    self.btn_kreiraj_dokument.show()
                    return

        self.btn_kreiraj_dokument.hide()
        self.selektovani_red = None

    def _izgradi_polja_za_filtere(self):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            height = 50
            for index, atribut in enumerate(self.meta_opis_dokumeta['parametri']):
                horizontal_layout_widget = QtWidgets.QWidget()
                horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

                horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
                horizontal_layout.setContentsMargins(0, 0, 0, 0)

                self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

                self.grid_layout.addWidget(horizontal_layout_widget, index//3, index - index//3*3)

    def _dobavi_polja_spram_tipa(self, atribut, horizontal_layout_widget, horizontal_layout):
        label = QtWidgets.QLabel(horizontal_layout_widget)
        label.setStyleSheet("color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
        label_text = atribut['reprezentacioni_atribut'] + ":"
        label.setText(label_text)
        horizontal_layout.addWidget(label)

        if atribut['tip'] == PodatakTip.STRING.value:
            lineEdit = QtWidgets.QLineEdit(horizontal_layout_widget)
            lineEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")

            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                lineEdit.setText(self.filteri[atribut['atribut']])

            lineEdit.textChanged.connect(lambda _: self.dodeli_filter(atribut, lineEdit.text()))
            horizontal_layout.addWidget(lineEdit)
        elif atribut['tip'] == PodatakTip.TEXT.value:
            textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
            textEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            textEdit.setEnabled(True)
            
            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                textEdit.setText(self.filteri[atribut['atribut']])

            textEdit.textChanged.connect(lambda _: self.dodeli_filter(atribut, textEdit.toPlainText()))             
            horizontal_layout.addWidget(textEdit)
        elif atribut['tip'] == PodatakTip.NUMBER.value:
            spinBox = QtWidgets.QSpinBox(horizontal_layout_widget)
            spinBox.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            
            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                spinBox.setValue(self.filteri[atribut['atribut']])

            spinBox.valueChanged.connect(lambda _: self.dodeli_filter(atribut, spinBox.value()))
            horizontal_layout.addWidget(spinBox)
        elif atribut['tip'] == PodatakTip.DATE.value:
            dateTimeEdit = QtWidgets.QDateTimeEdit(horizontal_layout_widget)
            dateTimeEdit.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
            # datetime.date
            # dateTimeEdit.dateChanged.connect(lambda _: print(dateTimeEdit.date().toPyDate()))
            # self.dodeli_filter(atribut, dateTimeEdit.date().toPyDate())

            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                dateTimeEdit.setDate(self.filteri[atribut['atribut']])

            dateTimeEdit.dateChanged.connect(lambda _: self.dodeli_filter(atribut, dateTimeEdit.date().toPyDate())) 
            horizontal_layout.addWidget(dateTimeEdit)
        elif atribut['tip'] == PodatakTip.BOOLEAN.value:
            checkBox = QtWidgets.QCheckBox(horizontal_layout_widget)
            checkBox.setStyleSheet("font: 8pt \"Segoe UI\";")
            
            if atribut['atribut'] in self.filteri and self.filteri[atribut['atribut']] is not None:
                checkBox.setChecked(self.filteri[atribut['atribut']])

            checkBox.stateChanged.connect(lambda _: self.dodeli_filter(atribut, checkBox.isChecked()))
            horizontal_layout.addWidget(checkBox)
       
    def dobavi_qwidget(self, parent = None):
        if parent is not None:
            self.ui.setParent(parent)
        return self.ui

    def _dobavi_meta_opis_dokumenta(self):
        atribut : ListInterface

        dlg = QtWidgets.QFileDialog()
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        mimeTypeFilters = ["application/json"]
        dlg.setMimeTypeFilters(mimeTypeFilters)

        if dlg.exec_():
            file_path = dlg.selectedFiles()[0]
            with open(file_path, 'r') as fp:
                self.meta_opis_dokumeta = json.load(fp)
                self.kontroler.postavi_meta_opis_dokumenta(self.meta_opis_dokumeta)
                self._proveri_broj_mogucih_strana()
                self._izgradi_polja_za_filtere()

                self.dobavi_dokumente()
                # self.popuni_tabelu()
                self.popuni_stablo()

    def ponovi_konektovanje_na_bazu(self):
        self.kontroler.postavi_meta_opis_dokumenta(self.meta_opis_dokumeta)
        self._proveri_broj_mogucih_strana()
        self._izgradi_polja_za_filtere()

        self.dobavi_dokumente()
        self.popuni_stablo()

    def _dobavi_start_end_index(self):
        start_index = self.strana * self.kontroler.limit
        self.kontroler.dobavi_ukupan_broj_zapisa()
        end_index = self.kontroler.ukupan_broj_zapisa - 1 if start_index + self.kontroler.limit - 1 > self.kontroler.ukupan_broj_zapisa - 1 else start_index + self.kontroler.limit - 1
        return start_index, end_index

    def dobavi_dokumente(self):
        # print('idi_na_stranu', self.strana)
        start_index, end_index = self._dobavi_start_end_index()
        # print('poc. index,', start_index, ', krajnji index,', end_index)

        self.dokumenti = self.kontroler.dobavi_dokumente(start_index, end_index)

    def prikazi_filtere(self):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.scroll_area.setVisible(not self.scroll_area.isVisible())
            self.btn_primeni_filtere.setVisible(not self.btn_primeni_filtere.isVisible())
            self.btn_ponisti_filtere.setVisible(not self.btn_ponisti_filtere.isVisible())

            if self.btn_prikazi_filtere.text() == 'prikazi filtere':
                self.btn_prikazi_filtere.setText('sakrij filtere')
            else:
                self.btn_prikazi_filtere.setText('prikazi filtere')

    def dodeli_filter(self, atribut, vrednost):
        if isinstance(vrednost, str):
            if vrednost == '':
                vrednost = None
            # else:
            #     vrednost = '%'+vrednost+'%'
                
        self.filteri[atribut['atribut']] = vrednost

    def primeni_filtere(self):
        self.kontroler.postavi_filtere(self.filteri)
        self.dobavi_dokumente()
        # self.popuni_tabelu()
        self.popuni_stablo()

    def ponisti_filtere(self):
        for kljuc in self.filteri:
            self.filteri[kljuc] = None

        self.primeni_filtere()
        self._izgradi_polja_za_filtere()

    # ------------------------
    def popuni_stablo(self):
        self.tree.clear()

        for index, dokument in enumerate(self.dokumenti):
            # for kljuc in dokument:
            #     _dokument = dokument[kljuc]
            self.tree : QtWidgets.QWidget
            tree_widget_item = QtWidgets.QTreeWidgetItem(self.tree, [str(index), None])
            tree_widget_item.parent = self.tree
            self.zakaci_se_za_stablo(tree_widget_item, dokument)

        if self.selektovani_red is not None:
            strana_za_selektovani_red = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
            if self.strana != strana_za_selektovani_red:
                self.selektovani_red = self.strana * self.kontroler.limit

            # print(self.selektovani_red - self.kontroler.limit * self.strana)
            self.tree.setCurrentItem(self.tree.topLevelItem(self.selektovani_red - self.kontroler.limit * self.strana))

    def zakaci_se_za_stablo(self, parent_widget, dokument):
        for kljuc in dokument:
            parent_widget : QtWidgets.QWidget
            if isinstance(dokument[kljuc], dict):
                # recnik
                tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [kljuc, None])
                tree_widget_item.parent = parent_widget
                self.zakaci_se_za_stablo(tree_widget_item, dokument[kljuc])
            elif isinstance(dokument[kljuc], list):
                # lista
                tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [kljuc, None])
                tree_widget_item.parent = parent_widget

                for index, _dokument in enumerate(dokument[kljuc]):
                    _tree_widget_item = QtWidgets.QTreeWidgetItem(tree_widget_item, [str(index), None])
                    _tree_widget_item.parent = tree_widget_item
                    self.zakaci_se_za_stablo(_tree_widget_item, _dokument)
            else:
                # jednostavan dokument
                tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [kljuc, str(dokument[kljuc])])
                tree_widget_item.parent = parent_widget

    def idi_na_stranu(self, strana):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            # print(self.strana)
            
            if strana < 0:
                strana = 0

            if self.strana != strana:
                self.strana = strana

                self.sp_box_broj_strane.setValue(self.strana+1)
                self.dobavi_dokumente()
                self.popuni_stablo()

    def _refresh(self):
        if self.meta_opis_dokumeta is None:
            self.l_msg.setText('molimo unesite meta opis dokumenta')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.kontroler.isprazni_dokumente()
            self.dobavi_dokumente()
            self.popuni_stablo()

    def kreiraj_dokument(self):
        if self.selektovani_red is None:
            self.l_msg.setText('molimo odaberite dokument koji zelite da unesete u bazu')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()
        else:
            self.kontroler.kreiraj_dokument(self.dokumenti[self.selektovani_red])
            
            self.l_msg.setText('odabrani dokument je uspesno unet u bazu dokumenata')
            self.l_msg.setVisible(True)

            timer = Timer(2.0, lambda : self.l_msg.setVisible(False))
            timer.start()

            self.tree.clearSelection()
