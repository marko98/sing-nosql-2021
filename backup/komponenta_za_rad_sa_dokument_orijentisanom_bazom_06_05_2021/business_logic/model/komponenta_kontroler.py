import json
import math
import copy

from core.business_logic.model.jezgro import Jezgro
from ..pattern.behavioural.observer.model.observable import Observable
from ..rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface
from ..rukovalac_skladistem.model.rukovalac_dokumentalistickim_skladistem import RukovalacDokumentalistickimSkladistem

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...komponenta_gui import KomponentaGUI

class KomponentaKontroler(Observable):
    instanca = None

    def __init__(self, gui_komponenta):
        super(KomponentaKontroler, self).__init__()
        # print(__name__)
        self.meta_opis_dokumeta = None

        self.gui_komponenta = gui_komponenta
        self.gui_komponenta : KomponentaGUI

        self.jezgro = Jezgro.dobavi_instancu()
        self.jezgro : Observable

        self.rukovalac_skladistem = None
        self.rukovalac_skladistem : RukovalacSkladistemInterface

        self.limit = 4
        self.ukupan_broj_zapisa = 0
    
    @staticmethod
    def dobavi_instancu(gui_komponenta=None):
        if KomponentaKontroler.instanca is not None:
            return KomponentaKontroler.instanca
        else:
            if gui_komponenta is not None:
                KomponentaKontroler.instanca = KomponentaKontroler(gui_komponenta)
                return KomponentaKontroler.instanca
        return None

    def postavi_meta_opis_dokumenta(self, meta_opis_dokumeta):
        self.meta_opis_dokumeta = meta_opis_dokumeta
        self.rukovalac_skladistem = RukovalacDokumentalistickimSkladistem(self.meta_opis_dokumeta)

    def postavi_filtere(self, filteri=None):
        self.rukovalac_skladistem.postavi_filtere(filteri)

    def dobavi_dokumente(self, start_index, end_index):
        return self.rukovalac_skladistem.dobavi(start_index, end_index)

    def dobavi_ukupan_broj_zapisa(self):
        self.ukupan_broj_zapisa = self.rukovalac_skladistem.dobavi_ukupan_broj_zapisa()

    def dobavi_broj_mogucih_strana(self):
        self.dobavi_ukupan_broj_zapisa()

        br_strana = math.ceil(self.ukupan_broj_zapisa / self.limit)
        return br_strana

    def postavi_limit(self, limit):
        self.limit = limit

    def isprazni_dokumente(self):
        self.rukovalac_skladistem.dokumenti = {}

    def kreiraj_dokument(self, dokument):
        self.rukovalac_skladistem.kreiraj(copy.deepcopy(dokument))
