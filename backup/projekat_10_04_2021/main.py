# from PyQt5 import QtWidgets
# import sys

# from core.model.registar_komponenti import RegistarKomponenti
# from core.model.jezgro import Jezgro
# from gui.jezgro import JezgroGUI

# app = QtWidgets.QApplication(sys.argv)

# # business logic
# jezgro = Jezgro.dobavi_instancu()
# # jezgro.obavesti('projekat.komponenta_1')

# # gui logic
# jezgro_gui = JezgroGUI()
# jezgro_gui.load_ui()
# jezgro_gui.ui.show()

# # komp_1_ui = jezgro.obavesti('projekat.komponenta_1')

# # komp_1_ui.setParent(jezgro_gui.ui)
# # jezgro_gui.ui.setCentralWidget(komp_1_ui)
# # komp_1_ui.show()

# sys.exit(app.exec_())

# # proba
# from proba.model.kompozitni_podatak_recnik import KompozitniPodatakRecnik
# from proba.model.kompozitni_podatak_lista import KompozitniPodatakLista
# from proba.model.podatak_tip_enum import PodatakTip
# from proba.model.podatak import Podatak

# print('Primeri za Podatak:')
# print(Podatak('Marko', 'string_atr').dobavi_atribut_vrednost())
# print(Podatak(10, 'int_atr').dobavi_atribut_vrednost())
# print(Podatak(False, 'bool_atr').dobavi_atribut_vrednost())
# print(Podatak(["A", "b", 5], 'list_atr').dobavi_atribut_vrednost())
# print(Podatak({"ime": "David", "godine": 22}, 'dict_atr').dobavi_atribut_vrednost())
# print('')
# print('Primeri za KompozitniPodatakRecnik:')
# print(KompozitniPodatakRecnik([Podatak('ime', 'Marko'), Podatak('prezime', 'Zahorodni'), Podatak('godine', 22)], 'osoba').dobavi_atribut_vrednost())
# print('')
# print('Primeri za KompozitniPodatakLista:')
# studenti = KompozitniPodatakLista([
#     KompozitniPodatakRecnik([Podatak('ime', 'Marko'), Podatak('prezime', 'Zahorodni'), Podatak('godine', 22)], 'osoba'), \
#     KompozitniPodatakRecnik([Podatak('ime', 'David'), Podatak('prezime', 'Zahorodni'), Podatak('godine', 22)], 'osoba'), \
#     Podatak(1, 'OSOBA_ID', PodatakTip.NUMBER, True), Podatak('Marko', 'ime'), Podatak('Zahorodni', 'prezime'), Podatak(22, 'godine', PodatakTip.NUMBER, False)], 'studenti')
# print(studenti.dobavi_atribut_vrednost())
# print('')

# print(studenti.dobavi_decu())
# print([dete for dete in studenti.dobavi_decu()])
# for dete in studenti.dobavi_decu():
#     if isinstance(dete, Podatak):
#         print('atribut:', dete.atribut, ', vrednost:', dete.vrednost, \
#             ', tip:', dete.tip, ', obavezan:', dete.obavezan, \
#             ', da li je deo primarnog kljuca:', dete.deo_primarnog_kljuca)
# print('')

# print(Podatak("Dada").dobavi_atribut_vrednost())
# print(KompozitniPodatakRecnik([Podatak('ime', 'Dada')]).dobavi_atribut_vrednost())

# studenti = KompozitniPodatakLista([KompozitniPodatakRecnik([Podatak('Marko', 'ime'), Podatak('Zahorodni', 'prezime')]), KompozitniPodatakRecnik([Podatak('David', 'ime'), Podatak('Zahorodni', 'prezime')])])
# print(studenti.dobavi_atribut_vrednost())

# studenti.postavi_atribut('studenti')
# univerzitet = KompozitniPodatakRecnik([Podatak('Singidunum', 'naziv'), studenti])
# print(univerzitet.dobavi_atribut_vrednost())

# pokusaj
from pokusaj.model.podatak import Podatak
from pokusaj.model.podatak_tip import PodatakTip
from pokusaj.model.kompozitni_podatak import KompozitniPodatak
from pokusaj.model.kompozitni_podatak_tip import KompozitniPodatakTip
from pokusaj.pattern.creational.abstract_factory.abstract_factory import AbstractFactory

ab_factory = AbstractFactory.dobavi_instancu()

# atribut
srbija_dr_id = Podatak()
srbija_dr_id.postavi_atribut('DR_ID')
srbija_dr_id.postavi_obavezan(True)
srbija_dr_id.postavi_jedinstven(True)
srbija_dr_id.postavi_vrednost(0)
srbija_dr_id.postavi_tip(PodatakTip.NUMBER)

usa_dr_id = Podatak()
usa_dr_id.postavi_atribut('DR_ID')
usa_dr_id.postavi_obavezan(True)
usa_dr_id.postavi_jedinstven(True)
usa_dr_id.postavi_vrednost(1)
usa_dr_id.postavi_tip(PodatakTip.NUMBER)

# entiteti
srbija = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.ENTITET)
srbija.postavi_atribut('DRZAVA_ENTITET')
srbija.dodaj_dete(srbija_dr_id)

usa = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.ENTITET)
usa.postavi_atribut('DRZAVA_ENTITET')
usa.dodaj_dete(usa_dr_id)

# tabele
drzave = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.BATCH)
drzave.postavi_atribut('DRZAVA_BATCH')
drzave.dodaj_dete(srbija)
drzave.dodaj_dete(usa)

# baze
host = Podatak()
host.postavi_atribut('HOST')
host.postavi_obavezan(True)
host.postavi_jedinstven(True)
host.postavi_vrednost('localhost')
host.postavi_tip(PodatakTip.STRING)
korisnicko_ime = Podatak()
korisnicko_ime.postavi_atribut('KORISNICKO_IME')
korisnicko_ime.postavi_obavezan(True)
korisnicko_ime.postavi_jedinstven(True)
korisnicko_ime.postavi_vrednost('root')
korisnicko_ime.postavi_tip(PodatakTip.STRING)
lozinka = Podatak()
lozinka.postavi_atribut('LOZINKA')
lozinka.postavi_obavezan(True)
lozinka.postavi_jedinstven(True)
lozinka.postavi_vrednost('root')
lozinka.postavi_tip(PodatakTip.STRING)

mysql_db = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.DATABASE)
mysql_db.postavi_atribut('MYSQL_DATABASE')
mysql_db.postavi_decu([host, korisnicko_ime, lozinka])
mysql_db.dodaj_dete(drzave)

# print(drzave)
# print(srbija)
# print(usa)
# print(srbija_dr_id)
# print(usa_dr_id)

mysql_db.prikazi_podatke()

# 

# atribut
reg_id = Podatak()
reg_id.postavi_atribut('REG_ID')
reg_id.postavi_obavezan(True)
reg_id.postavi_jedinstven(True)
reg_id.postavi_vrednost(0)
reg_id.postavi_tip(PodatakTip.NUMBER)

reg_dr_id = Podatak()
reg_dr_id.postavi_atribut('DR_ID')
reg_dr_id.postavi_obavezan(True)
reg_dr_id.postavi_jedinstven(True)
reg_dr_id.postavi_tip(PodatakTip.NUMBER)
# reg_dr_id.postavi_referencira({'atribut': 'DRZAVA_ENTITET.DR_ID', 'vrednost': None})
reg_dr_id.postavi_referencira({'atribut': 'DRZAVA_ENTITET.DR_ID', 'vrednost': srbija_dr_id})

# entiteti
vojvodina = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.ENTITET)
vojvodina.postavi_atribut('VOJVODINA_REGION_ENTITET')
vojvodina.dodaj_dete(reg_id)
vojvodina.dodaj_dete(reg_dr_id)

# tabele
regioni = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.BATCH)
regioni.postavi_atribut('REGION_BATCH')
regioni.dodaj_dete(vojvodina)

regioni.prikazi_podatke()
print(reg_dr_id.dobavi_vrednost())

from pokusaj.model.database_parser import kreiraj_bazu
import json
# with open ('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.json', 'r') as fp:
with open ('./meta/informacioni_resurs_generisano.json', 'r') as fp:    
    metapodatak = json.load(fp)
    baza = kreiraj_bazu(metapodatak)
    print('hi')