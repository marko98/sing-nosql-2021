from PyQt5 import QtWidgets, uic
import os

from core.model.komponenta import Komponenta

class PrvaKomponenta(Komponenta):

    def __init__(self, metapodaci):
        super(PrvaKomponenta, self).__init__(metapodaci)

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_1', 'widget.ui')
        print('ucitava se komponenta:', self, ', ui loading...')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QWidget
        print('ucitana')

    def dobavi_qwidget(self, parent = None):
        if parent is not None:
            self.ui.setParent(parent)
        return self.ui