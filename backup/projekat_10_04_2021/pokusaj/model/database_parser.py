from ..pattern.creational.abstract_factory.abstract_factory import AbstractFactory
from .podatak_tip import PodatakTip
from .kompozitni_podatak_tip import KompozitniPodatakTip

def kreiraj_bazu(metapodatak):
    ab_factory = AbstractFactory.dobavi_instancu()
    ab_factory : AbstractFactory

    # baza
    konekcija = metapodatak['konekcija']
    host = ab_factory.kreiraj_podatak(PodatakTip.STRING)
    host.postavi_atribut('HOST')
    host.postavi_obavezan(True)
    host.postavi_jedinstven(True)
    host.postavi_vrednost(konekcija['host'])

    korisnicko_ime = ab_factory.kreiraj_podatak(PodatakTip.STRING)
    korisnicko_ime.postavi_atribut('KORISNICKO_IME')
    korisnicko_ime.postavi_obavezan(True)
    korisnicko_ime.postavi_jedinstven(True)
    korisnicko_ime.postavi_vrednost(konekcija['korisnicko_ime'])

    lozinka = ab_factory.kreiraj_podatak(PodatakTip.STRING)
    lozinka.postavi_atribut('LOZINKA')
    lozinka.postavi_obavezan(True)
    lozinka.postavi_jedinstven(True)
    lozinka.postavi_vrednost(konekcija['lozinka'])

    db = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.DATABASE)
    db.postavi_atribut(metapodatak['db_naziv'])
    db.postavi_decu([host, korisnicko_ime, lozinka])

    # tabela - batch
    kreirani_batchovi = []
    for tabela in metapodatak['tabele']:
        batch = ab_factory.kreiraj_kompozitni_podatak(KompozitniPodatakTip.BATCH)
        batch.postavi_atribut(tabela['naziv'])

        # za citanje atributa specificnih za tabelu imacemo ucitan json metapodatak u coru i 
        # potom cemo pristupati njemu za generisanje entiteta, tj atributa unutar njega

        kreirani_batchovi.append({'naziv': tabela['naziv'], \
            'deca': tabela['deca'], 'roditelji': tabela['roditelji'], 'batch': batch})
    
    for batch_data in kreirani_batchovi:
        batch = batch_data['batch']

        for dete_naziv in batch_data['deca']['po_hijerarhiji']:
            dete = None
            for kreirani_batch in kreirani_batchovi:
                if kreirani_batch['naziv'] == dete_naziv:
                    dete = kreirani_batch['batch']
                    break
            if dete is None:
                raise ValueError('database_parser, dete po hijerarhiji je izgubljeno')
            batch.dodaj_dete_po_hijerarhiji(dete)

        for dete_naziv in batch_data['deca']['po_relaciji']:
            dete = None
            for kreirani_batch in kreirani_batchovi:
                if kreirani_batch['naziv'] == dete_naziv:
                    dete = kreirani_batch['batch']
                    break
            if dete is None:
                raise ValueError('database_parser, dete po relaciji je izgubljeno')
            batch.dodaj_dete_po_relaciji(dete)
            
    batchovi = [batch_data['batch'] for batch_data in kreirani_batchovi]

    for batch in batchovi:
        db.dodaj_dete(batch)

    # print(metapodatak)
    return db