# https://docs.python.org/3/library/enum.html
from enum import Enum

class KompozitniPodatakTip(Enum):
    DATABASE = 'DATABASE'
    BATCH = 'BATCH'
    ENTITET = 'ENTITET'