from ..interface.podatak_interface import PodatakInterface
from .podatak_tip import PodatakTip

class Podatak(PodatakInterface):

    def __init__(self):
        super(Podatak, self).__init__()

        self.atribut = None
        self.obavezan = False
        self.jedinstven = False
        self.vrednost = None
        self.tip = None
        self.roditelj = None
        self.referencira = None

    def dobavi_obavezan(self):
        return self.obavezan

    def postavi_obavezan(self, obavezan):
        self.obavezan = obavezan

    def dobavi_jedinstven(self):
        return self.jedinstven

    def postavi_jedinstven(self, jedinstven):
        self.jedinstven = jedinstven

    def dobavi_referencira(self):
        return self.referencira

    def postavi_referencira(self, referencira):
        self.referencira = referencira

    def dobavi_vrednost(self):
        if self.referencira is not None:
            referenca = self.referencira['vrednost']

            if referenca is None:
                # veza hijerarhije
                if self.obavezan and self.jedinstven:
                    raise ValueError('podatak koji referencira neki drugi podatak nema referencu na njega')
                # ako je preskocio if onda imamo vezu relacije
            else:
                koga = referenca.dobavi_roditelja().dobavi_atribut() + "." + referenca.dobavi_atribut()
                # print(koga)
                if koga == self.referencira['atribut']:
                    self.vrednost = referenca.dobavi_vrednost()
                else:
                    raise ValueError('podatak referencira pogresan podatak')

        return self.vrednost

    def postavi_vrednost(self, vrednost):
        self.vrednost = vrednost

    def __str__(self):
        self.roditelj : PodatakInterface

        return 'tip: {}, atribut: {}, obavezan: {}, jedinstven: {}, vrednost: {}, roditelj_atribut: {}, referencira: {}'.format(\
            self.tip, self.atribut, self.obavezan, self.jedinstven, self.vrednost, self.roditelj.dobavi_atribut() if self.roditelj is not None else None, self.referencira,)

        # # potpun roditelj
        # return 'tip: {}, atribut: {}, obavezan: {}, jedinstven: {}, vrednost: {}, roditelj: {}, referencira: {}'.format(\
        #     self.tip, self.atribut, self.obavezan, self.jedinstven, self.vrednost, self.roditelj , self.referencira,)

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        self.atribut = atribut

    def dobavi_tip(self):
        return self.tip

    def postavi_tip(self, tip):
        if not isinstance(tip, PodatakTip):
            raise ValueError('tip podatka mora biti PodatakTip')
        self.tip = tip

    def dobavi_roditelja(self):
        return self.roditelj

    def postavi_roditelja(self, roditelj):
        # dodaj dete u roditelja i bice mu postavljen roditelj, a roditelju dete
        self.roditelj = roditelj

    def prikazi_podatke(self):
        print(self)