class PodatakInterface:
    def dobavi_atribut(self):
        raise NotImplementedError('metoda dobavi_atribut nije implementirana')

    def postavi_atribut(self, atribut):
        raise NotImplementedError('metoda postavi_atribut nije implementirana')

    # def dobavi_vrednost(self):
    #     raise NotImplementedError('metoda dobavi_vrednost nije implementirana')

    # def postavi_vrednost(self, vrednost):
    #     raise NotImplementedError('metoda postavi_vrednost nije implementirana')

    def dobavi_tip(self):
        raise NotImplementedError('metoda dobavi_tip nije implementirana')

    def postavi_tip(self, tip):
        raise NotImplementedError('metoda postavi_tip nije implementirana')

    def dobavi_roditelja(self):
        raise NotImplementedError('metoda dobavi_roditelja nije implementirana')

    def postavi_roditelja(self, roditelja):
        raise NotImplementedError('metoda postavi_roditelja nije implementirana')

    def prikazi_podatke(self):
        raise NotImplementedError('metoda prikazi_podatke nije implementirana')