from PyQt5 import QtWidgets

from ..interface.mediator_interface import MediatorInterface
from .registar_komponenti import RegistarKomponenti

class Jezgro(MediatorInterface):
    instanca = None

    def __init__(self):
        super(Jezgro, self).__init__()

        self.registar_komponenti = RegistarKomponenti.dobavi_instancu()

    @staticmethod
    def dobavi_instancu():
        if Jezgro.instanca is None:
            Jezgro.instanca = Jezgro()
        return Jezgro.instanca

    def obavesti(self, simbolicko_ime):
        ui = self.registar_komponenti.dobavi_komponentu(simbolicko_ime).dobavi_qwidget()
        # print(self.registar_komponenti.dobavi_komponentu(simbolicko_ime).__mro__)

        # ui.show()
        return ui