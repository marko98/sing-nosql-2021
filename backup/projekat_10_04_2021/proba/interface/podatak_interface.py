class PodatakInterface:
    # def dobavi_vrednost(self):
    #     raise NotImplementedError('metoda dobavi_vrednost nije implementirana')

    # def postavi_vrednost(self, vrednost):
    #     raise NotImplementedError('metoda postavi_vrednost nije implementirana')

    def dobavi_atribut(self):
        raise NotImplementedError('metoda dobavi_atribut nije implementirana')

    def postavi_atribut(self, atribut):
        raise NotImplementedError('metoda postavi_atribut nije implementirana')

    def dobavi_atribut_vrednost(self):
        raise NotImplementedError('metoda dobavi_atribut_vrednost nije implementirana')