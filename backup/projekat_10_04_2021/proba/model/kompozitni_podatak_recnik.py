from ..interface.podatak_interface import PodatakInterface

class KompozitniPodatakRecnik(PodatakInterface):

    def __init__(self, deca=[], atribut=None):
        super(KompozitniPodatakRecnik, self).__init__()

        self.atribut = atribut

        for dete in deca:
            if dete.dobavi_atribut() is None:
                raise ValueError('dete u okviru kopozitnog recnika mora imati atribut')
        self.deca = deca

    def dodaj_dete(self, dete : PodatakInterface):
        if dete.dobavi_atribut() is None:
            raise ValueError('dete u okviru kopozitnog recnika mora imati atribut')
        self.deca.append(dete)

    def obrisi_dete(self, dete : PodatakInterface):
        for index, _dete in enumerate(self.deca):
            if _dete == dete:
                self.deca.pop(index)
                return dete
        return None

    def dobavi_decu(self):
        return self.deca

    def postavi_decu(self, deca=[]):
        for dete in deca:
            if dete.dobavi_atribut() is None:
                raise ValueError('dete u okviru kopozitnog recnika mora imati atribut')
        self.deca = deca
    
    # def dobavi_vrednost(self):
    #     atribut_vrednost = {}

    #     for dete in self.deca:
    #         atribut_vrednost.update(dete.dobavi_atribut_vrednost())
        
    #     return atribut_vrednost

    # def postavi_vrednost(self, deca):
    #     for dete in deca:
    #         if dete.dobavi_atribut() is None:
    #             raise ValueError('dete u okviru kopozitnog recnika mora imati atribut')
    #     self.deca = deca

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        if isinstance(atribut, str):
            self.atribut = atribut
        else:
            raise ValueError('atribut moze biti samo tipa string')

    def dobavi_atribut_vrednost(self):
        atribut_vrednost = {}

        for dete in self.deca:
            atribut_vrednost.update(dete.dobavi_atribut_vrednost())

        if self.atribut is None:
            return atribut_vrednost

        return {self.atribut: atribut_vrednost}