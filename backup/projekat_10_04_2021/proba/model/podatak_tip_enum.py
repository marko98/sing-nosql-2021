# https://docs.python.org/3/library/enum.html
from enum import Enum

class PodatakTip(Enum):
    STRING = 'string'
    NUMBER = 'number'
    DATE = 'date'
    BOOLEAN = 'boolean'
    BLOB = 'blob'
    TEXT = 'text'