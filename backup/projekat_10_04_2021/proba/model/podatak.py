from ..interface.podatak_interface import PodatakInterface
from .podatak_tip_enum import PodatakTip

class Podatak(PodatakInterface):

    def __init__(self, vrednost=None, atribut=None, tip=PodatakTip.STRING, deo_primarnog_kljuca=False, obavezan=False): #deo_primarnog_kljuca jer primarni kljuc moze biti kompozitan
        super(Podatak, self).__init__()

        self.atribut = atribut
        self.vrednost = vrednost
        self.tip = tip
        self.obavezan = obavezan
        self.deo_primarnog_kljuca = deo_primarnog_kljuca

        if self.deo_primarnog_kljuca:
            self.obavezan = True

    def dobavi_vrednost(self):
        return self.vrednost

    def postavi_vrednost(self, vrednost):
        self.vrednost = vrednost

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        if isinstance(atribut, str):
            self.atribut = atribut
        else:
            raise ValueError('atribut moze biti samo tipa string')

    def dobavi_atribut_vrednost(self):
        if self.atribut is None:
            return self.vrednost
        return {self.atribut: self.vrednost}