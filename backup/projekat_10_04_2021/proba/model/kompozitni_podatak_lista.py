from ..interface.podatak_interface import PodatakInterface

class KompozitniPodatakLista(PodatakInterface):

    def __init__(self, deca=[], atribut=None):
        super(KompozitniPodatakLista, self).__init__()

        self.atribut = atribut
        self.deca = deca

    def dodaj_dete(self, dete : PodatakInterface):
        self.deca.append(dete)

    def obrisi_dete(self, dete : PodatakInterface):
        for index, _dete in enumerate(self.deca):
            if _dete == dete:
                self.deca.pop(index)
                return dete
        return None

    def dobavi_decu(self):
        return self.deca

    def postavi_decu(self, deca=[]):
        self.deca = deca

    # def dobavi_vrednost(self):
    #     return [dete.dobavi_atribut_vrednost() for dete in self.deca]

    # def postavi_vrednost(self, deca):
    #     self.deca = deca

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        if isinstance(atribut, str):
            self.atribut = atribut
        else:
            raise ValueError('atribut moze biti samo tipa string')

    def dobavi_atribut_vrednost(self):
        if self.atribut is None:
            return [dete.dobavi_atribut_vrednost() for dete in self.deca]
            
        return {self.atribut: [dete.dobavi_atribut_vrednost() for dete in self.deca]}