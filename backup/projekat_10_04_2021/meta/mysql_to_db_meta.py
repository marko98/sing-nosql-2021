# parser je osetljiv na strukturu sql file-a!!!

import json

def get_type(tip):
    # types: string, date, boolean, number, blob, text

    _tip = 'STRING'

    if 'date' in tip:
        _tip = 'DATE'
    elif 'bool' in tip:
        _tip = 'BOOLEAN'
    elif 'blob' in tip:
        _tip = 'BLOB'
    elif 'text' in tip:
        _tip = 'TEXT'
    elif any(_t in tip for _t in ['int', 'numeric', 'smallint', 'bigint', 'decimal']):
        _tip = 'NUMBER'

    return _tip

def get_table(table : list):
    table_name = table.pop(0).replace("create table ", "")
    
    table.pop(0)
    table.pop()

    primary_keys = []
    if 'primary key' in table[-1]:
        primary_keys = table.pop().split('(')[1].replace(')', '').split(', ')

    atributi = []
    for line in table:
        atr = {'obavezan': False, 'jedinstven': False}

        parts = line.split(' ')
        parts = list(filter(lambda x: x != '', parts))

        for index, part in enumerate(parts):
            if index == 0:
                atr['naziv'] = part
            elif index == 1:
                atr['tip'] = get_type(part)
            elif index == 2 or index == 3:
                atr['obavezan'] = True

        atributi.append(atr)

    for primary_key in primary_keys:
        for atr in atributi:
            if atr['naziv'] == primary_key:
                atr['jedinstven'] = True

    return {"naziv": table_name, 'atributi': atributi, 'primarni_kljucevi': primary_keys, 'deca': {'po_hijerarhiji': [], 'po_relaciji': []}, 'roditelji': {'po_hijerarhiji': [], 'po_relaciji': []}}
    
def add_parent_child_relation(foreign_key_line, tables : list):
    # da li ima potrebe staviti po kom atributu su povezani, jer kako bismo to uradili u dokumentalistickoj bazi?
    child_table_name = foreign_key_line.split(' ')[2]
    child_table_referencing_attributes = foreign_key_line.split('(')[1].split(')')[0].split(', ')
    parent_table_name = foreign_key_line.split('references ')[1].split(' ')[0]
    parent_table_referenced_attributes = foreign_key_line.split('(')[2].split(')')[0].split(', ')

    brojac = 0

    dete_po_relaciji = True
    for table in tables:
        if table['naziv'] == child_table_name:
            if all(child_table_referencing_attribute in table['primarni_kljucevi'] for child_table_referencing_attribute in child_table_referencing_attributes):
                dete_po_relaciji = False

    for table in tables:
        if table['naziv'] == child_table_name:
            # table['roditelji'].append(parent_table_name)
            
            # table['roditelji'].append({'naziv': parent_table_name, 'referencirajuci_atributi': child_table_referencing_attributes, \
            #     'referencirani_atributi_roditelja': parent_table_referenced_attributes})

            reference = []
            for index, referencirajuci_atribut in enumerate(child_table_referencing_attributes):
                reference.append({"atribut": referencirajuci_atribut, 'roditeljski_atribut': parent_table_referenced_attributes[index]})

            if not dete_po_relaciji:
                table['roditelji']['po_hijerarhiji'].append({'naziv': parent_table_name, 'reference': reference})
            else:
                table['roditelji']['po_relaciji'].append({'naziv': parent_table_name, 'reference': reference})

            brojac += 1
    
        if table['naziv'] == parent_table_name:
            if not dete_po_relaciji:
                table['deca']['po_hijerarhiji'].append(child_table_name)
            else:
                table['deca']['po_relaciji'].append(child_table_name)
            brojac += 1

        if brojac == 2:
            break

with open('./meta/MySQLSkript_2001_BazaCinjenica.sql', 'r') as fp:
# with open('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.sql', 'r') as fp:
    lines = list(filter(lambda line: line != '\n', fp.readlines()))

    # db name
    for line in lines:
        if line.startswith('create schema'):
            db_name = line.split('`')[1]

    # konekcija
    konekcija = {'host': 'localhost', 'korisnicko_ime': 'root', 'lozinka': 'root'}

    # tabele
    tables = []
    table_lines = []
    add_to_table_lines = False
    for line in lines:
        if line.startswith('create table'):
            add_to_table_lines = True
        
        if add_to_table_lines:
            table_lines.append(line.strip())

        if add_to_table_lines is True and line.startswith(');\n'):
            tables.append(table_lines)
            table_lines = []
            add_to_table_lines = False
        
    _tables = [get_table(table) for table in tables]
    tables = _tables

    # strani kljucevi
    foreign_key_lines = []
    foreign_key_line = ''
    for line in lines:
        line = line.strip()
        if 'alter table' in line and 'foreign key' in line or foreign_key_line != '':
            if ';' in line:
                foreign_key_line += line
                foreign_key_lines.append(foreign_key_line)
                foreign_key_line = ''
            else:
                foreign_key_line += line + ' '

    for foreign_key_line in foreign_key_lines:
        add_parent_child_relation(foreign_key_line, tables)

    with open('./meta/informacioni_resurs_generisano.json', 'w') as outfile:
    # with open('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.json', 'w') as outfile:
        json.dump({'db_naziv': db_name, 'konekcija': konekcija, 'tabele': tables}, outfile)
