create schema IF NOT EXISTS `lazna_db` DEFAULT CHARACTER SET utf8 ;
USE `lazna_db` ;

create table DRZAVA 
(
   DR_IDENTIFIKATOR char(3) NOT NULL,
   primary key (DR_IDENTIFIKATOR)
);

create table NASELJENO_MESTO 
(
   NM_IDENTIFIKATOR char(5) NOT NULL,
   DR_IDENTIFIKATOR char(3) NOT NULL,
   primary key (NM_IDENTIFIKATOR, DR_IDENTIFIKATOR)
);

create table REGION 
(
   RE_IDENTIFIKATOR char(10) NOT NULL,
   DR_IDENTIFIKATOR char(3),
   primary key (RE_IDENTIFIKATOR)
);

alter table NASELJENO_MESTO add constraint FK_NASELJENO_MESTO_0 foreign key (DR_IDENTIFIKATOR) references DRZAVA (DR_IDENTIFIKATOR);

alter table REGION add constraint FK_REGION_0 foreign key (DR_IDENTIFIKATOR) references DRZAVA (DR_IDENTIFIKATOR);