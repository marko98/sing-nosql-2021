from PyQt5 import QtWidgets, QtCore, uic
import os

from core.model.jezgro import Jezgro
from business_logic.podatak.pattern.creational.abstract_factory.abstract_factory import AbstractFactory
from business_logic.podatak.interface.podatak_interface import PodatakInterface
from business_logic.podatak.model.podatak_tip import PodatakTip
from business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

from .forma_gui import FormaGUI

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from PyQt5 import QtGui
    from business_logic.podatak.pattern.creational.abstract_factory.abstract_factory_interface import AbstractFactoryInterface
    from business_logic.pattern.behavioural.observer.model.observable import Observable
    from business_logic.podatak.interface.kompozit_interface import KompozitInterface
    from business_logic.podatak.interface.list import ListInterface
    from business_logic.podatak.model.lisni_podatak import LisniPodatak

class FormaZaKreiranjeGUI:
    def __init__(self, kompozitni_podatak):
        super(FormaZaKreiranjeGUI, self).__init__()

        kompozitni_podatak : PodatakInterface
        if kompozitni_podatak.dobavi_tip() != PodatakTip.BATCH:
            raise ValueError('pokusano je instanciranje klase FormaZaKreiranjeGUI sa kompozitnim podatkom koji nije tipa BATCH')
        
        self.kompozitni_podatak = kompozitni_podatak

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_entitetima', 'ui', 'create_update_delete.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QDialog
        self.ui.setWindowTitle('Kreiranje entiteta')

        self.jezgro = Jezgro.dobavi_instancu()
        self.jezgro : Observable
        self.jezgro.zakaci_observer(lambda x : self.proveri_rukovaoca_skladistem(x.rukovalac_skladistem))
        self.rukovalac_skladistem = self.jezgro.rukovalac_skladistem
        self.rukovalac_skladistem : RukovalacSkladistemInterface

        self.metapodaci_o_batchu = None
        for tabela in self.rukovalac_skladistem.dobavi_metapodatak_baze()['tabele']:
            if tabela['naziv'] == self.kompozitni_podatak.dobavi_atribut():
                self.metapodaci_o_batchu = tabela
                break
        if self.metapodaci_o_batchu is None:
            raise RuntimeError('nisu pronadjeni metapodaci o batch-u')

        ab_factory = AbstractFactory.dobavi_instancu()
        ab_factory : AbstractFactoryInterface 
        self.novi_entitet = ab_factory.kreiraj_entitet(self.metapodaci_o_batchu)
        self.novi_entitet : KompozitInterface

        # ui elements
        self.l_naslov = self.ui.l_naslov
        self.l_naslov : QtWidgets.QLabel
        self.l_naslov.setText(self.kompozitni_podatak.dobavi_reprezentacioni_atribut())

        self.scroll_area = self.ui.scroll_area
        self.scroll_area : QtWidgets.QScrollArea

        self.btn_potvrdi = self.ui.btn_potvrdi
        self.btn_potvrdi : QtWidgets.QPushButton

        self.btn_odustani = self.ui.btn_odustani
        self.btn_odustani : QtWidgets.QPushButton

        # connections
        self.btn_potvrdi.clicked.connect(self._potvrdi)
        self.btn_odustani.clicked.connect(self.ui.close)

        # polja
        self.entitet_sacuvan = False

        # metode
        self._izgradi_polja()

        self.ui.closeEvent = self._zatvori_dialog
        self.ui.exec()

    def _izgradi_polja(self):
        self.vbox = QtWidgets.QVBoxLayout()

        height = 50
        for index, atribut in enumerate(self.novi_entitet.dobavi_decu()):
            horizontal_layout_widget = QtWidgets.QWidget()
            horizontal_layout_widget.setGeometry(QtCore.QRect(10, 10 + index*height, 461, 41))

            horizontal_layout = QtWidgets.QHBoxLayout(horizontal_layout_widget)
            horizontal_layout.setContentsMargins(0, 0, 0, 0)

            self._dobavi_polja_spram_tipa(atribut, horizontal_layout_widget, horizontal_layout)

            # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
            # horizontal_layout.addItem(spacerItem)

            self.vbox.addWidget(horizontal_layout_widget)

        self.scroll_area.widget().setLayout(self.vbox)

    def _dobavi_polja_spram_tipa(self, atribut, horizontal_layout_widget, horizontal_layout):
        atribut : PodatakInterface

        label = QtWidgets.QLabel(horizontal_layout_widget)
        label.setText(atribut.dobavi_reprezentacioni_atribut() + ":")
        horizontal_layout.addWidget(label)

        atribut : LisniPodatak
        if atribut.dobavi_referencira():
            if atribut.dobavi_obavezan():
                # referenciranje po hijerarhiji

                label_vrednost = QtWidgets.QLabel(horizontal_layout_widget)
                # label_vrednost.setText("ref. po relaciji")
                horizontal_layout.addWidget(label_vrednost)

                pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
                pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
                pushButton.setText('odaberi')
                horizontal_layout.addWidget(pushButton)

                label = QtWidgets.QLabel(horizontal_layout_widget)
                label.setText("ref. po hijerarhiji")
                horizontal_layout.addWidget(label)

            elif not atribut.dobavi_obavezan():
                # referenciranje po relaciji

                label_vrednost = QtWidgets.QLabel(horizontal_layout_widget)
                # label_vrednost.setText("ref. po relaciji")
                horizontal_layout.addWidget(label_vrednost)

                pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
                pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
                pushButton.setText('odaberi')
                horizontal_layout.addWidget(pushButton)

                label = QtWidgets.QLabel(horizontal_layout_widget)
                label.setText("ref. po relaciji")
                horizontal_layout.addWidget(label)
        else:
            # ne referencira
            if atribut.dobavi_tip() == PodatakTip.STRING:
                lineEdit = QtWidgets.QLineEdit(horizontal_layout_widget)
                # lineEdit.setObjectName("lineEdit2")
                horizontal_layout.addWidget(lineEdit)
            elif atribut.dobavi_tip() == PodatakTip.TEXT:
                textEdit = QtWidgets.QTextEdit(horizontal_layout_widget)
                textEdit.setEnabled(True)
                # textEdit.setObjectName("textEdit")
                horizontal_layout.addWidget(textEdit)
            elif atribut.dobavi_tip() == PodatakTip.NUMBER:
                spinBox = QtWidgets.QSpinBox(horizontal_layout_widget)
                # spinBox.setObjectName("spinBox")
                horizontal_layout.addWidget(spinBox)
            elif atribut.dobavi_tip() == PodatakTip.DATE:
                dateTimeEdit = QtWidgets.QDateTimeEdit(horizontal_layout_widget)
                # dateTimeEdit.setObjectName("dateTimeEdit")
                horizontal_layout.addWidget(dateTimeEdit)
            elif atribut.dobavi_tip() == PodatakTip.BOOLEAN:
                checkBox = QtWidgets.QCheckBox(horizontal_layout_widget)
                # checkBox.setObjectName("checkBox")
                horizontal_layout.addWidget(checkBox)
            elif atribut.dobavi_tip() == PodatakTip.BLOB:
                pushButton = QtWidgets.QPushButton(horizontal_layout_widget)
                pushButton.setStyleSheet("background-color: rgb(255, 255, 255);\n" "font: 8pt \"Segoe UI\";")
                # pushButton.setObjectName("pushButton")
                pushButton.setText('izaberi')
                horizontal_layout.addWidget(pushButton)

    def _zatvori_dialog(self, closeEvent):
        closeEvent : QtGui.QCloseEvent

        if not self.entitet_sacuvan:
            for atribut in self.novi_entitet.dobavi_decu():
                referenca = atribut.dobavi_referenciran_od()
                if referenca and referenca['vrednost']:
                    atribut : ListInterface
                    atribut.postavi_referencira(None)
        
        # print('_zatvori_dialog')

    def _potvrdi(self):
        # sacuvaj entitet

        # ako je uspesno:
        self.entitet_sacuvan = True
        self.ui.close()
        # print('_potvrdi')

    def proveri_rukovaoca_skladistem(self, rukovalac_skladistem):
        if rukovalac_skladistem != self.rukovalac_skladistem:
            self.zatvori_formu()
