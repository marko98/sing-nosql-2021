from PyQt5 import QtWidgets, uic
import os
import math

from business_logic.podatak.model.podatak_tip import PodatakTip
from .business_logic.model.tabela_gui_kontroler import TabelaGUIKontroler
from .forma_gui_2 import FormaGUI
from .forma_za_kreiranje_gui import FormaZaKreiranjeGUI

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from PyQt5 import QtGui
    from ...business_logic.podatak.model.kompozitni_podatak import KompozitniPodatak
    from ...business_logic.podatak.model.podatak import Podatak

class TabelaGUI:
    instance = []

    def __init__(self, kompozitni_podatak):
        super(TabelaGUI, self).__init__()

        kompozitni_podatak : KompozitniPodatak
        if kompozitni_podatak.dobavi_tip() != PodatakTip.BATCH:
            raise ValueError('pokusano je instanciranje klase TabelaGUI sa kompozitnim podatkom koji nije tipa BATCH')
        
        self.kompozitni_podatak = kompozitni_podatak

        self.kontroler = TabelaGUIKontroler(self)

        self.ui_path = os.path.join(os.getcwd(), 'components', 'komponenta_za_rad_sa_entitetima', 'ui', 'tabel.ui')
        self.ui = uic.loadUi(self.ui_path)
        self.ui : QtWidgets.QDialog
        self.ui.setWindowTitle('Tabelarni prikaz')

        # ui elements
        self.cb_roditelji = self.ui.cb_roditelji
        self.cb_roditelji : QtWidgets.QComboBox

        self.cb_deca = self.ui.cb_deca
        self.cb_deca : QtWidgets.QComboBox

        self.btn_otvori_roditelj_tabelu = self.ui.btn_otvori_roditelj_tabelu
        self.btn_otvori_roditelj_tabelu : QtWidgets.QPushButton

        self.btn_otvori_dete_tabelu = self.ui.btn_otvori_dete_tabelu
        self.btn_otvori_dete_tabelu : QtWidgets.QPushButton

        self.table = self.ui.table
        self.table : QtWidgets.QTableWidget
        self.table.setSortingEnabled(True)

        self.btn_zatvori_dialog = self.ui.btn_zatvori_dialog
        self.btn_zatvori_dialog : QtWidgets.QPushButton
        
        self.label_naziv_batcha = self.ui.label_naziv_batcha
        self.label_naziv_batcha : QtWidgets.QLabel
        self.label_naziv_batcha.setText(self.kompozitni_podatak.dobavi_reprezentacioni_atribut())

        self.btn_obrisi = self.ui.btn_obrisi
        self.btn_obrisi : QtWidgets.QPushButton

        self.sp_box_broj_strane = self.ui.sp_box_broj_strane
        self.sp_box_broj_strane : QtWidgets.QSpinBox
        self.sp_box_broj_strane.setMinimum(1)
        self.sp_box_broj_strane.setMaximum(self.kontroler.dobavi_broj_mogucih_strana(self.kompozitni_podatak))

        self.sp_box_limit = self.ui.sp_box_limit
        self.sp_box_limit : QtWidgets.QSpinBox
        self.sp_box_limit.setValue(self.kontroler.limit)
        self.sp_box_limit.setMinimum(1)
        self.sp_box_limit.setMaximum(20)

        self.btn_prva_strana = self.ui.btn_prva_strana
        self.btn_prva_strana : QtWidgets.QPushButton

        self.btn_leva_strana = self.ui.btn_leva_strana
        self.btn_leva_strana : QtWidgets.QPushButton

        self.btn_desna_strana = self.ui.btn_desna_strana
        self.btn_desna_strana : QtWidgets.QPushButton

        self.btn_poslednja_strana = self.ui.btn_poslednja_strana
        self.btn_poslednja_strana : QtWidgets.QPushButton

        self.btn_prvi_entitet = self.ui.btn_prvi_entitet
        self.btn_prvi_entitet : QtWidgets.QPushButton

        self.btn_gornji_entitet = self.ui.btn_gornji_entitet
        self.btn_gornji_entitet : QtWidgets.QPushButton

        self.btn_donji_entitet = self.ui.btn_donji_entitet
        self.btn_donji_entitet : QtWidgets.QPushButton

        self.btn_poslednji_entitet = self.ui.btn_poslednji_entitet
        self.btn_poslednji_entitet : QtWidgets.QPushButton

        self.btn_kreiraj_entitet = self.ui.btn_kreiraj_entitet
        self.btn_kreiraj_entitet : QtWidgets.QPushButton

        # polja
        self.strana = 0
        self.entiteti = []   
        self.selektovani_red = 0     

        TabelaGUI.instance.append(self)

        # connections
        self.btn_otvori_roditelj_tabelu.clicked.connect(lambda _: self.otvori_tabelu(self.cb_roditelji.currentText()))
        self.btn_otvori_dete_tabelu.clicked.connect(lambda _: self.otvori_tabelu(self.cb_deca.currentText()))
        self.btn_zatvori_dialog.clicked.connect(self.ui.close)
        self.btn_obrisi.clicked.connect(self.obrisi)
        self.sp_box_broj_strane.valueChanged.connect(lambda x: self.idi_na_stranu(self.sp_box_broj_strane.value()-1))
        self.sp_box_limit.valueChanged.connect(lambda x: self.promeni_limit(self.sp_box_limit.value()))

        self.btn_prva_strana.clicked.connect(lambda _: self.idi_na_stranu(0))
        self.btn_leva_strana.clicked.connect(lambda _: self.idi_na_stranu(0 if self.strana - 1 < 0 else self.strana - 1))
        self.btn_desna_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1 if self.strana + 1 > self.sp_box_broj_strane.maximum()-1 else self.strana + 1))
        self.btn_poslednja_strana.clicked.connect(lambda _: self.idi_na_stranu(self.sp_box_broj_strane.maximum()-1))

        self.btn_prvi_entitet.clicked.connect(lambda _: self._selektuj_red(0))
        self.btn_gornji_entitet.clicked.connect(lambda _: self._selektuj_red(self.selektovani_red-1))
        self.btn_donji_entitet.clicked.connect(lambda _: self._selektuj_red(self.selektovani_red+1))
        self.btn_poslednji_entitet.clicked.connect(lambda _: self._selektuj_red(self.kontroler.ukupan_broj_entiteta_batcha-1))

        self.btn_kreiraj_entitet.clicked.connect(self.otvori_formu_za_kreiranje)

        # methods
        self.popuni_cb_roditelji()
        self.popuni_cb_deca()
        self.dobavi_entitete()
        self.popuni_tabelu()

        self.ui.closeEvent = self._zatvori_dialog
        self.ui.exec()
        
    def _zatvori_dialog(self, closeEvent):
        closeEvent : QtGui.QCloseEvent

        self.table.selectAll()
        items = self.table.selectedItems()
        for item in items:
            podatak = item.podatak
            podatak : Podatak
            podatak.widgets = list(filter(lambda widget_data: widget_data['widget'] != item, podatak.widgets))

        for index, tabela_gui in enumerate(TabelaGUI.instance):
            if tabela_gui == self:
                TabelaGUI.instance.pop(index)

    def popuni_cb_roditelji(self):
        roditelji = self.kompozitni_podatak.dobavi_roditelje_po_hijerarhiji() + self.kompozitni_podatak.dobavi_roditelje_po_relaciji()

        # proverimo da li vec postoji instanca sa nekim od roditelja, ako postoji necemo ga prikazati medju roditeljima
        otvoreni_roditelji = list(map(lambda x: x.kompozitni_podatak, TabelaGUI.instance))
        roditelji = list(filter(lambda x: x not in otvoreni_roditelji, roditelji))

        if len(roditelji) == 0:
            self.ui.label_roditelji.setVisible(False)
            self.cb_roditelji.setVisible(False)
            self.btn_otvori_roditelj_tabelu.setVisible(False)

        self.cb_roditelji.addItems(list(map(lambda x: x.dobavi_reprezentacioni_atribut(), roditelji)))

    def popuni_cb_deca(self):
        deca = self.kompozitni_podatak.dobavi_decu_po_hijerarhiji() + self.kompozitni_podatak.dobavi_decu_po_relaciji()

        # proverimo da li vec postoji instanca sa nekim od dece, ako postoji necemo ga prikazati medju decom
        otvorena_deca = list(map(lambda x: x.kompozitni_podatak, TabelaGUI.instance))
        deca = list(filter(lambda x: x not in otvorena_deca, deca))

        if len(deca) == 0:
            self.ui.label_deca.setVisible(False)
            self.cb_deca.setVisible(False)
            self.btn_otvori_dete_tabelu.setVisible(False)

        self.cb_deca.addItems(list(map(lambda x: x.dobavi_reprezentacioni_atribut(), deca)))

    def popuni_tabelu(self):
        # prvo otkaci bilo kakvu decu u tabeli
        self.table.selectAll()
        for item in self.table.selectedItems():
            item.podatak.widgets = list(filter(lambda x: x['widget'] != item, item.podatak.widgets))
            item.podatak = None

        self.table.setRowCount(0)

        metapodatak = self.kontroler.dobavi_metapodatak_baze()

        columns = []
        for batch in metapodatak['tabele']:
            if batch['naziv'] == self.kompozitni_podatak.dobavi_atribut():
                columns = list(map(lambda x: x['reprezentacioni_naziv'], batch['atributi']))

                self.table.setColumnCount(len(columns))
                self.table.setHorizontalHeaderLabels(columns)


                rows = []
                for entitet in self.entiteti:
                    entitet : KompozitniPodatak
                    if entitet.dobavi_tip() != PodatakTip.ENTITET:
                        raise RuntimeError("entitet nije tipa ENTITET")

                    row = entitet.dobavi_decu()
                    # for atribut_podatak in entitet.dobavi_decu():
                    #     atribut_podatak : Podatak
                    #     row_data.append(atribut_podatak.dobavi_vrednost())

                    rows.append(row)

                for row in rows:
                    self.table.setRowCount(self.table.rowCount() + 1)
                    row_index = self.table.rowCount() - 1
                    col_index = 0
                    for col in row:
                        col : Podatak

                        table_item = QtWidgets.QTableWidgetItem((str(col.dobavi_vrednost())))
                        col.widgets.append({'widget': table_item, 'tipovi': None})
                        table_item.podatak = col

                        self.table.setItem(row_index, col_index, table_item)
                        col_index += 1

                strana_za_selektovani_red = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
                if self.strana != strana_za_selektovani_red:
                    self.selektovani_red = self.strana * self.kontroler.limit

                # print(self.selektovani_red - self.kontroler.limit * self.strana)
                self.table.selectRow(self.selektovani_red - self.kontroler.limit * self.strana)
                return

        raise RuntimeError('kompozitni podatak nije pronadjen u metapodatku baze')

    def otvori_tabelu(self, batch_reprezentacioni_naziv):
        batch = self.kontroler.dobavi_batch_po_reprezentacionom_atributu(batch_reprezentacioni_naziv)
        TabelaGUI(batch)

    def obrisi(self):
        entiteti = []
        
        selected_items = self.table.selectedItems()
        for table_item in selected_items:
            podatak = table_item.podatak
            podatak : Podatak
            
            entitet = podatak.dobavi_roditelja()
            if entitet.dobavi_tip() != PodatakTip.ENTITET:
                raise RuntimeError('u tabeli se nalazi podatak koji za roditelja nema kompozitni podatak tipa ENTITET')
            
            if entitet not in entiteti:
                entiteti.append(entitet)



        print('obrisi')

    def idi_na_stranu(self, strana):
        # print(self.strana)
        
        if self.strana != strana:
            self.strana = strana

            self.sp_box_broj_strane.setValue(self.strana+1)
            self.dobavi_entitete()
            self.popuni_tabelu()        

    def promeni_limit(self, limit):
        self.kontroler.postavi_limit(limit)
        br_strana = self.kontroler.dobavi_broj_mogucih_strana(self.kompozitni_podatak)
        # print('br_strana', br_strana)
        self.sp_box_broj_strane.setMaximum(br_strana)

        self.dobavi_entitete()
        self.popuni_tabelu()

    def _selektuj_red(self, red):
        if red < 0:
            red = 0
        elif red > self.kontroler.ukupan_broj_entiteta_batcha - 1:
            red = self.kontroler.ukupan_broj_entiteta_batcha - 1

        self.selektovani_red = red
        strana = math.ceil((self.selektovani_red+1)/self.kontroler.limit)-1
        # print(strana)

        if self.strana != strana:
            self.idi_na_stranu(strana)
        else:
            self.table.clearSelection()
            self.table.selectRow(self.selektovani_red - self.kontroler.limit * self.strana)
        
    def _dobavi_start_end_index(self):
        start_index = self.strana * self.kontroler.limit
        end_index = self.kontroler.ukupan_broj_entiteta_batcha - 1 if start_index + self.kontroler.limit - 1 > self.kontroler.ukupan_broj_entiteta_batcha - 1 else start_index + self.kontroler.limit - 1
        return start_index, end_index

    def dobavi_entitete(self):
        # print('idi_na_stranu', self.strana)
        start_index, end_index = self._dobavi_start_end_index()
        # print('poc. index,', start_index, ', krajnji index,', end_index)

        self.entiteti = []
        self.entiteti = self.kontroler.dobavi_entitete(self.kompozitni_podatak, start_index, end_index)

    def otvori_formu_za_kreiranje(self):
        # forma_za_kreiranje = FormaGUI(self.kompozitni_podatak)
        forma_za_kreiranje = FormaZaKreiranjeGUI(self.kompozitni_podatak)