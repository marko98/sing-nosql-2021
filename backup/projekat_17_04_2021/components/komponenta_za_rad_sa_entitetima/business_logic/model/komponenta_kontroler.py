from core.model.jezgro import Jezgro

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...komponenta import KomponentaZaUcitavanje
    from business_logic.pattern.behavioural.observer.model.observable import Observable
    from business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class KomponentaKontroler:
    def __init__(self, gui_komponenta):
        super(KomponentaKontroler, self).__init__()
        # print(__name__)

        self.gui_komponenta = gui_komponenta
        self.gui_komponenta : KomponentaZaUcitavanje        

        self.jezgro = Jezgro.dobavi_instancu()
        self.jezgro : Observable
        self.jezgro.zakaci_observer(lambda x : self.postavi_rukovaoca_skladistem(x.rukovalac_skladistem))

        self.rukovalac_skladistem = self.jezgro.rukovalac_skladistem
        self.rukovalac_skladistem : RukovalacSkladistemInterface

    def popuni_tree_baza(self):
        baza = self.rukovalac_skladistem.dobavi_kompozitni_podatak_DATABASE()

        self.gui_komponenta.popuni_tree_baza(baza)

        # self.jezgro : Jezgro
        # self.jezgro.postavi_rukovaoca_skladistem()

    def isprazni_tree_baza(self):
        baza = self.rukovalac_skladistem.dobavi_kompozitni_podatak_DATABASE()

        self.gui_komponenta.isprazni_tree_baza(baza)

    def postavi_rukovaoca_skladistem(self, rukovalac_skladistem):
        self.rukovalac_skladistem = rukovalac_skladistem