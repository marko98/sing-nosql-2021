from core.model.jezgro import Jezgro
import math

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ...tabela_gui import TabelaGUI
    from business_logic.pattern.behavioural.observer.model.observable import Observable
    from business_logic.rukovalac_skladistem.interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

class TabelaGUIKontroler:
    def __init__(self, tabela_gui):
        super(TabelaGUIKontroler, self).__init__()
        # print(__name__)

        self.tabela_gui = tabela_gui
        self.tabela_gui : TabelaGUI        

        self.jezgro = Jezgro.dobavi_instancu()
        self.jezgro : Observable
        self.jezgro.zakaci_observer(lambda x : self.proveri_rukovaoca_skladistem(x.rukovalac_skladistem))

        self.rukovalac_skladistem = self.jezgro.rukovalac_skladistem
        self.rukovalac_skladistem : RukovalacSkladistemInterface

        self.limit = 2
        self.ukupan_broj_entiteta_batcha = 0
        

    def postavi_limit(self, limit):
        self.limit = limit

    def zatvori_dialog(self):
        self.tabela_gui.zatvori_dialog()

    def proveri_rukovaoca_skladistem(self, rukovalac_skladistem):
        if rukovalac_skladistem != self.rukovalac_skladistem:
            self.zatvori_dialog()

    def dobavi_metapodatak_baze(self):
        return self.rukovalac_skladistem.dobavi_metapodatak_baze()

    def dobavi_batch(self, batch_ime):
        return self.rukovalac_skladistem.dobavi_batch(batch_ime)

    def dobavi_batch_po_reprezentacionom_atributu(self, batch_reprezentacioni_naziv):
        return self.rukovalac_skladistem.dobavi_batch_po_reprezentacionom_atributu(batch_reprezentacioni_naziv)
    
    def broj_entiteta_batcha(self, kompozitni_podatak):
        self.ukupan_broj_entiteta_batcha = self.rukovalac_skladistem.broj_entiteta_batcha(kompozitni_podatak)
        return self.ukupan_broj_entiteta_batcha

    def dobavi_broj_mogucih_strana(self, kompozitni_podatak):
        br_strana = math.ceil(self.broj_entiteta_batcha(kompozitni_podatak) / self.limit)
        return br_strana

    def dobavi_entitete(self, kompozitni_podatak, start_index, end_index):
        # print('poc. index,', start_index, ', krajnji index,', end_index)
        return self.rukovalac_skladistem.dobavi(kompozitni_podatak, start_index, end_index)