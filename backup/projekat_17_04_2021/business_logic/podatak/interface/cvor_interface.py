class CvorInterface:
    def dobavi_dubinu(self):
        raise NotImplementedError('metoda dobavi_dubinu nije implementirana')

    def dobavi_redni_broj(self, tip=None):
        raise NotImplementedError('metoda dobavi_redni_broj nije implementirana')

    def dobavi_roditelja(self):
        raise NotImplementedError('metoda dobavi_roditelja nije implementirana')

    def postavi_roditelja(self, roditelja):
        raise NotImplementedError('metoda postavi_roditelja nije implementirana')