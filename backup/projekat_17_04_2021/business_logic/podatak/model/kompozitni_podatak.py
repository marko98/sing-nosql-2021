from ..interface.podatak_interface import PodatakInterface
from ..interface.kompozit_interface import KompozitInterface
from ..interface.deo_stabla_interface import DeoStablaInterface
from ..interface.cvor_interface import CvorInterface
from .podatak_tip import PodatakTip

from PyQt5 import QtWidgets

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from ..interface.podatak_widget_interface import PodatakWidgetInterface

class KompozitniPodatak(PodatakInterface, KompozitInterface, DeoStablaInterface, CvorInterface):

    def __init__(self):
        super(KompozitniPodatak, self).__init__()

        self.atribut = None
        self.reprezentacioni_atribut = None
        self.tip = None
        self.widgets = []
        
        self.roditelj = None
        self.deca = []

        self.roditelji_po_hijerarhiji = []
        self.deca_po_hijerarhiji = []

        self.roditelji_po_relaciji = []
        self.deca_po_relaciji = []

    def __str__(self):
        self.roditelj : PodatakInterface

        return 'tip: {}, atribut: {}, roditelj_atribut: {}, deca: {}, roditelji_po_hijerarhiji: {}, deca_po_hijerarhiji: {}, roditelji_po_relaciji: {}, \
deca_po_relaciji: {}, dubina: {}, broj_deteta: {}'.format(\
            self.tip, self.atribut, self.roditelj.dobavi_atribut() if self.roditelj is not None else None, self.deca, \
            self.roditelji_po_hijerarhiji, self.deca_po_hijerarhiji, self.roditelji_po_relaciji, self.deca_po_relaciji, \
            self.dobavi_dubinu(), self.dobavi_broj_deteta())

        # # potpun roditelj
        # return 'tip: {}, atribut: {}, roditelj: {}, deca: {}'.format(\
        #     self.tip, self.atribut, self.roditelj, self.deca,)

    # KompozitInterface
    def dodaj_dete(self, dete : PodatakInterface):
        dete.postavi_roditelja(self)
        self.deca.append(dete)

    def obrisi_dete(self, dete : PodatakInterface):
        for index, _dete in enumerate(self.deca):
            if _dete == dete:
                _dete.postavi_roditelja(None)
                self.deca.pop(index)
                return _dete
        return None

    def dobavi_decu(self):
        return self.deca

    def postavi_decu(self, deca : list):
        for dete in deca:
            dete : PodatakInterface
            dete.postavi_roditelja(self)
        self.deca = deca

    # po hijerarhiji
    def dodaj_dete_po_hijerarhiji(self, dete_po_hijerarhiji : KompozitInterface):
        dete_po_hijerarhiji.dodaj_roditelja_po_hijerarhiji(self)
        self.deca_po_hijerarhiji.append(dete_po_hijerarhiji)

    def obrisi_dete_po_hijerarhiji(self, dete_po_hijerarhiji : KompozitInterface):
        for index, _dete in enumerate(self.deca_po_hijerarhiji):
            if _dete == dete_po_hijerarhiji:
                _dete.obrisi_roditelja_po_hijerarhiji(self)
                self.deca_po_hijerarhiji.pop(index)
                return _dete
        return None

    def dobavi_decu_po_hijerarhiji(self):
        return self.deca_po_hijerarhiji

    def postavi_decu_po_hijerarhiji(self, deca_po_hijerarhiji : list):
        for dete_po_hijerarhiji in deca_po_hijerarhiji:
            dete_po_hijerarhiji : KompozitInterface
            dete_po_hijerarhiji.dodaj_roditelja_po_hijerarhiji(self)
        self.deca_po_hijerarhiji = deca_po_hijerarhiji

    def dodaj_roditelja_po_hijerarhiji(self, roditelj_po_hijerarhiji : KompozitInterface):
        self.roditelji_po_hijerarhiji.append(roditelj_po_hijerarhiji)

    def obrisi_roditelja_po_hijerarhiji(self, roditelj_po_hijerarhiji : KompozitInterface):
        for index, _roditelj in enumerate(self.roditelji_po_hijerarhiji):
            if _roditelj == roditelj_po_hijerarhiji:
                self.roditelji_po_hijerarhiji.pop(index)
                return _roditelj
        return None

    def dobavi_roditelje_po_hijerarhiji(self):
        return self.roditelji_po_hijerarhiji

    def postavi_roditelje_po_hijerarhiji(self, roditelji_po_hijerarhiji : list):
        self.roditelji_po_hijerarhiji = roditelji_po_hijerarhiji

    # po relaciji
    def dodaj_dete_po_relaciji(self, dete_po_relaciji : KompozitInterface):
        dete_po_relaciji.dodaj_roditelja_po_relaciji(self)
        self.deca_po_relaciji.append(dete_po_relaciji)

    def obrisi_dete_po_relaciji(self, dete_po_relaciji : KompozitInterface):
        for index, _dete in enumerate(self.deca_po_relaciji):
            if _dete == dete_po_relaciji:
                _dete.obrisi_roditelja_po_relaciji(self)
                self.deca_po_relaciji.pop(index)
                return _dete
        return None

    def dobavi_decu_po_relaciji(self):
        return self.deca_po_relaciji

    def postavi_decu_po_relaciji(self, deca_po_relaciji : list):
        for dete_po_relaciji in deca_po_relaciji:
            dete_po_relaciji : KompozitInterface
            dete_po_relaciji.dodaj_roditelja_po_relaciji(self)
        self.deca_po_relaciji = deca_po_relaciji

    def dodaj_roditelja_po_relaciji(self, roditelj_po_relaciji : KompozitInterface):
        self.roditelji_po_relaciji.append(roditelj_po_relaciji)

    def obrisi_roditelja_po_relaciji(self, roditelj_po_relaciji : KompozitInterface):
        for index, _roditelj in enumerate(self.roditelji_po_relaciji):
            if _roditelj == roditelj_po_relaciji:
                self.roditelji_po_relaciji.pop(index)
                return _roditelj
        return None

    def dobavi_roditelje_po_relaciji(self):
        return self.roditelji_po_relaciji

    def postavi_roditelje_po_relaciji(self, roditelji_po_relaciji : list):
        self.roditelji_po_relaciji = roditelji_po_relaciji

    # PodatakInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        self.atribut = atribut

    def dobavi_reprezentacioni_atribut(self):
        return self.reprezentacioni_atribut

    def postavi_reprezentacioni_atribut(self, reprezentacioni_atribut):
        self.reprezentacioni_atribut = reprezentacioni_atribut

    def dobavi_tip(self):
        return self.tip

    def postavi_tip(self, tip):
        if tip in [PodatakTip.STRING, PodatakTip.NUMBER, PodatakTip.DATE, PodatakTip.BOOLEAN, PodatakTip.BLOB, PodatakTip.TEXT]:
            raise ValueError('tip podatka mora biti KompozitniPodatakTip')
        self.tip = tip

    def prikazi_sebe(self):
        print(self)
        for dete in self.deca:
            dete : PodatakInterface
            dete.prikazi_podatke()

    # CvorInterface
    def dobavi_roditelja(self):
        return self.roditelj

    def postavi_roditelja(self, roditelj):
        self.roditelj = roditelj

    def dobavi_dubinu(self):
        if self.roditelj is None:
            return 0
        else:
            return self.roditelj.dobavi_dubinu() + 1

    def dobavi_redni_broj(self, tip=None):
        if self.roditelj is None:
            # # stavljeno zbog PodatakWidgetInterface -> odkaci_se_od_stabla
            # return 0
            return None
        else:
            if tip is None:
                for index, dete in enumerate(self.roditelj.dobavi_decu()):
                    if dete == self:
                        return index
            else:
                if tip in [PodatakTip.DATABASE, PodatakTip.BATCH, PodatakTip.ENTITET]:
                    filtrirana_deca = list(filter(lambda dete: dete.dobavi_tip() == tip, self.roditelj.dobavi_decu()))
                    for index, dete in enumerate(filtrirana_deca):
                        if dete == self:
                            return index
                else:
                    raise ValueError('vrsi se dobavi_broj_deteta po tipu(PodatakTip) koji ne odgovara klasi KompozitniPodatak')
            raise IndexError('Dete \'ima\' roditelja, ali roditelj ne zna za njega')

    # DeoStablaInterface
    def zakaci_se_za_stablo(self, parent_widget, tipovi=None):
        # vec postoji
        for w_data in self.widgets:
            widget = w_data['widget']
            widget : QtWidgets.QWidget
            if widget.parent == parent_widget:
                return

        parent_widget : QtWidgets.QTreeWidgetItem
        if tipovi is None or (tipovi is not None and self.dobavi_tip() in tipovi):
            tree_widget_item = QtWidgets.QTreeWidgetItem(parent_widget, [self.reprezentacioni_atribut])
            tree_widget_item.parent = parent_widget

            tree_widget_item.podatak = self
            # self.widgets.append(tree_widget_item)
            self.widgets.append({'widget': tree_widget_item, 'tipovi': tipovi})

            for dete in self.deca:
                dete : PodatakWidgetInterface
                dete.zakaci_se_za_stablo(tree_widget_item, tipovi)
            return
            
        for dete in self.deca:
            dete : PodatakWidgetInterface
            dete.zakaci_se_za_stablo(parent_widget, tipovi)

    def odkaci_se_od_stabla(self, parent_widget, tipovi=None):
        parent_widget : QtWidgets.QTreeWidgetItem
        if tipovi is None or (tipovi is not None and self.dobavi_tip() in tipovi):
            for index, w_data in enumerate(self.widgets):
                widget = w_data['widget']
                widget : QtWidgets.QWidget
                if widget.parent == parent_widget:

                    for dete in self.deca:
                        dete : PodatakWidgetInterface
                        dete.odkaci_se_od_stabla(widget, tipovi)

                    parent_widget.removeChild(widget)
                    self.widgets.pop(index)
                    return
            
        for dete in self.deca:
            dete : PodatakWidgetInterface
            dete.odkaci_se_od_stabla(parent_widget, tipovi)

        