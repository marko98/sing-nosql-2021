from ..interface.rukovalac_skladistem_interface import RukovalacSkladistemInterface

from .parser_fns import kreiraj_bazu, zapis_u_entitet
from ...podatak.model.lisni_podatak import LisniPodatak
from ...podatak.model.podatak_tip import PodatakTip

import mysql.connector

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from business_logic.podatak.interface.kompozit_interface import KompozitInterface
    from ...podatak.interface.podatak_interface import PodatakInterface
    
class RukovalacRelacionimSkladistem(RukovalacSkladistemInterface):
    instanca = None

    def __init__(self, metapodatak_baze):
        super(RukovalacRelacionimSkladistem, self).__init__()

        self.metapodatak_baze = metapodatak_baze
        self.baza = kreiraj_bazu(self.metapodatak_baze)

        self.mysql_konekcija = self._dobavi_konekciju()
        self.kursor = self.mysql_konekcija.cursor()
        self.kursor : mysql.connector.cursor.MySQLCursor

        self.batchovi = []
        self.sirocici = []

        self._postavi_batchove()
        self._proveri_relacije()

    def _dobavi_konekciju(self):
        if self.baza.dobavi_tip() == PodatakTip.DATABASE:
            host, korisnicko_ime, lozinka = None, None, None

            for dete in self.baza.dobavi_decu():
                dete : PodatakInterface
                if dete.dobavi_atribut() == 'HOST':
                    dete : Podatak
                    host = dete.dobavi_vrednost()
                elif dete.dobavi_atribut() == 'KORISNICKO_IME':
                    dete : Podatak
                    korisnicko_ime = dete.dobavi_vrednost()
                elif dete.dobavi_atribut() == 'LOZINKA':
                    dete : Podatak
                    lozinka = dete.dobavi_vrednost()
                
                if host is not None and korisnicko_ime is not None and lozinka is not None:
                    return mysql.connector.connect(
                            host=host,
                            user=korisnicko_ime,
                            password=lozinka
                        )
            raise RuntimeError('Baza ne sadrzi potrebne podtake za konekciju na server baze podataka')
        else:
            raise RuntimeError('Baza je neispravna')

    def _postavi_batchove(self):
        for dete in self.baza.dobavi_decu():
            dete : PodatakInterface
            if dete.dobavi_tip() == PodatakTip.BATCH:

                self.batchovi.append({'batch': dete, 'filter': [], "deca": {}})

    def _proveri_relacije(self):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            batch : KompozitInterface
            if batch.dobavi_tip() == PodatakTip.BATCH:
                for entitet in batch.dobavi_decu():
                    entitet : KompozitInterface
                    if entitet.dobavi_tip() == PodatakTip.ENTITET:
                        for dete in entitet.dobavi_decu():
                            if isinstance(dete.dobavi_tip(), PodatakTip):
                                self._pokusaj_da_postavis_referencu(dete)

    def _pokusaj_da_postavis_referencu(self, dete):
        dete : LisniPodatak
        referencira = dete.dobavi_referencira()
        if referencira is not None and referencira['vrednost'] is None and dete.dobavi_vrednost() is not None:
            # referencira i unutar reference vrednost je None, ali podatak ima vrednost - tako da ga mozda mozemo spojiti sa onim kojeg referencira
            atribut = referencira['atribut']
            parts = atribut.split('.')
            if len(parts) != 3:
                raise ValueError('postoji potreba za referenciranjem gde nemamo punu putanju do reference')

            referencirani_batch_atribut = parts[0]
            # referencirani_entitet_atribut - mozda je nepotreban
            referencirani_entitet_atribut = parts[1]
            referencirani_podatak_atribut = parts[2]

            # provera batchova
            for batch_data in self.batchovi:
                referencirani_batch = batch_data['batch']
                referencirani_batch : KompozitInterface
                if referencirani_batch.dobavi_tip() == PodatakTip.BATCH and referencirani_batch.dobavi_atribut() == referencirani_batch_atribut:
                    for referencirani_entitet in referencirani_batch.dobavi_decu():
                        referencirani_entitet : KompozitInterface
                        if referencirani_entitet.dobavi_tip() == PodatakTip.ENTITET and referencirani_entitet.dobavi_atribut() == referencirani_entitet_atribut:
                            for referencirano_dete in referencirani_entitet.dobavi_decu():
                                if isinstance(referencirano_dete.dobavi_tip(), PodatakTip) and referencirano_dete.dobavi_atribut() == referencirani_podatak_atribut:
                                    referencirano_dete : LisniPodatak
                                    vrednost = referencirano_dete.dobavi_vrednost()
                                    if vrednost is not None and vrednost == dete.dobavi_vrednost():
                                        dete.dobavi_referencira()['vrednost'] = referencirano_dete
                                        referencirano_dete.dodaj_referenciran_od(dete)
                                        return
            
            if dete.dobavi_referencira()['vrednost'] is None:
                # provera sirocica
                for siroce_data in self.sirocici:
                    if siroce_data['batch_ime'] == referencirani_batch_atribut:
                        referencirani_entitet = siroce_data['siroce']
                        if referencirani_entitet.dobavi_tip() == PodatakTip.ENTITET and referencirani_entitet.dobavi_atribut() == referencirani_entitet_atribut:
                            for referencirano_dete in referencirani_entitet.dobavi_decu():
                                if isinstance(referencirano_dete.dobavi_tip(), PodatakTip) and referencirano_dete.dobavi_atribut() == referencirani_podatak_atribut:
                                    referencirano_dete : LisniPodatak
                                    vrednost = referencirano_dete.dobavi_vrednost()
                                    if vrednost is not None and vrednost == dete.dobavi_vrednost():
                                        dete.dobavi_referencira()['vrednost'] = referencirano_dete
                                        referencirano_dete.dodaj_referenciran_od(dete)
                                        return

    @staticmethod
    def dobavi_instancu(metapodatak_baze=None):
        if RukovalacRelacionimSkladistem.instanca is not None:
            return RukovalacRelacionimSkladistem.instanca
        else:
            if metapodatak_baze is None:
                raise ValueError('pokusano instanciranje klase RukovalacRelacionimSkladistem bez metapodataka o relacionoj bazi')
            RukovalacRelacionimSkladistem.instanca = RukovalacRelacionimSkladistem(metapodatak_baze)
            return RukovalacRelacionimSkladistem.instanca

    def _proveri_sirocice_na_osnovu_zapisa(self, zapis, batch):
        for meta_batch in self.metapodatak_baze['tabele']:
            if meta_batch['naziv'] == batch.dobavi_atribut():

                if len(zapis) != len(meta_batch['atributi']):
                    raise RuntimeError('dobijen zapis sa servera nema isti broj vrednosti svojstava kao sta pise u metapodacima')

                atributi_pk_data = []
                for index, atr in enumerate(meta_batch['atributi']):
                    if atr['naziv'] in meta_batch['primarni_kljucevi']:
                        atributi_pk_data.append({'naziv': atr['naziv'], 'index': index})
                
                for index, siroce_data in enumerate(self.sirocici):
                    if siroce_data['batch_ime'] == batch.dobavi_atribut():
                        pronadjeno_siroce = True

                        siroce = siroce_data['siroce']
                        siroce : KompozitniPodatakInterface
                        if siroce.dobavi_tip() == KompozitniPodatakTip.ENTITET:

                            for dete in siroce.dobavi_decu():
                                dete : Podatak
                                for atr_pk_data in atributi_pk_data:
                                    if dete.dobavi_atribut() == atr_pk_data['naziv']:
                                        if dete.dobavi_vrednost() != zapis[atr_pk_data['index']]:
                                            pronadjeno_siroce = False
                                            break
                                
                                if not pronadjeno_siroce:
                                    break
                            
                            if pronadjeno_siroce:
                                self.sirocici.pop(index) # izbacujemo ga iz liste buduci da vise nije siroce
                                return siroce
                        else:     
                            raise ValueError('postoji siroce koje nije entitet')
        return None

    # RukovalacSkladistemInterface
    # CRUD -----------------------------
    def dobavi(self, kompozitni_podatak, start_index=0, end_index=0):
        kompozitni_podatak : KompozitInterface
        if kompozitni_podatak.dobavi_tip() == PodatakTip.BATCH:

            batch_deca = None
            for batch_data in self.batchovi:
                batch = batch_data['batch']
                batch : PodatakInterface
                kompozitni_podatak : PodatakInterface
                if batch.dobavi_atribut() == kompozitni_podatak.dobavi_atribut():
                    batch_deca = batch_data['deca']
                    break
            if batch_deca is None:
                raise RuntimeError('pokusavamo da dohvatimo entitete za batch koji ne postoji u skladistu')

            sf = []
            s_i = None
            for i in range(start_index, end_index + 1):
                if i in batch_deca:
                    if s_i is not None:
                        sf.append([s_i, i-1])
                        s_i = None
                else:
                    if s_i is None:
                        s_i = i
            if s_i is not None:
                sf.append([s_i, end_index])
                s_i = None

            self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))
            # sql = 'SELECT * FROM {} LIMIT {}, {}'.format(kompozitni_podatak.dobavi_atribut(), len(kompozitni_podatak.dobavi_decu()), self.limit,)
            # self.kursor.execute(sql)
            for _sf in sf:
                _start_index = _sf[0]
                _end_index = _sf[1]

                for tabela in self.metapodatak_baze['tabele']:
                    if tabela['naziv'] == kompozitni_podatak.dobavi_atribut():
                        ime_procedure = tabela['operacije']['dobavi']['naziv']
                        args = tabela['operacije']['dobavi']['args']
                        # limit
                        args[-2] = _end_index - _start_index + 1
                        # offset
                        args[-1] = _start_index

                        self.kursor.callproc(ime_procedure, args)
                        print('procedura ' + ime_procedure + ' pozvana')
                        for result in self.kursor.stored_results():

                            brojac = _start_index
                            zapisi = result.fetchall()
                            for zapis in zapisi:
                                # proveravamo da li je mozda vec dohvacen entitet ali je u sirocicima
                                entitet = self._proveri_sirocice_na_osnovu_zapisa(zapis, kompozitni_podatak)
                                if entitet is None:
                                    entitet = zapis_u_entitet(kompozitni_podatak.dobavi_atribut(), zapis, self.metapodatak_baze)
                                kompozitni_podatak.dodaj_dete(entitet)                
                                
                                # # za testiranje _proveri_sirocice_na_osnovu_zapisa
                                # entitet = zapis_u_entitet(kompozitni_podatak.dobavi_atribut(), zapis, self.metapodatak_baze)
                                # self.sirocici.append({'batch_ime': kompozitni_podatak.dobavi_atribut(), 'siroce': entitet})
                                # entitet = self._proveri_sirocice_na_osnovu_zapisa(zapis, kompozitni_podatak)

                                for dete in entitet.dobavi_decu():
                                    self._pokusaj_da_postavis_referencu(dete)

                                batch_deca[brojac] = entitet
                                brojac += 1

            entiteti = []
            for i in range(start_index, end_index + 1):
                if i in batch_deca:
                    entiteti.append(batch_deca[i])
            return entiteti
        else:
            raise RuntimeError('pokusano dobavljanje entiteta za kompozitni podatak koji nije tipa BATCH')

    def kreiraj(self):
        raise NotImplementedError('metoda kreiraj nije implementirana')

    def izmeni(self):
        raise NotImplementedError('metoda izmeni nije implementirana')

    def obrisi(self):
        raise NotImplementedError('metoda obrisi nije implementirana')

    # -----------------------------

    def dobavi_kompozitni_podatak_DATABASE(self):
        return self.baza

    def dobavi_metapodatak_baze(self):
        return self.metapodatak_baze

    def dobavi_batch(self, batch_ime):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            if batch.dobavi_atribut() == batch_ime:
                return batch
        raise RuntimeError('zatrazeni batch po atributu ne postoji u bazi')

    def dobavi_batch_po_reprezentacionom_atributu(self, reprezentacioni_atribut):
        for batch_data in self.batchovi:
            batch = batch_data['batch']
            if batch.dobavi_reprezentacioni_atribut() == reprezentacioni_atribut:
                return batch
        raise RuntimeError('zatrazeni batch po reprezentacionom atributu ne postoji u bazi')

    def broj_entiteta_batcha(self, kompozitni_podatak):
        kompozitni_podatak : PodatakInterface
        if kompozitni_podatak.dobavi_tip() == PodatakTip.BATCH:
            # prebaci logiku na server, napisi proceduru
            self.kursor.execute('USE {}'.format(self.baza.dobavi_atribut(),))
            self.kursor.execute('SELECT COUNT(*) FROM {};'.format(kompozitni_podatak.dobavi_atribut(),))
            broj_entiteta = self.kursor.fetchone()[0]
            return broj_entiteta
        return 0