from ..interface.komponenta_interface import KomponentaInterface

class Komponenta(KomponentaInterface):

    def __init__(self, metapodaci):
        super(Komponenta, self).__init__()
        
        self.metapodaci = metapodaci

    @property
    def ime(self):
        return self.metapodaci.get("ime", "")

    @property
    def opis(self):
        return self.metapodaci.get("opis", "")
    
    @property
    def simbolicko_ime(self):
        return self.metapodaci.get("simbolicko_ime", "")

    @property
    def verzija_jezgra(self):
        return self.metapodaci.get("verzija_jezgra", "")

    @property
    def verzija(self):
        return self.metapodaci.get("verzija", "")

    @property
    def kategorija(self):
        return self.metapodaci.get("kategorija", "")

    @property
    def odobrena(self):
        return self.metapodaci.get("odobrena", True)

    @odobrena.setter
    def odobrena(self, vrednost):
        if isinstance(vrednost, bool):
            self.metapodaci['odobrena'] = vrednost

    def __str__(self):
        return "ime: {}, verzija: {}.".format(self.ime, self.verzija)