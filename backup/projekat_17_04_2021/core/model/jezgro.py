from PyQt5 import QtWidgets
import json

from ..interface.mediator_interface import MediatorInterface
from .registar_komponenti import RegistarKomponenti

from business_logic.pattern.behavioural.observer.model.observable import Observable
from business_logic.rukovalac_skladistem.model.rukovalac_relacionim_skladistem import RukovalacRelacionimSkladistem

class Jezgro(Observable, MediatorInterface):
    instanca = None

    def __init__(self):
        super(Jezgro, self).__init__()
        # print(__name__)

        self.registar_komponenti = RegistarKomponenti.dobavi_instancu()

        with open ('./meta/razlika_izmedju_veza_hijerarhije_i_relacije.json', 'r') as fp:
        # with open ('./meta/informacioni_resurs_generisano.json', 'r') as fp:    
            self.metapodatak_baze = json.load(fp)

            self.rukovalac_skladistem = RukovalacRelacionimSkladistem.dobavi_instancu(self.metapodatak_baze)

        self.registrovani_korisnik = None

    # def postavi_rukovaoca_skladistem(self):
    #     self.rukovalac_skladistem = RukovalacRelacionimSkladistem.dobavi_instancu()
    #     self.obavseti_observere()

    @staticmethod
    def dobavi_instancu():
        if Jezgro.instanca is None:
            Jezgro.instanca = Jezgro()
        return Jezgro.instanca

    def obavesti(self, simbolicko_ime):
        ui = self.registar_komponenti.dobavi_komponentu(simbolicko_ime).dobavi_qwidget()
        # print(self.registar_komponenti.dobavi_komponentu(simbolicko_ime).__mro__)

        # ui.show()
        return ui