from ..interface.vrednost_interface import VrednostInterface

class KompozitnaVrednost(VrednostInterface):

    def __init__(self, deca = []):
        super(KompozitnaVrednost, self).__init__()

        self.deca = deca

    def dodaj_dete(self, dete : VrednostInterface):
        self.deca.append(dete)

    def obrisi_dete(self, dete : VrednostInterface):
        for index, _dete in enumerate(self.deca):
            if _dete == dete:
                self.deca.pop(index)
                return dete
        return None

    def dobavi_decu(self):
        return self.deca

    # VrednostInterface
    def dobavi_vrednost(self):
        vrednost = {}
        for dete in self.deca:
            vrednost.update(dete.dobavi_vrednost())

        return vrednost