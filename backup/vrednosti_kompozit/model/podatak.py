from ..interface.vrednost_interface import VrednostInterface

class Podatak(VrednostInterface):

    def __init__(self, atribut, vrednost : VrednostInterface):
        super(Podatak, self).__init__()

        self.atribut = atribut
        self.vrednost = vrednost

    def dobavi_vrednost(self):
        return self.vrednost

    def postavi_vrednost(self, vrednost):
        self.vrednost = vrednost

    # VrednostInterface
    def dobavi_atribut(self):
        return self.atribut

    def postavi_atribut(self, atribut):
        if isinstance(atribut, str):
            self.atribut = atribut
        else:
            raise ValueError('atribut moze biti samo tipa string')

    def dobavi_atribut_vrednost(self):
        return {self.atribut: self.vrednost.dobavi_vrednost()}