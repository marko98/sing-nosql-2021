from ..interface.vrednost_interface import VrednostInterface

class Vrednost(VrednostInterface):

    def __init__(self, vrednost):
        super(Vrednost, self).__init__()

        self.vrednost = vrednost

    # VrednostInterface
    def dobavi_vrednost(self):
        return self.vrednost