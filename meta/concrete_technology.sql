/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     4/4/2021 10:31:26 AM                         */
/*==============================================================*/

create schema IF NOT EXISTS `mydb_project` DEFAULT CHARACTER SET utf8 ;
USE `mydb_project` ;

drop table if exists ELECTIVE_COURSEC;

drop table if exists COURSE;

drop table if exists HIGH_EDUCATION_INSTITUTION;

drop table if exists COURSES_IN_BLOCK;

drop table if exists EDUCATION_PLAN;

drop table if exists FOUND_FOR_TYPE;

drop table if exists COVERES_COMPETENCES;

drop table if exists INTERNAL_STRUCTURE;

drop table if exists DOCUMENT_PART;

drop table if exists LECTURING_TYPE;

drop table if exists CURRICULUM_STRUCTURE;

drop table if exists TYPE_OF_BLOCK;

drop table if exists COVERED_COMPETENCIES;

drop table if exists DOCUMENT_INSTANCE;

drop table if exists REGISTERED_CURRICULUMS;

drop table if exists LANGUAGES;

drop table if exists PROFESSIONS;

drop table if exists CURRICULUM_TYPE;

drop table if exists EDUCATION_FIELDS;

drop table if exists COVERED_EDUCATION_SUBGROUPS;

drop table if exists COMPETENCE;

drop table if exists STRUCTURED_COMPETENCE;

drop table if exists STRUCTURED_CATEGORIES;

drop table if exists COMPETENCE_CATEGORY;

drop table if exists EDUCATION_SUBGROUPS;

drop table if exists EDUCATION_GROUPS;

drop table if exists CURRICULUM_LEVEL;

drop table if exists CURRICULUM_DEGREE;

/*==============================================================*/
/* Table: COMPETENCE                                            */
/*==============================================================*/
create table COMPETENCE
(
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   KOMP_KATEGORIJA      char(2) not null,
   KO_KOMPETENCIJA      char(2) not null,
   KO_NAZIV             varchar(120) not null,
   KO_OPIS              varchar(512),
   primary key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA)
);

/*==============================================================*/
/* Table: COMPETENCE_CATEGORY                                   */
/*==============================================================*/
create table COMPETENCE_CATEGORY
(
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   KOMP_KATEGORIJA      char(2) not null,
   KOMP_NAZIV           varchar(120) not null,
   KOMP_OPIS            varchar(512),
   primary key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA)
);

/*==============================================================*/
/* Table: COURSE                                                */
/*==============================================================*/
create table COURSE
(
   VU_IDENTIFIKATOR     int not null,
   NP_PREDMET           varchar(6) not null,
   NP_VERZIJA           numeric(2,0) not null,
   NP_NAZIV_PREDMETA    varchar(120) not null,
   NP_IZBORNA           bool not null default 0,
   primary key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA)
);

/*==============================================================*/
/* Table: COURSES_IN_BLOCK                                      */
/*==============================================================*/
create table COURSES_IN_BLOCK
(
   CUR_VU_IDENTIFIKATOR int not null,
   TIPP_TIP             char(1) not null,
   SP_EVIDENCIONI_BROJ  int not null,
   SP_VERZIJA           numeric(2,0) not null,
   ON_OZNAKA            char(1) not null,
   BLOKN_REDNI_BROJ     numeric(2,0) not null,
   NP_PREDMET           varchar(6) not null,
   NP_VERZIJA           numeric(2,0) not null,
   primary key (CUR_VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA, ON_OZNAKA, BLOKN_REDNI_BROJ, NP_PREDMET, NP_VERZIJA)
);

/*==============================================================*/
/* Table: COVERED_COMPETENCIES                                  */
/*==============================================================*/
create table COVERED_COMPETENCIES
(
   VU_IDENTIFIKATOR     int not null,
   TIPP_TIP             char(1) not null,
   SP_EVIDENCIONI_BROJ  int not null,
   SP_VERZIJA           numeric(2,0) not null,
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   KOMP_KATEGORIJA      char(2) not null,
   KO_KOMPETENCIJA      char(2) not null,
   primary key (VU_IDENTIFIKATOR, PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA, KO_KOMPETENCIJA)
);

/*==============================================================*/
/* Table: COVERED_EDUCATION_SUBGROUPS                           */
/*==============================================================*/
create table COVERED_EDUCATION_SUBGROUPS
(
   VU_IDENTIFIKATOR     int not null,
   TIPP_TIP             char(1) not null,
   SP_EVIDENCIONI_BROJ  int not null,
   SP_VERZIJA           numeric(2,0) not null,
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   primary key (VU_IDENTIFIKATOR, PO_POLJE, GRU_GRUPA, OBL_OBLAST, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA)
);

/*==============================================================*/
/* Table: COVERES_COMPETENCES                                   */
/*==============================================================*/
create table COVERES_COMPETENCES
(
   VU_IDENTIFIKATOR     int not null,
   NP_PREDMET           varchar(6) not null,
   NP_VERZIJA           numeric(2,0) not null,
   VID_VID              char(1) not null,
   EPL_WEEK             numeric(2,0) not null,
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   KOMP_KATEGORIJA      char(2) not null,
   KO_KOMPETENCIJA      char(2) not null,
   primary key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, VU_IDENTIFIKATOR, KO_KOMPETENCIJA, NP_PREDMET, NP_VERZIJA, VID_VID, EPL_WEEK)
);

/*==============================================================*/
/* Table: CURRICULUM_DEGREE                                     */
/*==============================================================*/
create table CURRICULUM_DEGREE
(
   STS_OZNAKA           char(2) not null,
   STS_NAZIV            varchar(40) not null,
   primary key (STS_OZNAKA)
);

/*==============================================================*/
/* Table: CURRICULUM_LEVEL                                      */
/*==============================================================*/
create table CURRICULUM_LEVEL
(
   STS_OZNAKA           char(2) not null,
   NS_NIVO              numeric(1,0) not null,
   NA_NAZIV             varchar(60) not null,
   SN_OZNAKA            char(2),
   primary key (STS_OZNAKA, NS_NIVO)
);

/*==============================================================*/
/* Table: CURRICULUM_STRUCTURE                                  */
/*==============================================================*/
create table CURRICULUM_STRUCTURE
(
   VU_IDENTIFIKATOR     int not null,
   TIPP_TIP             char(1) not null,
   SP_EVIDENCIONI_BROJ  int not null,
   SP_VERZIJA           numeric(2,0) not null,
   ON_OZNAKA            char(1) not null,
   BLOKN_REDNI_BROJ     numeric(2,0) not null,
   BLOKN_TRAJE          numeric(2,0) not null default 1,
   primary key (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA, ON_OZNAKA, BLOKN_REDNI_BROJ)
);

/*==============================================================*/
/* Table: CURRICULUM_TYPE                                       */
/*==============================================================*/
create table CURRICULUM_TYPE
(
   TIPP_TIP             char(1) not null,
   TIPP_NAZIV           varchar(40) not null,
   primary key (TIPP_TIP)
);

/*==============================================================*/
/* Table: DOCUMENT_INSTANCE                                     */
/*==============================================================*/
create table DOCUMENT_INSTANCE
(
   VU_IDENTIFIKATOR     int not null,
   TIPP_TIP             char(1) not null,
   SP_EVIDENCIONI_BROJ  int not null,
   SP_VERZIJA           numeric(2,0) not null,
   DOC_ID               numeric(3,0) not null,
   DIN_SEGMENT_ID       numeric(2,0) not null,
   DIN_DESCRIPTION      varchar(7000),
   primary key (VU_IDENTIFIKATOR, TIPP_TIP, DOC_ID, SP_EVIDENCIONI_BROJ, SP_VERZIJA, DIN_SEGMENT_ID)
);

/*==============================================================*/
/* Table: DOCUMENT_PART                                         */
/*==============================================================*/
create table DOCUMENT_PART
(
   DOC_ID               numeric(3,0) not null,
   DOC_PARTH_NAME       varchar(240) not null,
   DOC_META_DESCRIPTION varchar(1024),
   primary key (DOC_ID)
);

/*==============================================================*/
/* Table: EDUCATION_FIELDS                                      */
/*==============================================================*/
create table EDUCATION_FIELDS
(
   PO_POLJE             char(2) not null,
   PO_NAZIV             varchar(120) not null,
   primary key (PO_POLJE)
);

/*==============================================================*/
/* Table: EDUCATION_GROUPS                                      */
/*==============================================================*/
create table EDUCATION_GROUPS
(
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   GRU_NAZIV            varchar(120) not null,
   primary key (PO_POLJE, GRU_GRUPA)
);

/*==============================================================*/
/* Table: EDUCATION_PLAN                                        */
/*==============================================================*/
create table EDUCATION_PLAN
(
   VU_IDENTIFIKATOR     int not null,
   NP_PREDMET           varchar(6) not null,
   NP_VERZIJA           numeric(2,0) not null,
   VID_VID              char(1) not null,
   EPL_WEEK             numeric(2,0) not null,
   EPL_FOUND            numeric(2,0) not null default 2,
   EPL_DESCRIPTION      varchar(512),
   primary key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA, VID_VID, EPL_WEEK)
);

/*==============================================================*/
/* Table: EDUCATION_SUBGROUPS                                   */
/*==============================================================*/
create table EDUCATION_SUBGROUPS
(
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   OBL_NAZIV            varchar(120) not null,
   OBL_KORPUS           bool not null default 0,
   primary key (PO_POLJE, GRU_GRUPA, OBL_OBLAST)
);

/*==============================================================*/
/* Table: ELECTIVE_COURSEC                                      */
/*==============================================================*/
create table ELECTIVE_COURSEC
(
   VU_IDENTIFIKATOR     int not null,
   NP_PREDMET           varchar(6) not null,
   NP_VERZIJA           numeric(2,0) not null,
   ELL_POSSITION        numeric(2,0) not null,
   COU_NP_PREDMET       varchar(6) not null,
   COU_NP_VERZIJA       numeric(2,0) not null,
   primary key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA, ELL_POSSITION)
);

/*==============================================================*/
/* Table: FOUND_FOR_TYPE                                        */
/*==============================================================*/
create table FOUND_FOR_TYPE
(
   VU_IDENTIFIKATOR     int not null,
   NP_PREDMET           varchar(6) not null,
   NP_VERZIJA           numeric(2,0) not null,
   VID_VID              char(1) not null,
   FOND_UKUPNO_CASOVA   decimal(5,2) not null default 30,
   FOND_NACIN_IZVO_ENJA char(1) not null default 'C',
   primary key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA, VID_VID)
);

/*==============================================================*/
/* Table: HIGH_EDUCATION_INSTITUTION                            */
/*==============================================================*/
create table HIGH_EDUCATION_INSTITUTION
(
   VU_IDENTIFIKATOR     int not null,
   VU_NAZIV             varchar(120),
   VU_ADRESA            varchar(60),
   VU_OSNOVANA          date,
   VU_GRB               longblob,
   VU_MEMORANDUM        longblob,
   VU_WEB_ADRESA        varchar(80),
   VU_E_MAIL            varchar(60),
   VU_PIB               char(10),
   VU_MATICNI_BROJ      char(11),
   primary key (VU_IDENTIFIKATOR)
);

/*==============================================================*/
/* Table: INTERNAL_STRUCTURE                                    */
/*==============================================================*/
create table INTERNAL_STRUCTURE
(
   DOC_DOC_ID           numeric(3,0) not null,
   DOC_ID               numeric(3,0) not null,
   primary key (DOC_ID, DOC_DOC_ID)
);

/*==============================================================*/
/* Table: LANGUAGES                                             */
/*==============================================================*/
create table LANGUAGES
(
   JEZ_JEZIK            char(3) not null,
   JEZ_NAZIV            varchar(40) not null,
   primary key (JEZ_JEZIK)
);

/*==============================================================*/
/* Table: LECTURING_TYPE                                        */
/*==============================================================*/
create table LECTURING_TYPE
(
   VID_VID              char(1) not null,
   VID_NAZIV            varchar(40) not null,
   primary key (VID_VID)
);

/*==============================================================*/
/* Table: PROFESSIONS                                           */
/*==============================================================*/
create table PROFESSIONS
(
   SN_OZNAKA            char(2) not null,
   SN_STRUCNI_NAZIV     varchar(120) not null,
   SN_SKRACENI_NAZIV    varchar(12) not null,
   primary key (SN_OZNAKA)
);

/*==============================================================*/
/* Table: REGISTERED_CURRICULUMS                                */
/*==============================================================*/
create table REGISTERED_CURRICULUMS
(
   VU_IDENTIFIKATOR     int not null,
   TIPP_TIP             char(1) not null,
   SP_EVIDENCIONI_BROJ  int not null,
   SP_VERZIJA           numeric(2,0) not null,
   SP_NAZIV             varchar(60) not null,
   SP_DATUM_FORMIRANJA  date not null,
   SP_DATUM_UKIDANJA    date,
   SP_ECTS              numeric(3,0) not null default 60,
   JEZ_JEZIK            char(3) not null,
   STS_OZNAKA           char(2) not null,
   NS_NIVO              numeric(1,0) not null,
   SN_OZNAKA            char(2) not null,
   primary key (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA)
);

/*==============================================================*/
/* Table: STRUCTURED_CATEGORIES                                 */
/*==============================================================*/
create table STRUCTURED_CATEGORIES
(
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   KOMP_KATEGORIJA      char(2) not null,
   KAT_PO_POLJE         char(2) not null,
   KAT_GRU_GRUPA        char(2) not null,
   KAT_OBL_OBLAST       char(2) not null,
   KAT_KOMP_KATEGORIJA  char(2) not null,
   COM_KOMP_KATEGORIJA  char(2),
   primary key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KAT_KOMP_KATEGORIJA)
);

/*==============================================================*/
/* Table: STRUCTURED_COMPETENCE                                 */
/*==============================================================*/
create table STRUCTURED_COMPETENCE
(
   PO_POLJE             char(2) not null,
   GRU_GRUPA            char(2) not null,
   OBL_OBLAST           char(2) not null,
   KOMP_KATEGORIJA      char(2) not null,
   KO_KOMPETENCIJA      char(2) not null,
   COM_KO_KOMPETENCIJA  char(2) not null,
   primary key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA, COM_KO_KOMPETENCIJA)
);

/*==============================================================*/
/* Table: TYPE_OF_BLOCK                                         */
/*==============================================================*/
create table TYPE_OF_BLOCK
(
   ON_OZNAKA            char(1) not null,
   ON_NAZIV             varchar(20) not null,
   primary key (ON_OZNAKA)
);

alter table COMPETENCE add constraint FK_COMPETEN_COMPETENC_COMPETE2 foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA)
      references COMPETENCE_CATEGORY (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA) on delete restrict on update restrict;

alter table COMPETENCE_CATEGORY add constraint FK_CATEGORISED_COMPETENCIES foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST)
      references EDUCATION_SUBGROUPS (PO_POLJE, GRU_GRUPA, OBL_OBLAST) on delete restrict on update restrict;

alter table COURSE add constraint FK_RUN_COURSES foreign key (VU_IDENTIFIKATOR)
      references HIGH_EDUCATION_INSTITUTION (VU_IDENTIFIKATOR) on delete restrict on update restrict;

alter table COURSES_IN_BLOCK add constraint FK_COURSES_IN_BLOCK foreign key (CUR_VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA, ON_OZNAKA, BLOKN_REDNI_BROJ)
      references CURRICULUM_STRUCTURE (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA, ON_OZNAKA, BLOKN_REDNI_BROJ) on delete restrict on update restrict;

alter table COURSES_IN_BLOCK add constraint FK_COURSES_IN_BLOCK2 foreign key (CUR_VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA)
      references COURSE (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA) on delete restrict on update restrict;

alter table COVERED_COMPETENCIES add constraint FK_COVERED_COMPETENCIES foreign key (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA)
      references REGISTERED_CURRICULUMS (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA) on delete restrict on update restrict;

alter table COVERED_COMPETENCIES add constraint FK_COVERED_COMPETENCIES2 foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA)
      references COMPETENCE (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA) on delete restrict on update restrict;

alter table COVERED_EDUCATION_SUBGROUPS add constraint FK_COVERED_EDUCATION_SUBGROUPS foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST)
      references EDUCATION_SUBGROUPS (PO_POLJE, GRU_GRUPA, OBL_OBLAST) on delete restrict on update restrict;

alter table COVERED_EDUCATION_SUBGROUPS add constraint FK_COVERED_EDUCATION_SUBGROUPS2 foreign key (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA)
      references REGISTERED_CURRICULUMS (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA) on delete restrict on update restrict;

alter table COVERES_COMPETENCES add constraint FK_COVERES_COMPETENCES foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA)
      references COMPETENCE (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA) on delete restrict on update restrict;

alter table COVERES_COMPETENCES add constraint FK_COVERES_COMPETENCES2 foreign key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA, VID_VID, EPL_WEEK)
      references EDUCATION_PLAN (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA, VID_VID, EPL_WEEK) on delete restrict on update restrict;

alter table CURRICULUM_LEVEL add constraint FK_LEVEL_BASED_PROFESSION foreign key (SN_OZNAKA)
      references PROFESSIONS (SN_OZNAKA) on delete restrict on update restrict;

alter table CURRICULUM_LEVEL add constraint FK_WITH_LEVELS foreign key (STS_OZNAKA)
      references CURRICULUM_DEGREE (STS_OZNAKA) on delete restrict on update restrict;

alter table CURRICULUM_STRUCTURE add constraint FK_CURRICULUM_BLOCKS foreign key (ON_OZNAKA)
      references TYPE_OF_BLOCK (ON_OZNAKA) on delete restrict on update restrict;

alter table CURRICULUM_STRUCTURE add constraint FK_CURRICULUM_STRUCTURE foreign key (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA)
      references REGISTERED_CURRICULUMS (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA) on delete restrict on update restrict;

alter table DOCUMENT_INSTANCE add constraint FK_CURRICULUM_DOCUMENTATION foreign key (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA)
      references REGISTERED_CURRICULUMS (VU_IDENTIFIKATOR, TIPP_TIP, SP_EVIDENCIONI_BROJ, SP_VERZIJA) on delete restrict on update restrict;

alter table DOCUMENT_INSTANCE add constraint FK_IN_CURRICULUM_INSTANCE foreign key (DOC_ID)
      references DOCUMENT_PART (DOC_ID) on delete restrict on update restrict;

alter table EDUCATION_GROUPS add constraint FK_CONTAINE_GROUPS foreign key (PO_POLJE)
      references EDUCATION_FIELDS (PO_POLJE) on delete restrict on update restrict;

alter table EDUCATION_PLAN add constraint FK_EDUCATION_SCHRDULE foreign key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA)
      references COURSE (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA) on delete restrict on update restrict;

alter table EDUCATION_PLAN add constraint FK_FOR_TYPE foreign key (VID_VID)
      references LECTURING_TYPE (VID_VID) on delete restrict on update restrict;

alter table EDUCATION_SUBGROUPS add constraint FK_HAVE_SUBGROUPS foreign key (PO_POLJE, GRU_GRUPA)
      references EDUCATION_GROUPS (PO_POLJE, GRU_GRUPA) on delete restrict on update restrict;

alter table ELECTIVE_COURSEC add constraint FK_ELECTIVE_COURSE foreign key (VU_IDENTIFIKATOR, COU_NP_PREDMET, COU_NP_VERZIJA)
      references COURSE (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA) on delete restrict on update restrict;

alter table ELECTIVE_COURSEC add constraint FK_ELECTIVE_POOL foreign key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA)
      references COURSE (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA) on delete restrict on update restrict;

alter table FOUND_FOR_TYPE add constraint FK_FUNDS foreign key (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA)
      references COURSE (VU_IDENTIFIKATOR, NP_PREDMET, NP_VERZIJA) on delete restrict on update restrict;

alter table FOUND_FOR_TYPE add constraint FK_FUND_FOR_TYPE foreign key (VID_VID)
      references LECTURING_TYPE (VID_VID) on delete restrict on update restrict;

alter table INTERNAL_STRUCTURE add constraint FK_EMBEDDED_PART foreign key (DOC_ID)
      references DOCUMENT_PART (DOC_ID) on delete restrict on update restrict;

alter table INTERNAL_STRUCTURE add constraint FK_STRUCTURED_PART foreign key (DOC_DOC_ID)
      references DOCUMENT_PART (DOC_ID) on delete restrict on update restrict;

alter table REGISTERED_CURRICULUMS add constraint FK_CURRICULUM_DEGREE_AND_LEVEL foreign key (STS_OZNAKA, NS_NIVO)
      references CURRICULUM_LEVEL (STS_OZNAKA, NS_NIVO) on delete restrict on update restrict;

alter table REGISTERED_CURRICULUMS add constraint FK_CURRICULUM_LANGUAGE foreign key (JEZ_JEZIK)
      references LANGUAGES (JEZ_JEZIK) on delete restrict on update restrict;

alter table REGISTERED_CURRICULUMS add constraint FK_CURRICULUM_PROFESSION foreign key (SN_OZNAKA)
      references PROFESSIONS (SN_OZNAKA) on delete restrict on update restrict;

alter table REGISTERED_CURRICULUMS add constraint FK_REGISTERED_CURRICULUMS foreign key (VU_IDENTIFIKATOR)
      references HIGH_EDUCATION_INSTITUTION (VU_IDENTIFIKATOR) on delete restrict on update restrict;

alter table REGISTERED_CURRICULUMS add constraint FK_TYPED_CURRICULUMS foreign key (TIPP_TIP)
      references CURRICULUM_TYPE (TIPP_TIP) on delete restrict on update restrict;

alter table STRUCTURED_CATEGORIES add constraint FK_EMBEDDED_CATEGORY foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KAT_KOMP_KATEGORIJA)
      references COMPETENCE_CATEGORY (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA) on delete restrict on update restrict;

alter table STRUCTURED_CATEGORIES add constraint FK_STRUCTUR_STRUCTURE_COMPETE2 foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA)
      references COMPETENCE_CATEGORY (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA) on delete restrict on update restrict;

alter table STRUCTURED_CATEGORIES add constraint FK_STRUCTURED_CATEGORIES2 foreign key (KAT_PO_POLJE, KAT_GRU_GRUPA, KAT_OBL_OBLAST, COM_KOMP_KATEGORIJA)
      references COMPETENCE_CATEGORY (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA) on delete restrict on update restrict;

alter table STRUCTURED_COMPETENCE add constraint FK_STRUCTUR_STRUCTURE_COMPETE3 foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA)
      references COMPETENCE (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA) on delete restrict on update restrict;

alter table STRUCTURED_COMPETENCE add constraint FK_STRUCTURED_COMPETENCE2 foreign key (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, COM_KO_KOMPETENCIJA)
      references COMPETENCE (PO_POLJE, GRU_GRUPA, OBL_OBLAST, KOMP_KATEGORIJA, KO_KOMPETENCIJA) on delete restrict on update restrict;
