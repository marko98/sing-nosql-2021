DROP VIEW IF EXISTS `MONGO_REGISTERED_CURRICULUMS`;

CREATE VIEW `MONGO_REGISTERED_CURRICULUMS` 
AS SELECT 
registered_curriculums.SP_NAZIV, registered_curriculums.SP_DATUM_FORMIRANJA, registered_curriculums.SP_DATUM_UKIDANJA, registered_curriculums.SP_ECTS,
languages.JEZ_NAZIV,
curriculum_type.TIPP_NAZIV,
curriculum_level.NS_NIVO, curriculum_level.NA_NAZIV, 
curriculum_degree.STS_NAZIV,
professions.SN_STRUCNI_NAZIV, professions.SN_SKRACENI_NAZIV,
document_instance.DIN_SEGMENT_ID, document_instance.DIN_DESCRIPTION,
document_part.DOC_PARTH_NAME, document_part.DOC_META_DESCRIPTION

FROM registered_curriculums 
INNER JOIN languages ON registered_curriculums.JEZ_JEZIK = languages.JEZ_JEZIK
INNER JOIN curriculum_type ON registered_curriculums.TIPP_TIP = curriculum_type.TIPP_TIP
INNER JOIN curriculum_level ON registered_curriculums.STS_OZNAKA = curriculum_level.STS_OZNAKA AND registered_curriculums.NS_NIVO = curriculum_level.NS_NIVO
INNER JOIN curriculum_degree ON curriculum_level.STS_OZNAKA = curriculum_degree.STS_OZNAKA
INNER JOIN professions ON curriculum_level.SN_OZNAKA = professions.SN_OZNAKA
INNER JOIN document_instance ON registered_curriculums.VU_IDENTIFIKATOR = document_instance.VU_IDENTIFIKATOR AND registered_curriculums.TIPP_TIP = document_instance.TIPP_TIP
							AND registered_curriculums.SP_EVIDENCIONI_BROJ = document_instance.SP_EVIDENCIONI_BROJ AND registered_curriculums.SP_VERZIJA = document_instance.SP_VERZIJA
INNER JOIN document_part ON document_instance.DOC_ID = document_part.DOC_ID;