import json
import copy
import mysql.connector
import pymongo
import decimal
from datetime import date
from datetime import datetime

# fns
def obradi_podatak_iz_mysql(podatak):
    if isinstance(podatak, decimal.Decimal):
        return float(podatak)
    elif isinstance(podatak, date):
        # podatak : date
        # return "{}-{}-{}".format(podatak.year, podatak.month, podatak.day)
        return datetime(podatak.year, podatak.month, podatak.day)
    return podatak

def obradi_deo_dokumenta(args, deo):

    meta_kljucevi = ["_tabela_naziv", "_tip", "_od", "_do"]
    for kljuc in deo:
        if isinstance(deo[kljuc], dict):
            if deo[kljuc]['_tip'] == "RECNIK":
                obradi_deo_dokumenta(args, deo[kljuc])
            else:
                # lista

                novi_doc = copy.deepcopy(deo[kljuc])
                obradi_deo_dokumenta(args, novi_doc)

                deo[kljuc] = [novi_doc]
                # print('hi')
        else:
            if kljuc not in meta_kljucevi:
                arg = args.pop(0)
                deo[kljuc] = arg
    # print('hi')
    return

pks = []
def dobavi_mysql_polja(tabela, roditeljska_tabela=None):
    naziv_tabele = tabela["_tabela_naziv"]
    tip = tabela['_tip'] if '_tip' in tabela else None
    od = tabela['_od'] if '_od' in tabela else None
    do = tabela['_do'] if '_do' in tabela else None

    meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
    fields = []
    for kljuc in tabela:
        podatak = tabela[kljuc]
        if isinstance(podatak, dict):
            _fields = dobavi_mysql_polja(podatak, tabela)
            fields += _fields
        else:
            if kljuc in ['_pks']:
                for pk in podatak:
                    if pk.split('=')[0] not in pks:
                        pks.append(pk.split('=')[0])
                        fields.append(pk.split('=')[0])
            if kljuc not in meta_kljucevi and podatak.split('=')[0] not in pks:
                fields.append(podatak.split('=')[0])
    return fields

metpodatak_dokumenta = ""
with open('meta/documents/registrovani_studijski_program.json', 'r') as fp:
    metpodatak_dokumenta = json.load(fp)

baza_cinjenica = metpodatak_dokumenta['baza_cinjenica']
baza_dokumenata = metpodatak_dokumenta['baza_dokumenata']
dobavi_procedura = metpodatak_dokumenta['operacije']['dobavi']
dokument = metpodatak_dokumenta['dokument']
nazivi_mysql_polja = dobavi_mysql_polja(dokument)
parametri = metpodatak_dokumenta['parametri']

mysql_client = mysql.connector.connect(
    host=baza_cinjenica['konekcija']['host'],
    user=baza_cinjenica['konekcija']['korisnicko_ime'],
    password=baza_cinjenica['konekcija']['lozinka'],
)
mycursor = mysql_client.cursor()
mycursor.execute('USE ' + baza_cinjenica['db_naziv'])

mongo_client = pymongo.MongoClient(host=baza_dokumenata['konekcija']['host'], port=baza_dokumenata['konekcija']['port'])
mongo_db = mongo_client[baza_dokumenata['db_naziv']]
kolekcija = mongo_db[metpodatak_dokumenta['kolekcija_naziv']]


# fetch from mysql
args = list(map(lambda recnik: recnik[list(recnik)[0]], dobavi_procedura['args']))

# menjanje argumenata pri pozivu procedure(poslednja dva su limit i offset)
# args[-2-2] = 1
mycursor.callproc(dobavi_procedura['naziv'], args)
zapisi = []
for _cursor in mycursor.stored_results():    
    for torka in _cursor.fetchall():
        zapis_recnik = {}
        for index, podatak in enumerate(list(map(obradi_podatak_iz_mysql, list(torka)))):
            zapis_recnik[nazivi_mysql_polja[index]] = podatak
        zapisi.append(zapis_recnik)

# zapisi u dokumente(json)
def does_doc_exist(doc, docs=None):
    if docs is None:
        return False, None

    for _doc_zapisi in docs:
        _doc = _doc_zapisi['doc']
        if list(doc['_meta']['_pks']) == list(_doc['_meta']['_pks']) and \
            list(map(lambda kljuc: doc['_meta']['_pks'][kljuc], list(doc['_meta']['_pks']))) == list(map(lambda kljuc: _doc['_meta']['_pks'][kljuc], list(_doc['_meta']['_pks']))):
            # exists
            return True, _doc_zapisi
    return False, None

def izgradi_doc(metaopis_dokumenta, zapisi, roditeljski_doc=None):
    docs = []
    # docs_i_zapisi = {}

    for zapis_recnik in zapisi:
        doc = {"_meta": {"_pks": {}}}
        for pk in metaopis_dokumenta['_pks']:
            doc['_meta']["_pks"][pk.split('=')[0]] = zapis_recnik[pk.split('=')[0]]

        exists, original_doc_zapisi = does_doc_exist(doc, docs)
        if not exists:
            docs.append({'doc':doc, 'zapisi': [zapis_recnik]})
        else:
            original_doc_zapisi['zapisi'].append(zapis_recnik)

    for doc_zapisi in docs:
        # dodaj ostala polja
        doc = doc_zapisi['doc']

        # if len(doc_zapisi['zapisi']) > 1:
        zapis_recnik = doc_zapisi['zapisi'][0]

        meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
        for kljuc in metaopis_dokumenta:
            podatak = metaopis_dokumenta[kljuc]
            if isinstance(podatak, dict):
                if podatak['_tip'] == "RECNIK":
                    doc_child = izgradi_doc(podatak, doc_zapisi['zapisi'], doc)[0]['doc']
                    doc[kljuc] = doc_child
                else:
                    # lista
                    doc[kljuc] = list(map(lambda doc_zapisi: doc_zapisi['doc'], izgradi_doc(podatak, doc_zapisi['zapisi'], doc)))
            else:
                if kljuc in ['_tabela_naziv']:
                    doc['_meta'][kljuc] = podatak

                    if roditeljski_doc is not None:
                        doc['_meta']['referencira'] = roditeljski_doc['_meta']['_tabela_naziv']
                elif kljuc not in meta_kljucevi:
                    doc[kljuc] = zapis_recnik[podatak.split("=")[0]]
    return docs

docs = list(map(lambda doc_zapisi: doc_zapisi['doc'], izgradi_doc(dokument, zapisi)))
# with open('meta/documents/registrovani_studijski_program_docs.json', 'w') as w:
#     json.dump(docs, w)

for doc in docs:
    kolekcija.insert_one(doc)

def izgradi_zapise(metaopis_dokumenta, doc, zapisi=None):
    if zapisi is None:
        zapisi = [{}]

    meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
    for kljuc in doc:
        if kljuc in metaopis_dokumenta:
            podatak = doc[kljuc]
            metapodatak = metaopis_dokumenta[kljuc]
            if isinstance(podatak, dict):
                zapisi = izgradi_zapise(metapodatak, podatak, zapisi)
            elif isinstance(podatak, list):
                # _zapisi = []
                # for _ in range(len(podatak)):
                #     for _zapis in zapisi:
                #         _zapisi.append(copy.deepcopy(_zapis))

                # zapisi = _zapisi
                _zapisi = []
                for _podatak in podatak:
                    _zapisi += izgradi_zapise(metapodatak, _podatak, copy.deepcopy(zapisi))
                zapisi = _zapisi
            else:
                if kljuc not in meta_kljucevi:
                    _kljuc = metapodatak.split('=')[1]
                    for _zapis in zapisi:
                        _zapis[_kljuc] = podatak
    
    return zapisi

for doc in docs:
    # iz doc u zapise
    zapisi = izgradi_zapise(dokument, doc)
    # print('hi')