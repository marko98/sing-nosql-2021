import json
import copy
import mysql.connector
from pyArango import graph, collection, connection
import decimal
from datetime import date
from datetime import datetime

# fns
def obradi_podatak_iz_mysql(podatak):
    if isinstance(podatak, decimal.Decimal):
        return float(podatak)
    elif isinstance(podatak, date):
        # podatak : date
        # return "{}-{}-{}".format(podatak.year, podatak.month, podatak.day)
        return datetime(podatak.year, podatak.month, podatak.day)
    return podatak

def obradi_deo_dokumenta(args, deo):

    meta_kljucevi = ["_tabela_naziv", "_tip", "_od", "_do"]
    for kljuc in deo:
        if isinstance(deo[kljuc], dict):
            if deo[kljuc]['_tip'] == "RECNIK":
                obradi_deo_dokumenta(args, deo[kljuc])
            else:
                # lista

                novi_doc = copy.deepcopy(deo[kljuc])
                obradi_deo_dokumenta(args, novi_doc)

                deo[kljuc] = [novi_doc]
                # print('hi')
        else:
            if kljuc not in meta_kljucevi:
                arg = args.pop(0)
                deo[kljuc] = arg
    # print('hi')
    return

pks = []
def dobavi_mysql_polja(tabela, roditeljska_tabela=None):
    naziv_tabele = tabela["_tabela_naziv"]
    tip = tabela['_tip'] if '_tip' in tabela else None
    od = tabela['_od'] if '_od' in tabela else None
    do = tabela['_do'] if '_do' in tabela else None

    meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
    fields = []
    for kljuc in tabela:
        podatak = tabela[kljuc]
        if isinstance(podatak, dict):
            _fields = dobavi_mysql_polja(podatak, tabela)
            fields += _fields
        else:
            if kljuc in ['_pks']:
                for pk in podatak:
                    if pk.split('=')[0] not in pks:
                        pks.append(pk.split('=')[0])
                        fields.append(pk.split('=')[0])
            if kljuc not in meta_kljucevi and podatak.split('=')[0] not in pks:
                fields.append(podatak.split('=')[0])
    return fields
    
metpodatak_dokumenta = ""
with open('meta/documents/registrovani_studijski_program.json', 'r') as fp:
    metpodatak_dokumenta = json.load(fp)

baza_cinjenica = metpodatak_dokumenta['baza_cinjenica']
baza_grafova = metpodatak_dokumenta['baza_grafova']
dobavi_procedura = metpodatak_dokumenta['operacije']['dobavi']
dokument = metpodatak_dokumenta['dokument']
nazivi_mysql_polja = dobavi_mysql_polja(dokument)
parametri = metpodatak_dokumenta['parametri']
graf_naziv = metpodatak_dokumenta['graf_naziv']

# mysql
mysql_client = mysql.connector.connect(
    host=baza_cinjenica['konekcija']['host'],
    user=baza_cinjenica['konekcija']['korisnicko_ime'],
    password=baza_cinjenica['konekcija']['lozinka'],
)
mycursor = mysql_client.cursor()
mycursor.execute('USE ' + baza_cinjenica['db_naziv'])

# arango
arangodb_client = connection.Connection(username=baza_grafova['konekcija']['korisnicko_ime'], password=baza_grafova['konekcija']['lozinka'])
# arangodb = arangodb_client.createDatabase(name=baza_grafova['db_naziv'])
arangodb = arangodb_client[baza_grafova['db_naziv']]
arangodb : connection.Database

# fetch from mysql
args = list(map(lambda recnik: recnik[list(recnik)[0]], dobavi_procedura['args']))

# menjanje argumenata pri pozivu procedure(poslednja dva su limit i offset)
# args[-2-2] = 1
mycursor.callproc(dobavi_procedura['naziv'], args)
zapisi = []
for _cursor in mycursor.stored_results():    
    for torka in _cursor.fetchall():
        zapis_recnik = {}
        for index, podatak in enumerate(list(map(obradi_podatak_iz_mysql, list(torka)))):
            zapis_recnik[nazivi_mysql_polja[index]] = podatak
        zapisi.append(zapis_recnik)

# zapisi u dokumente(json)
def does_doc_exist(doc, docs=None):
    if docs is None:
        return False, None

    for _doc_zapisi in docs:
        _doc = _doc_zapisi['doc']
        if list(doc['_meta']['_pks']) == list(_doc['_meta']['_pks']) and \
            list(map(lambda kljuc: doc['_meta']['_pks'][kljuc], list(doc['_meta']['_pks']))) == list(map(lambda kljuc: _doc['_meta']['_pks'][kljuc], list(_doc['_meta']['_pks']))):
            # exists
            return True, _doc_zapisi
    return False, None

def izgradi_doc(metaopis_dokumenta, zapisi, roditeljski_doc=None):
    docs = []
    # docs_i_zapisi = {}

    for zapis_recnik in zapisi:
        doc = {"_meta": {"_pks": {}}}
        for pk in metaopis_dokumenta['_pks']:
            doc['_meta']["_pks"][pk.split('=')[0]] = zapis_recnik[pk.split('=')[0]]

        exists, original_doc_zapisi = does_doc_exist(doc, docs)
        if not exists:
            docs.append({'doc':doc, 'zapisi': [zapis_recnik]})
        else:
            original_doc_zapisi['zapisi'].append(zapis_recnik)

    for doc_zapisi in docs:
        # dodaj ostala polja
        doc = doc_zapisi['doc']

        # if len(doc_zapisi['zapisi']) > 1:
        zapis_recnik = doc_zapisi['zapisi'][0]

        meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
        for kljuc in metaopis_dokumenta:
            podatak = metaopis_dokumenta[kljuc]
            if isinstance(podatak, dict):
                if podatak['_tip'] == "RECNIK":
                    doc_child = izgradi_doc(podatak, doc_zapisi['zapisi'], doc)[0]['doc']
                    doc[kljuc] = doc_child
                else:
                    # lista
                    doc[kljuc] = list(map(lambda doc_zapisi: doc_zapisi['doc'], izgradi_doc(podatak, doc_zapisi['zapisi'], doc)))
            else:
                if kljuc in ['_tabela_naziv']:
                    doc['_meta'][kljuc] = podatak

                    if roditeljski_doc is not None:
                        doc['_meta']['referencira'] = roditeljski_doc['_meta']['_tabela_naziv']
                elif kljuc not in meta_kljucevi:
                    doc[kljuc] = zapis_recnik[podatak.split("=")[0]]
    return docs

docs = list(map(lambda doc_zapisi: doc_zapisi['doc'], izgradi_doc(dokument, zapisi)))

def nazivi_kolekcije_dokumenata_za_kreirati(doc, nazivi_kolekcije_dokumenata=None):
    if nazivi_kolekcije_dokumenata is None:
        nazivi_kolekcije_dokumenata = []

    if doc['_meta']['_tabela_naziv'] not in nazivi_kolekcije_dokumenata:
        nazivi_kolekcije_dokumenata.append(doc['_meta']['_tabela_naziv'])

    for kljuc in doc:
        podatak = doc[kljuc]
        if isinstance(podatak, dict) and kljuc not in ['_meta']:
            nazivi_kolekcije_dokumenata_za_kreirati(podatak, nazivi_kolekcije_dokumenata)
        elif isinstance(podatak, list):
            # lista
            for _doc in podatak:
                nazivi_kolekcije_dokumenata_za_kreirati(_doc, nazivi_kolekcije_dokumenata)

    return nazivi_kolekcije_dokumenata

nazivi_kolekcije_dokumenata = []
for doc in docs:
    nazivi_kolekcije_dokumenata += list(map(lambda naziv_kolekcije_dokumenata: graf_naziv.upper() + "_" + naziv_kolekcije_dokumenata, nazivi_kolekcije_dokumenata_za_kreirati(doc)))

# ------ generisanje klasa u privremenom fajlu uz pomoc jinja2
from jinja2 import Environment, PackageLoader, select_autoescape
env = Environment(
    loader=PackageLoader('iz_baze_cinjenica_u_grafovsku_bazu', 'templates'),
    autoescape=select_autoescape(['py'])
)

kreirane_klase = "from pyArango import collection, graph\n"
kreirane_klase += "class " + graf_naziv.upper() + "_RELACIJE(collection.Edges):\n\tpass\n"
for naziv_kolekcije_dokumenata in nazivi_kolekcije_dokumenata:
    template = env.get_template('kolekcija_dokumenata.py')
    kreirane_klase += template.render(ime_klase=naziv_kolekcije_dokumenata)

kreirane_klase += env.get_template('graf.py').render(ime_klase=graf_naziv.upper(), edges_collection=graf_naziv.upper()+"_RELACIJE", \
                                                    nazivi_kolekcije_dokumenata=nazivi_kolekcije_dokumenata)
# ------ 
with open("meta/documents/templates/kreirane_klase.py", 'w') as w:
    w.write(kreirane_klase)
from templates.kreirane_klase import *

try:
    graf = arangodb.createGraph(graf_naziv.upper(), createCollections=True)
except Exception as e:
    pass

graf = arangodb.graphs[graf_naziv.upper()]
graf : graph.Graph

def kreiraj_temena_i_relacije(doc, roditeljski_doc=None):
    teme = {}

    for kljuc in doc:
        podatak = doc[kljuc]
        if isinstance(podatak, dict):
            if kljuc in ['_meta']:
                teme[kljuc] = podatak
        elif isinstance(podatak, list):
            pass
        else:
            # jednostavno polje
            teme[kljuc] = podatak

    naziv_kolekcije = graf_naziv.upper() + "_" + teme['_meta']['_tabela_naziv'].upper()
    teme = graf.createVertex(naziv_kolekcije, teme)
    teme.save()
    doc['_teme'] = teme

    if roditeljski_doc is not None:
        # kreiraj relacije
        graf.link(graf_naziv.upper() + "_RELACIJE", teme, roditeljski_doc['_teme'], {'type': roditeljski_doc['_meta']['_tabela_naziv'].replace("_", " ").lower()})

    # prolazimo kroz decu doc-a
    for kljuc in doc:
        podatak = doc[kljuc]
        if isinstance(podatak, dict) and kljuc not in ['_meta']:
            kreiraj_temena_i_relacije(podatak, doc)
        elif isinstance(podatak, list):
            for _doc in podatak:
                kreiraj_temena_i_relacije(_doc, doc)

# brisanje svih dokumenata u kolekcijama
for naziv_kolekcije_dokumenata in nazivi_kolekcije_dokumenata:
    kolekcija_dokumenata = arangodb.collections[naziv_kolekcije_dokumenata]
    kolekcija_dokumenata : collection.Collection
    for doc in kolekcija_dokumenata.fetchAll():
        graf.deleteVertex(doc) # obrisace sve dokumente unutar kolekcije i relacije sa dokumentom

# arangodb.dropAllCollections()

for doc in docs:
    kreiraj_temena_i_relacije(doc)