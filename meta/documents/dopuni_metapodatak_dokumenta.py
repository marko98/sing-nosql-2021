import json
import copy

metpodatak_dokumenta = ""
with open('meta/documents/registrovani_studijski_program.json', 'r') as fp:
    metpodatak_dokumenta = json.load(fp)

pks = []
def obradi_tabelu(tabela, roditeljska_tabela=None):
    naziv_tabele = tabela["_tabela_naziv"]
    tip = tabela['_tip'] if '_tip' in tabela else None
    od = tabela['_od'] if '_od' in tabela else None
    do = tabela['_do'] if '_do' in tabela else None

    inner_join = ""
    if roditeljska_tabela is not None:
        inner_join = "INNER JOIN " + naziv_tabele + " ON "
        if len(od) != len(do):
            raise ValueError('inner join error')
        
        for index, _od in enumerate(od):
            inner_join += roditeljska_tabela['_tabela_naziv'] + "." + _od + " = " + naziv_tabele + "." + do[index]
            if index + 1 != len(od):
                inner_join += " AND "

        inner_join += "\n"
        # print('hi')

    meta_kljucevi = ["_tabela_naziv", "_pks", "_tip", "_od", "_do"]
    fields = ""
    for kljuc in tabela:
        podatak = tabela[kljuc]
        if isinstance(podatak, dict):
            _inner_join, _fields = obradi_tabelu(podatak, tabela)
            inner_join += _inner_join
            fields += _fields
        else:
            if kljuc in ["_pks"]:
                # # _pks
                pk_postoji_medju_poljima = False
                for pk in podatak:
                    # proverimo da li se pk nalazi u nekom polju(tj. da li ga neko polje predstavlja)
                    for _kljuc in tabela:
                        _podatak = tabela[_kljuc]
                        if not isinstance(_podatak, dict) and _kljuc not in meta_kljucevi:
                            if _podatak.split('=')[0] == pk.split('=')[0]:
                                pk_postoji_medju_poljima = True

                    if not pk_postoji_medju_poljima and pk.split('=')[0] not in pks:
                        pks.append(pk.split('=')[0])
                        fields += naziv_tabele + "." + pk.split('=')[0] + " AS " + pk.split('=')[0] + ", \n"
                
            if kljuc not in meta_kljucevi:
                fields += naziv_tabele + "." + podatak.split('=')[0] + ", \n"
    return inner_join, fields

def izgradi_view_naredbu(metpodatak_dokumenta):    
    view_naziv = metpodatak_dokumenta['kolekcija_naziv'].upper()

    view_naredba = "DROP VIEW IF EXISTS `" + view_naziv + "`;"
    view_naredba += "CREATE VIEW `" + view_naziv + "`\n"
    view_naredba += "AS SELECT\n"

    inner_join, fields = obradi_tabelu(copy.deepcopy(metpodatak_dokumenta['dokument']))
    inner_join = inner_join[:-1] + ";"
    fields = fields[:-3] + "\n"

    view_naredba += fields
    view_naredba += "FROM " + metpodatak_dokumenta['dokument']['_tabela_naziv'] + "\n"
    view_naredba += inner_join
    return view_naredba

def izgradi_dobavi_proceduru(metpodatak_dokumenta):
    # DROP PROCEDURE IF EXISTS `dobavi_drzava`; CREATE PROCEDURE `dobavi_drzava`(IN DR_IDENTIFIKATOR CHAR(3), IN DR_NAZIV VARCHAR(10), IN DR_GODINA_OSNIVANJA DATE) BEGIN END // 

    table_name = metpodatak_dokumenta['kolekcija_naziv'].upper()

    procedure_name = 'dobavi_' + table_name.lower()

    params = ""
    conditions = ""
    params_value = []
    for index, parametar in enumerate(metpodatak_dokumenta['parametri']):
        param_name = "_" + parametar['atribut']
        param_type = parametar['sql_tip']

        params += "IN " + param_name + " " + param_type + ", "
        # if index + 1 != len(table):
        #     params += ', '
        params_value.append({parametar['atribut']: None})

        if parametar['tip'] in ['STRING', 'DATE', 'BLOB', 'TEXT']:
            conditions += ' IF ' + param_name + ' IS NOT NULL THEN ' + \
                'IF @WHERE_CLAUSE = "WHERE " THEN ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "' + parametar['atribut'] + ' LIKE \'", ' + param_name + ', "\' "); ' + \
                'ELSE ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "AND ' + parametar['atribut'] + ' LIKE \'", ' + param_name + ', "\' "); ' + \
                'END IF; ' + \
                'END IF;'
        else:
            conditions += ' IF ' + param_name + ' IS NOT NULL THEN ' + \
                'IF @WHERE_CLAUSE = "WHERE " THEN ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "' + parametar['atribut'] + ' LIKE ", ' + param_name + ', " "); ' + \
                'ELSE ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "AND ' + parametar['atribut'] + ' LIKE ", ' + param_name + ', " "); ' + \
                'END IF; ' + \
                'END IF;'

    params += "IN _LIMIT INT, IN _OFFSET INT"
    params_value.append({"LIMIT": None})
    params_value.append({"OFFSET": None})

    conditions += " IF (_LIMIT IS NULL) THEN SET _LIMIT := 5; END IF; IF (_OFFSET IS NULL) THEN SET _OFFSET := 0; END IF;"
    conditions += " IF @WHERE_CLAUSE != 'WHERE ' THEN SET @SQL_CLAUSE := CONCAT(@SQL_CLAUSE, @WHERE_CLAUSE, 'LIMIT ', _LIMIT, ' OFFSET ', _OFFSET); ELSE SET @SQL_CLAUSE := CONCAT(@SQL_CLAUSE, 'LIMIT ', _LIMIT, ' OFFSET ', _OFFSET); END IF;"

    sql_clause = "SET @SQL_CLAUSE := 'SELECT * FROM " + table_name + " ';"
    where_clause = "SET @WHERE_CLAUSE := 'WHERE ';"
    sql = """DROP PROCEDURE IF EXISTS `""" + procedure_name + """` // CREATE PROCEDURE `""" + procedure_name + """`(""" + params + """) BEGIN """ + \
    sql_clause + """ """ + where_clause + """ """ + conditions + """ """ + \
    "PREPARE sql_cl FROM @SQL_CLAUSE; " + "EXECUTE sql_cl; " + "DEALLOCATE PREPARE sql_cl;" + \
    """ END //"""

    return sql, {"naziv": procedure_name, "args": params_value}

def izgradi_prebroj_proceduru(metpodatak_dokumenta):
    # DROP PROCEDURE IF EXISTS `dobavi_drzava`; CREATE PROCEDURE `dobavi_drzava`(IN DR_IDENTIFIKATOR CHAR(3), IN DR_NAZIV VARCHAR(10), IN DR_GODINA_OSNIVANJA DATE) BEGIN END // 

    table_name = metpodatak_dokumenta['kolekcija_naziv'].upper()

    procedure_name = 'prebroj_' + table_name.lower()

    params = ""
    conditions = ""
    params_value = []
    for index, parametar in enumerate(metpodatak_dokumenta['parametri']):
        param_name = "_" + parametar['atribut']
        param_type = parametar['sql_tip']

        params += "IN " + param_name + " " + param_type
        if index + 1 != len(metpodatak_dokumenta['parametri']):
            params += ', '
        params_value.append({parametar['atribut']: None})

        if parametar['tip'] in ['STRING', 'DATE', 'BLOB', 'TEXT']:
            conditions += ' IF ' + param_name + ' IS NOT NULL THEN ' + \
                'IF @WHERE_CLAUSE = "WHERE " THEN ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "' + parametar['atribut'] + ' LIKE \'", ' + param_name + ', "\' "); ' + \
                'ELSE ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "AND ' + parametar['atribut'] + ' LIKE \'", ' + param_name + ', "\' "); ' + \
                'END IF; ' + \
                'END IF;'
        else:
            conditions += ' IF ' + param_name + ' IS NOT NULL THEN ' + \
                'IF @WHERE_CLAUSE = "WHERE " THEN ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "' + parametar['atribut'] + ' LIKE ", ' + param_name + ', " "); ' + \
                'ELSE ' + \
                'SET @WHERE_CLAUSE := CONCAT(@WHERE_CLAUSE, "AND ' + parametar['atribut'] + ' LIKE ", ' + param_name + ', " "); ' + \
                'END IF; ' + \
                'END IF;'

    conditions += " IF @WHERE_CLAUSE != 'WHERE ' THEN SET @SQL_CLAUSE := CONCAT(@SQL_CLAUSE, @WHERE_CLAUSE); END IF;"

    sql_clause = "SET @SQL_CLAUSE := 'SELECT COUNT(*) FROM " + table_name + " ';"
    where_clause = "SET @WHERE_CLAUSE := 'WHERE ';"
    sql = """DROP PROCEDURE IF EXISTS `""" + procedure_name + """` // CREATE PROCEDURE `""" + procedure_name + """`(""" + params + """) BEGIN """ + \
    sql_clause + """ """ + where_clause + """ """ + conditions + """ """ + \
    "PREPARE sql_cl FROM @SQL_CLAUSE; " + "EXECUTE sql_cl; " + "DEALLOCATE PREPARE sql_cl;" + \
    """ END //"""
    
    return sql, {"naziv": procedure_name, "args": params_value}

dobavi_procedura_naredba, dobavi_procedura = izgradi_dobavi_proceduru(copy.deepcopy(metpodatak_dokumenta))
metpodatak_dokumenta['operacije']['dobavi'] = dobavi_procedura
prebroj_procedura_naredba, prebroj_procedura = izgradi_prebroj_proceduru(copy.deepcopy(metpodatak_dokumenta))
metpodatak_dokumenta['operacije']['prebroj'] = prebroj_procedura

with open('meta/documents/registrovani_studijski_program.json', 'w') as w:
    json.dump(metpodatak_dokumenta, w)

with open('meta/documents/registrovani_studijski_program.sql', 'w') as w:
    sql = izgradi_view_naredbu(copy.deepcopy(metpodatak_dokumenta)) + "\n"
    sql += """USE `""" + metpodatak_dokumenta['baza_cinjenica']['db_naziv'] + """`;
DELIMITER // 
""" + dobavi_procedura_naredba + """
""" + prebroj_procedura_naredba + """
"""
    sql += """DELIMITER ;"""

    w.write(sql)