from PyQt5 import QtWidgets
import sys
from core.jezgro_gui import JezgroGUI

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    jezgro_gui = JezgroGUI()
    sys.exit(app.exec_())

# ==========================================
# from PyQt5 import QtWidgets
# import sys

# from core.business_logic.model.registar_komponenti import RegistarKomponenti
# from core.business_logic.model.jezgro import Jezgro
# from core.jezgro_gui import JezgroGUI

# app = QtWidgets.QApplication(sys.argv)

# # business logic
# jezgro = Jezgro.dobavi_instancu()
# # jezgro.obavesti('projekat.komponenta_1')

# # gui logic
# jezgro_gui = JezgroGUI()

# moja_komp = jezgro.obavesti('projekat.komponenta_za_rad_sa_entitetima')
# moja_komp.setParent(jezgro_gui.ui)
# jezgro_gui.ui.setCentralWidget(moja_komp)
# # komp_1_ui.show()

# sys.exit(app.exec_())
# ==========================================

# # business_logic.podatak
# from business_logic.podatak.model.lisni_podatak import LisniPodatak
# from business_logic.podatak.model.podatak_tip import PodatakTip
# from business_logic.podatak.model.kompozitni_podatak import KompozitniPodatak
# from business_logic.podatak.pattern.creational.abstract_factory.abstract_factory import AbstractFactory

# ab_factory = AbstractFactory.dobavi_instancu()

# # atribut
# srbija_dr_id = LisniPodatak()
# srbija_dr_id.postavi_atribut('DR_ID')
# srbija_dr_id.postavi_obavezan(True)
# srbija_dr_id.postavi_jedinstven(True)
# srbija_dr_id.postavi_vrednost(0)
# srbija_dr_id.postavi_tip(PodatakTip.NUMBER)

# usa_dr_id = LisniPodatak()
# usa_dr_id.postavi_atribut('DR_ID')
# usa_dr_id.postavi_obavezan(True)
# usa_dr_id.postavi_jedinstven(True)
# usa_dr_id.postavi_vrednost(1)
# usa_dr_id.postavi_tip(PodatakTip.NUMBER)

# # entiteti
# srbija = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.ENTITET)
# srbija.postavi_atribut('DRZAVA_ENTITET')
# srbija.dodaj_dete(srbija_dr_id)

# usa = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.ENTITET)
# usa.postavi_atribut('DRZAVA_ENTITET')
# usa.dodaj_dete(usa_dr_id)

# # tabele
# drzave = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.BATCH)
# drzave.postavi_atribut('DRZAVA_BATCH')
# drzave.dodaj_dete(srbija)
# drzave.dodaj_dete(usa)

# # baze
# host = LisniPodatak()
# host.postavi_atribut('HOST')
# host.postavi_obavezan(True)
# host.postavi_jedinstven(True)
# host.postavi_vrednost('localhost')
# host.postavi_tip(PodatakTip.STRING)
# korisnicko_ime = LisniPodatak()
# korisnicko_ime.postavi_atribut('KORISNICKO_IME')
# korisnicko_ime.postavi_obavezan(True)
# korisnicko_ime.postavi_jedinstven(True)
# korisnicko_ime.postavi_vrednost('root')
# korisnicko_ime.postavi_tip(PodatakTip.STRING)
# lozinka = LisniPodatak()
# lozinka.postavi_atribut('LOZINKA')
# lozinka.postavi_obavezan(True)
# lozinka.postavi_jedinstven(True)
# lozinka.postavi_vrednost('root')
# lozinka.postavi_tip(PodatakTip.STRING)

# mysql_db = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.DATABASE)
# mysql_db.postavi_atribut('MYSQL_DATABASE')
# mysql_db.postavi_decu([host, korisnicko_ime, lozinka])
# mysql_db.dodaj_dete(drzave)

# # print(drzave)
# # print(srbija)
# # print(usa)
# # print(srbija_dr_id)
# # print(usa_dr_id)

# # mysql_db.prikazi_podatke()

# # region batch

# # atribut
# reg_id = LisniPodatak()
# reg_id.postavi_atribut('REG_ID')
# reg_id.postavi_obavezan(True)
# reg_id.postavi_jedinstven(True)
# reg_id.postavi_vrednost(0)
# reg_id.postavi_tip(PodatakTip.NUMBER)

# reg_dr_id = LisniPodatak()
# reg_dr_id.postavi_atribut('DR_ID')
# reg_dr_id.postavi_obavezan(True)
# reg_dr_id.postavi_jedinstven(True)
# reg_dr_id.postavi_tip(PodatakTip.NUMBER)
# # reg_dr_id.postavi_referencira({'atribut': 'DRZAVA_ENTITET.DR_ID', 'vrednost': None})
# reg_dr_id.postavi_referencira({'atribut': 'DRZAVA_ENTITET.DR_ID', 'vrednost': srbija_dr_id})

# # entiteti
# vojvodina = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.ENTITET)
# vojvodina.postavi_atribut('VOJVODINA_REGION_ENTITET')
# vojvodina.dodaj_dete(reg_id)
# vojvodina.dodaj_dete(reg_dr_id)

# # tabele
# regioni = ab_factory.kreiraj_kompozitni_podatak(PodatakTip.BATCH)
# regioni.postavi_atribut('REGION_BATCH')
# regioni.dodaj_dete(vojvodina)

# # regioni.prikazi_podatke()
# # print(reg_dr_id.dobavi_vrednost())

# mysql_db.dodaj_dete(regioni)
# # mysql_db.prikazi_podatke()